// Final ID - 106
import * as images from '../assets/Images';
export let banner = [
    {
        "videos": [{
            "duration": 21334,
            "id": 1,
            "name": "Arm and Leg Raise",
            "preview": images.BiodtvArm,
            "desc": "",
            "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Arm+and+Leg+Raise.mp4"

        },
            {
                "duration": 21334,
                "id": 10,
                "name": "The Skin I Live In",
                "year": 2011,
                "sliderImg": images.esmobiotvSlide2,
                "preview": images.esmobiotvTheSkin,
                "desc": "Ever since his beloved wife was horribly burned in an auto accident, Dr. Robert Ledgard (Antonio Banderas), a skilled plastic surgeon, has tried to develop a new skin that could save the lives of burn victims. Finally, after 12 years, Ledgard has created a skin that guards the body, but is still sensitive to touch. With the aid of his faithful housekeeper (Marisa Paredes), Ledgard tests his creation on Vera (Elena Anaya), who is held prisoner against her will in the doctor's mansion.",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/The+Skin+I+Live+In/The+Skin+I+Live+In.mp4"
            },
            {
                "duration": 21334,
                "id": 10,
                "name": "The Skin I Live In",
                "year": 2011,
                "sliderImg": images.esmobiotvSlide2,
                "preview": images.esmobiotvTheSkin,
                "desc": "Ever since his beloved wife was horribly burned in an auto accident, Dr. Robert Ledgard (Antonio Banderas), a skilled plastic surgeon, has tried to develop a new skin that could save the lives of burn victims. Finally, after 12 years, Ledgard has created a skin that guards the body, but is still sensitive to touch. With the aid of his faithful housekeeper (Marisa Paredes), Ledgard tests his creation on Vera (Elena Anaya), who is held prisoner against her will in the doctor's mansion.",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/The+Skin+I+Live+In/The+Skin+I+Live+In.mp4"
            },
            {
                "duration": 21334,
                "id": 20,
                "sliderImg": images.esmobiotvSlide3,
                "name": "Pasión por el flamenco",
                "year": 2015,
                "preview": images.esmobiotvPasion,
                "desc": "A look at the history and traditions of flamenco music and dance.",
                "videoUrl": ""
            },
        ],
    },

]