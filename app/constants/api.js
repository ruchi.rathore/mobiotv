import Globals from './Globals';

const host = "http://ie-ec2-portal-b.sam-media.com";//"http://ie-ec2-portal-b.sam-media.com";//http://43.241.63.15";
const port = "3005";
export const Url = Globals.type == 'es' ? 'http://es.mobiotv.com' : 'http://uk.mobiotv.com';
export const BASE_API_URL =  `${Url}/api`;
//export const BASE_API_URL =  `${host}:${port}/api`;
export const BASE_URL =  `${host}:${port}/`;