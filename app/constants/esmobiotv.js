// Final ID -
import * as images from '../assets/Images';
export let esmobiotv = [

    {
        "id": 1,
        "contents": 8,
        "name": "Series de Televisión",
        "videos": [
            {
                "duration": 21334,
                "id": 7,
                "name": "El Cartel",
                "year": 2008,
                "preview": images.esmobiotvElCartel,
                "desc": "“El Cartel” cuenta la historia de “Martin, ” un narco o mejor dicho, un villano arrepentido que se enamora de “Sofía”. Una bella mujer que le pide como condición para compartir su vida con él, que se salga de este obscuro mundo de exceso, muerte, y desmedida ambición.\n" +
                "\n" +
                "“Martin” por primera vez esta realmente enamorado de una mujer. Siempre supo que el “traquetear” como él le llama a su “profesión” iba en contra de sus principios. Además de ser totalmente inmoral. Sin embargo, siempre fue más su ambición “por ser alguien en la vida” y su amor por “la plata”.\n" +
                "\n" +
                "Ahora con los ojos abiertos, gracias al amor de “Sofía” entiende sus pecados en su justa medida. Al punto de retar a “los duros” por sus actos violentos para conseguir el poder.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 8,
                "name": "El Señor de los Cielos",
                "year": 2013,
                "preview": images.esmobiotvSenior,
                "desc": "La telenovela narra la historia de Aurelio Casillas (Rafael Amaya), uno de los narcotraficantes más importantes de México en los años 90. La única ambición de Aurelio era convertirse en el narcotraficante más poderoso de México, sin importarle ser cauteloso y mucho menos llamativo. Logró tener fortuna, mujeres, casas, edificios y mansiones sin mucho esfuerzo. Cuando era pequeño, Aurelio no sabía leer ni escribir. Perdió a su padre desde joven y tuvo que sostener a su familia junto con su hermano Víctor Casillas (Raul Méndez). Con la ayuda de Daniel Jiménez Arroyo \"El Letrudo\" (Juan Ríos), Aurelio aprendió a escribir y leer. Ximena(Ximena Herrera), la mujer de su vida y madre de sus 3 hijos Heriberto (Ruy Senderos), Rutila (Carmen Aub) y Luz Marina (Gala Montes de Oca) vivió en el mundo del narcotráfico desde pequeña: su padre, Don Cleto (Javier Díaz Dueñas), era uno de los narcos más poderosos de la época.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 29,
                "name": "La Promesa",
                "year": 2013,
                "preview": images.esmobiotvLaPromesa,
                "desc": "Cada año cientos de mujeres caen en las redes de reclutadores que trafican con personas, viéndose forzadas a una vida de esclavitud sexual y laboral. Esta es la historia de «Ana», «Frida» y «Seleni», tres bellas jóvenes engañadas bajo la promesa de alcanzar la vida soñada. Inocentes y vulnerables, con la férrea convicción de que pese a las condiciones adversas en las que viven, pueden alcanzar un mejor futuro, serán engañadas y utilizadas por personas inescrupulosas que las adentrarán en una red de prostitución de la que no podrán escapar.\n" +
                "\n" +
                "Ellas nunca se habían visto antes; pero se convertirán en amigas a la fuerza, cuando se encuentren en un camino sin retorno y vivan la peor pesadilla que puede sufrir una mujer: ser víctima de la trata de personas. Sin saberlo «Ana», «Frida» y «Seleni», han adquirido una deuda imposible de pagar y amenazas de muerte contra sus familias, por eso sólo tendrán dos caminos para tomar, vivir como prostitutas esclavizadas o arriesgar sus vidas en su búsqueda por la libertad. Estas tres mujeres y un lote de compañeras más, serán trasladadas a España, destino final de la mercancía humana de la banda de trata de personas colombiana donde un mundo ajeno espera sus servicios. En medio de su lucha por escapar del abismo, «Ana», «Frida» y «Seleni», tratarán de encontrar en el amor la clave para salir de la degradación y tortura que han vivido y con ellas, el público recorrerá el camino que las hará más fuertes o que terminará destruyéndolas.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 1,
                "name": "Pablo Escobar, el patron del mal-Episode-1",
                "year": 2012,
                "preview": images.esmobiotvPablo,
                "desc": "PABLO ESCOBAR: EL PATRON DEL MAL, PARTE 1-3 Based on the best seller \" La Parabola de Pablo \" by journalist and ex Major of Medellin, Alonso Salazar, the series from Juana Uribe and Camilo Cano is based on real life events and is backed up by an impressive journalist documentation that makes it the most ambitious TV series from Colombia. In the series we are presented with a provocative and realist look at the life of one of the most famous criminals of the XX Century: Pablo Escobar. Considered by some a hero and by others the most violent of \"Capos\" ever. ",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Pablo+Escobar+Capitulo/Pablo+Escobar+Capitulo+1.mp4",
                "subCategory":[
                    {
                        "duration": 21334,
                        "id": 28,
                        "name": "Pablo Escobar, el patron del mal-Episode-2",
                        "year": 2012,
                        "preview": images.esmobiotvPablo,
                        "desc": "PABLO ESCOBAR: EL PATRON DEL MAL, PARTE 1-3 Based on the best seller \" La Parabola de Pablo \" by journalist and ex Major of Medellin, Alonso Salazar, the series from Juana Uribe and Camilo Cano is based on real life events and is backed up by an impressive journalist documentation that makes it the most ambitious TV series from Colombia. In the series we are presented with a provocative and realist look at the life of one of the most famous criminals of the XX Century: Pablo Escobar. Considered by some a hero and by others the most violent of \"Capos\" ever. ",
                        "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Pablo+Escobar+Capitulo/Pablo+Escobar+Capitulo+2.mp4",
                        "subCategory":[]
                    },
                ]
            },
            {
                "duration": 21334,
                "id": 2,
                "name": "Amar es para siempre",
                "year": 2013,
                "preview": images.esmobiotvamar,
                "desc": "La sexta temporada de Amar Es Para Siempre arranca en Septiembre de 1969. Muchas cosas han pasado en el mundo, y en la Plaza de los Frutos, desde que hace un año Ginés fuera detenido por el homicidio de Alonso Núñez de Losada... La protagonista de esta temporada seguirá siendo Marta Novoa, quien mantendrá una apasionada historia de amor con Diego Durán, periodista del semanario España Siete Días. La redacción del semanario será uno de los núcleos principales de esta temporada. Ernesto Armero, director del semanario, y casado con Matilde Velázquez, verá cómo su orgullo se tambalea cuando su mujer decide dar rienda suelta a sus deseos. Y es que Matilde se reencuentra con el gran amor de su vida, el escritor Julián Azevedo. Ambos reavivarán su historia de amor, truncada tras el exilio del escritor. Armero, mujeriego hasta la médula, se sigue refugiando cuando le conviene en los brazos de Charo Ponce, una actriz venida a menos con la que comparte un oscuro secreto... También los jóvenes caerán en las redes del amor. Susana Azevedo, la hija de Julián, será el vértice de un triángulo amoroso en el que Javier Armero, hijo de Ernesto, e Ignacio Solano, sobrino de Quintero, se disputarán el corazón de la muchacha. La buena amistad entre Javier e Ignacio, forjada durante la mili, se verá profundamente afectada por esta disputa. En el barrio, los Gómez, volverán a ser testigos del desamor de María, que regresa de Miami con el corazón partido y la confianza mermada. María, como buena asturiana, no se dejará llevar por el desánimo y seguirá enfrentándose a la vida como lo hace su familia: plantando cara a las adversidades. También, Benito y Benigna sufrirán una crisis amorosa al inicio de temporada, aunque gracias a los buenos consejos de Manolita y de Pelayo la pareja consigue reconciliarse. Lo mismo les sucederá a Teresa y a Quintero, que para más inri, han de lidiar con las correrías de su sobrino Ignacio, un bala perdida que trae a Teresa por la calle de la amargura.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 3,
                "name": "Bandolera-Episode-1",
                "year": 2010,
                "preview": images.esmobiotvBandolera,
                "desc": "'Bandolera' es una serie de televisión española emitida por Antena 3 durante 510 episodios divididos en 2 temporadas transcurridas entre los años 2011 y 2013. La ficción cuenta la historia de Sara Reeves (Marta Hazas), una británica enamorada de la cultura española que huye a Andalucía en busca de la vida que siempre había soñado. Cuando se entera del paradero de la joven, Richard Thomas, tío de Sara, viaja a España para intentar que la chica recapacite y vuelva a su país y a su acomodada vida. Cuando Sara es convencida por su tío, ambos son secuestrados por unos bandoleros que acaban asesinando a Thomas. Sara decide prolongar su aventura un poco más y, entre bandoleros, caciques, viajeros y campesinos, la joven intentará vengar el asesinato de su tío. Intentando emular el éxito con el que otras series diarias españolas habían contado en Italia, Canale 5 se hizo con los derechos del serial y fue emitida en el país transalpino bajo el nombre de 'Cuore ribelle' (Corazón rebelde').",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Bandolera/Bandolera+-+Season+1+Episode+1.mp4",
                "subCategory":[
                    {
                        "duration": 21334,
                        "id": 24,
                        "name": "Bandolera - Episode - 2",
                        "year": 2010,
                        "preview": images.esmobiotvBandolera,
                        "desc": "'Bandolera' es una serie de televisión española emitida por Antena 3 durante 510 episodios divididos en 2 temporadas transcurridas entre los años 2011 y 2013. La ficción cuenta la historia de Sara Reeves (Marta Hazas), una británica enamorada de la cultura española que huye a Andalucía en busca de la vida que siempre había soñado. Cuando se entera del paradero de la joven, Richard Thomas, tío de Sara, viaja a España para intentar que la chica recapacite y vuelva a su país y a su acomodada vida. Cuando Sara es convencida por su tío, ambos son secuestrados por unos bandoleros que acaban asesinando a Thomas. Sara decide prolongar su aventura un poco más y, entre bandoleros, caciques, viajeros y campesinos, la joven intentará vengar el asesinato de su tío. Intentando emular el éxito con el que otras series diarias españolas habían contado en Italia, Canale 5 se hizo con los derechos del serial y fue emitida en el país transalpino bajo el nombre de 'Cuore ribelle' (Corazón rebelde').",
                        "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Bandolera/Bandolera+-+Season+1+Episode+2.mp4",
                        "subCategory":[]
                    },
                    {
                        "duration": 21334,
                        "id": 25,
                        "name": "Bandolera - Episode - 3",
                        "year": 2010,
                        "preview": images.esmobiotvBandolera,
                        "desc": "'Bandolera' es una serie de televisión española emitida por Antena 3 durante 510 episodios divididos en 2 temporadas transcurridas entre los años 2011 y 2013. La ficción cuenta la historia de Sara Reeves (Marta Hazas), una británica enamorada de la cultura española que huye a Andalucía en busca de la vida que siempre había soñado. Cuando se entera del paradero de la joven, Richard Thomas, tío de Sara, viaja a España para intentar que la chica recapacite y vuelva a su país y a su acomodada vida. Cuando Sara es convencida por su tío, ambos son secuestrados por unos bandoleros que acaban asesinando a Thomas. Sara decide prolongar su aventura un poco más y, entre bandoleros, caciques, viajeros y campesinos, la joven intentará vengar el asesinato de su tío. Intentando emular el éxito con el que otras series diarias españolas habían contado en Italia, Canale 5 se hizo con los derechos del serial y fue emitida en el país transalpino bajo el nombre de 'Cuore ribelle' (Corazón rebelde').",
                        "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Bandolera/Bandolera+-+Season+1+Episode+3.mp4",
                        "subCategory":[]

                    },
                    {
                        "duration": 21334,
                        "id": 26,
                        "name": "Bandolera - Episode - 4",
                        "year": 2010,
                        "preview": images.esmobiotvBandolera,
                        "desc": "'Bandolera' es una serie de televisión española emitida por Antena 3 durante 510 episodios divididos en 2 temporadas transcurridas entre los años 2011 y 2013. La ficción cuenta la historia de Sara Reeves (Marta Hazas), una británica enamorada de la cultura española que huye a Andalucía en busca de la vida que siempre había soñado. Cuando se entera del paradero de la joven, Richard Thomas, tío de Sara, viaja a España para intentar que la chica recapacite y vuelva a su país y a su acomodada vida. Cuando Sara es convencida por su tío, ambos son secuestrados por unos bandoleros que acaban asesinando a Thomas. Sara decide prolongar su aventura un poco más y, entre bandoleros, caciques, viajeros y campesinos, la joven intentará vengar el asesinato de su tío. Intentando emular el éxito con el que otras series diarias españolas habían contado en Italia, Canale 5 se hizo con los derechos del serial y fue emitida en el país transalpino bajo el nombre de 'Cuore ribelle' (Corazón rebelde').",
                        "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Bandolera/Bandolera+-+Season+1+Episode+4.mp4",
                        "subCategory":[]

                    },
                    {
                        "duration": 21334,
                        "id": 27,
                        "name": "Bandolera - Episode - 5",
                        "year": 2010,
                        "preview": images.esmobiotvBandolera,
                        "desc": "'Bandolera' es una serie de televisión española emitida por Antena 3 durante 510 episodios divididos en 2 temporadas transcurridas entre los años 2011 y 2013. La ficción cuenta la historia de Sara Reeves (Marta Hazas), una británica enamorada de la cultura española que huye a Andalucía en busca de la vida que siempre había soñado. Cuando se entera del paradero de la joven, Richard Thomas, tío de Sara, viaja a España para intentar que la chica recapacite y vuelva a su país y a su acomodada vida. Cuando Sara es convencida por su tío, ambos son secuestrados por unos bandoleros que acaban asesinando a Thomas. Sara decide prolongar su aventura un poco más y, entre bandoleros, caciques, viajeros y campesinos, la joven intentará vengar el asesinato de su tío. Intentando emular el éxito con el que otras series diarias españolas habían contado en Italia, Canale 5 se hizo con los derechos del serial y fue emitida en el país transalpino bajo el nombre de 'Cuore ribelle' (Corazón rebelde').",
                        "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Bandolera/Bandolera+-+Season+1+Episode+5.mp4",
                        "subCategory":[]

                    }]
            },
            {
                "duration": 21334,
                "id": 4,
                "name": "Carmina o revienta",
                "year": 2012,
                "preview": images.esmobiotvCarminao,
                "desc": "Ver Carmina o revienta. | Pelicula Completa en Español Latino - Castellano - Subtitulado. Peli Pelicula Gratis para ver ONLINE | Calidad HD - Watch movies free online Carmina o revienta.\n" +
                "\n" +
                "Carmina es una señora de 58 años que regenta una venta en Sevilla donde se venden productos ibéricos. Tras sufrir varios robos y no encontrar el apoyo de la aseguradora, inventa una manera de recuperar el dinero para sacar a su familia adelante. Mientras espera el desenlace de su plan reflexiona en la cocina de su casa sobre su vida, obra y milagros",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 5,
                "name": "18 Comidas",
                "year": 2011,
                "preview": images.esmobiotvCOmidas,
                "desc": "Un músico callejero se reencuentra el amor de su vida; dos borrachos desayunan cubatas con marisco; un hombre cocina, cocina y cocina para la mujer que nunca llega; dos hombres se quieren, pero se esconden; una cocinera sueña con ser cantante; Una joven quiere, lo que un hombre no dá, mientras un camarero se muere por ella; un macedonio está perdido por desamor en Santiago de Compostela; una pareja de ancianos que ya se lo ha dicho todo desayuna, come y cena en silencio. Todas estas historias se cruzan a lo largo de un día de ficción en 18 comidas, una película de emociones servidas en torno a una mesa, un viaje por los sentimientos en la más universal de todas las luchas: la de ser feliz.6 historias, 24 personajes, un día de ficción, 90 horas de material, 6 meses de montaje. 18 comidas es todo eso y mucho más. Es un puzzle de sentimientos de personas muy distintas que comparten un mismo objetivo: la felicidad. Es un cruce constante de caminos a lo largo de desayunos, comidas y cenas, con una frontera muy difusa entre la risa y el llanto. Como la vida misma.",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/18+Comidas/18+Comidas.mp4"
            },
            {
                "duration": 21334,
                "id": 6,
                "name": "El Bronx",
                "year": 2017,
                "preview": images.esmobiotvElBronx,
                "desc": "El Bronx: Entre el cielo y el infierno, or simply El Bronx, is an upcoming Colombian television series written by Gustavo Bolívar and produced by Fox Telecolombia and Caracol Televisión, based on the events that occurred in the town of El Bronx, a place that is located in the center of Bogota, where people lived in misery, drugs and crime. The series features the debut of Venezuelan actress Rosmeri Marval as the titular character.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 30,
                "name": "Sobreviviendo a Escobar, Alias JJ",
                "year": 2017,
                "preview": images.esmobiotvSobrevi,
                "desc": "“Sobreviviendo a Escobar, Alias JJ” relata la vida de Alias JJ “Popeye” después de convertirse en el único sobreviviente del Cartel de Medellín. Todo comienza en la decadencia de Pablo Escobar, cuando Alias JJ decide entregarse a la justicia. negociando una condena no superior a los 7 años. Pero con la muerte de Escobar, la vida de J.J. en la cárcel cambia por completo, tiene que enfrentarse a su peores enemigos y usar todas sus tácticas no solo para sobrevivir, si o no para lograr establecerse de nuevo como un gran capo dentro de la cárcel y continuar con su reino de terror, volviendo a ser un temido narcotraficante desde la prisión.",
                "videoUrl": ""
            },
        ]
    },
    {
        "id": 2,
        "contents": 8,
        "name": "Películas",
        "videos": [{
                "duration": 21334,
                "id": 9,
                "name": "Volver (I)",
                "year": 2006,
                "preview": images.esmobiotvVolver,
                "desc": "Raimunda (Penélope Cruz) es manchega, pero vive en Madrid. Está casada con un obrero en paro (Antonio de la Torre) y tiene una hija adolescente (Yohana Cobo). Su hermana Sole (Lola Dueñas) se gana la vida como peluquera. Ambas echan de menos a su madre (Carmen Maura), que murió en un incendio. Pero, inesperadamente, la madre se presenta en casa de su hermana (Lampreave); después va a ver a Sole, a Raimunda y a Agustina (Portillo), una vecina del pueblo.",
                "videoUrl": ""
                //"videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Volver/Volver.mp4"
            },
            {
                "duration": 21334,
                "id": 10,
                "name": "La piel que habito",
                "year": 2011,
                "preview": images.esmobiotvTheSkin,
                "desc": "Robert Ledgard (Antonio Baneras) es un eminente cirujano plástico que realiza sus experimentos en su clínica privada, El Cigarral. Desde que su mujer y su hija murieran, vive obsesionado con la creación de un nuevo tipo de piel, al que ha denominado G.A.L. La comunidad científica no permite esos experimentos, pero él, de forma clandestina, ha seguido realizándolos por su cuenta gracias a una cobaya, una joven, Vera (Elena Anaya, 'Habitación en Roma') a la que mantiene retenida y que se ha convertido en el centro de su vida. Pero Vera oculta un secreto y quiere venganza por todo lo que el doctor Legrand ha hecho con ella. Los dos viven encerrados en la mansión del cirujano junto a la servil Marilia (Marisa Paredes, 'La flor de mi secreto'), que es en realidad la madre de Robert.  El equilibrio del trío protagonista se verá alterado cuando una tarde de carnaval se presente en la casa otro de los hijos de Marilia, Zeca (Roberto Álamo, 'Dispongo de barcos'), disfrazado de tigre, arrollando por donde pase y creando confusión en el delicado microclima de relaciones que se ha establecido. A partir de ese momento, nada volverá a ser lo mismo para ninguno de ellos. Marilia, Robert y Vera rememorarán su pasado, cada uno desde su punto de vista. Un pasado que vuelve para tomarse su revancha.",
                "videoUrl": ""
                //"videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/The+Skin+I+Live+In/The+Skin+I+Live+In.mp4"
            },
            {
                "duration": 21334,
                "id": 11,
                "name": "Detective Marañon",
                "year": 2015,
                "preview": images.esmobiotvDetective,
                "desc": "Marañón es un peculiar detective que resuelve casos de extorsión. Cuando un podersoso senador lo contrata, se une al equipo Valeria, una psicóloga que podría ser pieza clave para resolver un crimen que vale millones.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 12,
                "name": "El Coco",
                "year": 2016,
                "preview": images.esmobiotvCoco,
                "desc": "Piroberta después de la muerte de su tía, recibe la noticia de ser la única heredera de una casa que a él no le trae buenos recuerdos, pero sus amigos más cercanos: Don Jediondo, Micolta, Sagrario y Tahiana lo convencen de ir a recibirla. Piroberta y sus amigos llegan a la casa y se encuentran con una tétrica casona: grande, vieja, oscura, abandonada y rodeada de un espeso bosque. Quien recibe a los amigos dentro de la misteriosa casona es Hugo un críptico y pálido mayordomo que ha trabajado para la familia desde que Piroberta tiene memoria. Estando en la casa tienen un encuentro con el espíritu de Auxilito, una niña que habitó allí y que canta la canción de EL COCO, luego de este suceso y en medio de varios acontecimientos inexplicables, Tahiana desaparece misteriosamente, el grupo se divide entre los asustados que quieren partir y los otros que piensan que no pueden dejar a su amiga allí, por lo que toman una decisión drástica: buscar ayuda especializada. Ahora acompañados por la Bruja Dioselina, quien en pleno trance lee en las cenizas del tabaco que EL COCO les quiere comunicar algo y que es la niña quien trae el mensaje por lo tanto van a tener que regresar al mismo lugar donde tuvieron el contacto con la pequeña y esperar a la noche. Poco después de la media noche, desde el ático del segundo piso, nuestros personajes empiezan a escuchar sonidos extraños, golpes, lamentos, risas desencajadas, la canción del Coco cantada por la niña. La luz de la casona se corta, algunas cosas se empiezan a mover solas, llaman a la puerta pero ninguno se atreve a ir y abrir, se rompen vidrios, presencias inmateriales empiezan a acosar a los personajes, a hablarles al oído, a empujarlos. Cada vez todo se torna más terrorífico y no sabemos cómo nuestros personajes envueltos en situaciones paranormales van a salir de ellas. Y solo la fuerza y la verdad los puede llevar a una salida y al rescate de su amiga.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 13,
                "name": "La Rectora",
                "year": 2015,
                "preview": images.esmobiotvLaRectora,
                "desc": "¿Quién es Bárbara Gaos? A partir de este interrogante se desencadena una historia llena de intrigas, pasiones y dolor. Una seductora bailarina española busca cambiar su suerte. De las noches de Madrid llega a las frías calles de Bogotá, donde un día el azar pone en su camino a Francisco Charry, un hombre con una exitosa carrera como rector de una prestigiosa universidad, que no ha logrado recomponer su vida íntima tras la muerte de su esposa. Este encuentro cambia radicalmente sus vidas, pues finalmente creen encontrar lo que tanto buscaban. Pero la ambición y la ingenuidad pondrán en riesgo la vida que ambos han idealizado.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 14,
                "name": "Cruz Del Sur",
                "year": 2012,
                "preview": images.esmobiotvCruzDelSur,
                "desc": "La vida de Juan cambia cuando es despedido de su trabajo debido a una reducción de la plantilla. La difícil situación unida a la tensión familiar le llevan a tomar la decisión de abandonar su país para buscar un futuro mejor en Barcelona.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 15,
                "name": "Morenita, El Escandalo",
                "year": 2008,
                "preview": images.esmobiotvMorenitaElEscandalo,
                "desc": "Mateo, un joven pintor y experto entrenador de palomas mensajeras, vende 42 ejemplares a Aurelio alias \"El Pinto\", un importante narcotraficante del cartel de Tijuana, quien las utilizará para transportar cocaína. El viaje resulta fallido, y \"El Pinto\" tortura a Mateo y amenaza a su familia si no recupera su dinero, así que, en su desesperación, Mateo decide secuestrar la imagen sagrada de la Virgen de Guadalupe de la Basílica, lo que acarrea un caos espiritual en el país.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 16,
                "name": "Dos a La Carta",
                "year": 2014,
                "preview": images.esmobiotvDosaLaCarta,
                "desc": "Oscar, un calculador broker urbano casado con una ambiciosa pija, y Dani, un torpe aldeano sin oficio ni beneficio, descubren que son hermanos y se ven obligados a compartir un restaurante perdido en un idílico paraje rural.",
                "videoUrl": ""
            },
        ]
    },
    {
        "id": 3,
        "contents": 8,
        "name": "Documentales",
        "videos": [
            {
                "duration": 21334,
                "id": 17,
                "name": "Buenaventura, No Me Dejes Más",
                "year": 2015,
                "preview": images.esmobiotvBuenaventura,
                "desc": "His stage name, refers to a port of the Colombian pacific named Buenaventura, where Yuri Bedoya was born and raised, a 46 years old mulatto, who in spite of being rather short and thin, says he is a 6 foot 7 inch black man deep down. Buenaventura was inhabited by Cimarrones, free slaves; being born and raised there really scarred Yuri. The sounds of the drum, marimba, the cooing of black people in the seashore and the gray sky; the forest, the Tunda and the Virgen del Carmen; the witchcraft and faith that are breathed there, gave the hallmark with which you can recognize a musician from Buenaventura.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 18,
                "name": "El Valle sin Sombras",
                "year": 2016,
                "preview": images.esmobiotvBElVallesinSombras,
                "desc": "A dark night of 1985 a huge avalanche mixed, in only one mass, children, women, elderly, animals, tractors. More than 25,000 people and an entire village were buried in only 15 minutes. It was an announced tragedy.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 19,
                "name": "Gabo, La Mágia de lo Real",
                "year": 2015,
                "preview": images.esmobiotvGabo,
                "desc": "'Gabo, The Magic of Reality' is a story about the incredible power of human imagination, which follows the interwoven threads of Gabriel García Márquez's life and work - \"Gabo\" to all of Latin America - with the narrative tension of an investigation.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 20,
                "name": "Pasión por el flamenco",
                "year": 2015,
                "preview": images.esmobiotvPasion,
                "desc": "A look at the history and traditions of flamenco music and dance.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 21,
                "name": "Nature Tech - How Engineers Are Inspired by Nature's Top Designs",
                "year": 2006,
                "preview": images.esmobiotvNature,
                "desc": "Designers and engineers all over the world are borrowing from Nature’s blueprints—finally using a resource that has been at our fingertips for millions of years.",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 22,
                "name": "Ice Diving - Underwater",
                "year": "",
                "preview": images.esmobiotvIcce,
                "desc": "Ready to go under the Ice? Experience cold water with the latest innovation from the undisputed leader in regulators. Our exclusive Extended Thermal Insulating System (XTIS) is included in the MK25 EVO, leaving you free to go further than ever in both cold and tropical waters. So, where will you take it first?",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 23,
                "name": "Big City - Award",
                "year": "",
                "preview": images.esmobiotvBig,
                "desc": "A rich history of the life and legacy of the Pruitt-Igoe housing complex in big city, an urban renewal project that suffered from neglect, vandalism and crime and was eventually torn down.",
                "videoUrl": ""
            },
        ]
    },
];