// Final ID - 106
import * as images from '../assets/Images';
export let bidiotvMoviesData = [

    {
        "id": 1,
        "contents": 24,
        "name": "Fitness",
        "videos": [
            {
                "duration": 21334,
                "id": 1,
                "name": "Arm and Leg Raise",
                "preview": images.BiodtvArm,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Arm+and+Leg+Raise.mp4"

            },
            {
                "duration": 21334,
                "id": 2,
                "name": "Attack and Defense",
                "preview": images.BiodtvAttack,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Attack+and+Defense.mp4"

            },
            {
                "duration": 21334,
                "id": 3,
                "name": "Condition training",
                "preview": images.BiodtvCond,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Condition+training.mp4"

            },
            {
                "duration": 21334,
                "id": 4,
                "name": "Core Strength",
                "preview": images.BiodtvCore,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Core+strength.mp4"

            },
            {
                "duration": 21334,
                "id": 5,
                "name": "Lunges",
                "preview": images.BiodtvLunges,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Lunges.mp4"

            },
            {
                "duration": 21334,
                "id": 6,
                "name": "Punch Bag",
                "preview": images.BiodtvPunch,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Punch+Bag.mp4"

            },
            {
                "duration": 21334,
                "id": 7,
                "name": "Push Ups",
                "preview": images.BiodtvPush,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Push+Ups.mp4"

            },
            {
                "duration": 21334,
                "id": 8,
                "name": "Russian Kicks",
                "preview": images.BiodtvRussian,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Russian+Kicks.mp4"

            },
            {
                "duration": 21334,
                "id": 9,
                "name": "Shadow Boxing",
                "preview": images.BiodtvShadow,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Shadowboxing.mp4"

            },
            {
                "duration": 21334,
                "id": 10,
                "name": "Sit Ups",
                "preview": images.BiodtvSitUp,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Sit+Ups.mp4"

            },
            {
                "duration": 21334,
                "id": 11,
                "name": "Stretching",
                "preview": images.BiodtvStrecthing,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Stretching.mp4"

            },
            {
                "duration": 21334,
                "id": 12,
                "name": "The Plank",
                "preview": images.BiodtvPlank,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/The+Plank.mp4"

            },
            {
                "duration": 21334,
                "id": 13,
                "name": "Tricep Dips",
                "preview": images.BiodtvTricep,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fitness/Tricep+Dips.mp4"

            },
        ]
    },
    {
        "id": 2,
        "contents": 24,
        "name": "Sports",
        "videos": [
            {
                "duration": 21334,
                "id": 14,
                "name": "Coaches Clinic Pete Herrman",
                "preview": images.BiodtvCoaches,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/Coaches+Clinic++Pete+Herrman.mp4"

            },
            {
                "duration": 21334,
                "id": 15,
                "name": "Dribble Drive Motion Skills",
                "preview": images.BiodtvDribble,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/Dribble+Drive+Motion+Skills.mp4"

            },
            {
                "duration": 21334,
                "id": 16,
                "name": "GAMCA Coaches Clinic",
                "preview": images.BiodtvCoaches1,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/GAMCA+Coaches+Clinic.mp4"

            },
            {
                "duration": 21334,
                "id": 17,
                "name": "Robert Tutein (UTICA) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvRobert,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/ROBERT+TUTEIN+(UTICA)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
            {
                "duration": 21334,
                "id": 18,
                "name": "Ryan Kelley (ARCADIA) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvSports,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/RYAN+KELLEY+(ARCADIA)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
            {
                "duration": 21334,
                "id": 19,
                "name": "Sam Borst Smith (ROCHESTER) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvSAM,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/SAM+BORST+SMITH+(ROCHESTER)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
            {
                "duration": 21334,
                "id": 20,
                "name": "Special Spectators visits the College Slam Dunk & 3- Point Championships Intersport",
                "preview": images.BiodtvSpectator,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/Special+Spectators+visits+the+College+Slam+Dunk+%26+3-Point+Championships+Intersport.mp4"

            },
            {
                "duration": 21334,
                "id": 21,
                "name": "Tanner Schieve (Wisconsin Lutheran College) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvTanner,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/Tanner+Schieve+(Wisconsin+Lutheran+College)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
            {
                "duration": 21334,
                "id": 22,
                "name": "Trevor Krizenesky (Concordia University Wisconsin) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvTrevor,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/Trevor+Krizenesky+(Concordia+University+-+Wisconsin)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
            {
                "duration": 21334,
                "id": 23,
                "name": "Venky Jois (EASTERN WASHINGTON) - 2016 GEICO Play of the Year Nominee",
                "preview": images.BiodtvVENKY,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Sports/VENKY+JOIS+(EASTERN+WASHINGTON)+-+2016+GEICO+Play+of+the+Year+Nominee.mp4"

            },
        ]
    },
    {
        "id": 3,
        "contents": 24,
        "name": "Top Gear",
        "videos": [
            {
                "duration": 21334,
                "id": 24,
                "name": "2016 Chevrolet Camaro SS",
                "preview": images.BiodtvTop,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Top+Gear/2016+Chevrolet+Camaro+SS.mp4"

            },
            {
                "duration": 21334,
                "id": 25,
                "name": "2016 Ford Mustang Fastback",
                "preview": images.BiodtvFord,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Top+Gear/2016+Ford+Mustang+Fastback.mp4"

            },
            {
                "duration": 21334,
                "id": 26,
                "name": "2016 Ford Mustang GT4",
                "preview": images.BiodtvGT4,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Top+Gear/2016+Ford+Mustang+GT4.mp4"

            },
            {
                "duration": 21334,
                "id": 27,
                "name": "2016 Toyota Prius G Racing Car",
                "preview": images.BiodtvToyota,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Top+Gear/2016+Toyota+Prius+G+Racing+Car.mp4"

            },
            {
                "duration": 21334,
                "id": 28,
                "name": "2016 Toyota Xtreme Corolla",
                "preview": images.BiodtvCorolla,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Top+Gear/2016+Toyota+Xtreme+Corolla.mp4"

            },
        ]
    },
    {
        "id": 4,
        "contents": 24,
        "name": "Fashion & Beauty",
        "videos": [
            {
                "duration": 21334,
                "id": 29,
                "name": "Glamorous Touch",
                "preview": images.BiodtvGlamorous,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Glamorous+Touch.mp4"

            },
            {
                "duration": 21334,
                "id": 30,
                "name": "Lips Glamorous Red",
                "preview": images.BiodtvLips,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Lips+Glamorous+Red.mp4"

            },
            {
                "duration": 21334,
                "id": 31,
                "name": "Lips Natural",
                "preview": images.BiodtvNatural,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Lips+Natural.mp4"

            },
            {
                "duration": 21334,
                "id": 32,
                "name": "Lips Sparkling and Full",
                "preview": images.BiodtvSparkling,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Lips+Sparkling+and+Full.mp4"

            },
            {
                "duration": 21334,
                "id": 33,
                "name": "Make-Up Blush",
                "preview": images.BiodtvBlushm,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Make-Up+Blush.mp4"

            },
            {
                "duration": 21334,
                "id": 34,
                "name": "Make-Up Bronzer",
                "preview": images.BiodtvBronzer,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Make-Up+Bronzer.mp4"

            },
            {
                "duration": 21334,
                "id": 35,
                "name": "Make-Up Concealer",
                "preview": images.BiodtvAConcealer,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Make-Up+Concealer.mp4"

            },
            {
                "duration": 21334,
                "id": 36,
                "name": "Make-Up Eyebrows",
                "preview": images.BiodtvEyebrows,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Make-Up+Eyebrows.mp4"

            },
            {
                "duration": 21334,
                "id": 37,
                "name": "Make-Up Foundation",
                "preview": images.BiodtvFoundation,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Make-Up+Foundation.mp4"

            },
            {
                "duration": 21334,
                "id": 38,
                "name": "Smokey Eyes",
                "preview": images.BiodtvSmokey,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Smokey+Eyes.mp4"

            },
            {
                "duration": 21334,
                "id": 39,
                "name": "Sparkling Eyes",
                "preview": images.BiodtvSparklingEyes,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Fashion+%26+Beauty/Sparkling+Eyes.mp4"

            },
        ]
    },
    {
    "id" : 5,
        "contents" : 10,
        "name" : "E-Learning",
        "videos" : [
            {
                "duration" : 21334,
                "id" : 40,
                "name": "Learn Arabic Ep1 - How to introduce yourself",
                "preview" : images.BiodtvArabic,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Arabic+Ep1+-+How+to+introduce+yourself.mp4"

            },
            {
                "duration" : 21334,
                "id" : 41,
                "name": "Learn Arabic Ep2 - Thank you",
                "preview" : images.BiodtvArabic2,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Arabic+Ep2+-+Thank+you.mp4"

            },
            {
                "duration" : 21334,
                "id" : 42,
                "name": "Learn English Ep3 - Asking About Hobbies",
                "preview" : images.BiodtvEng,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+English+Ep3+-+Asking+About+Hobbies.mp4"

            },
            {
                "duration" : 21334,
                "id" : 43,
                "name": "Learn English Ep4 - Where are you from",
                "preview" : images.BiodtvEng2,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+English+Ep4+-+Where+are+you+from.mp4"

            },
            {
                "duration" : 21334,
                "id" : 44,
                "name": "Learn French Ep5 - How to Apologize",
                "preview" : images.BiodtvEng3,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+French+Ep5+-+How+to+Apologize.mp4"

            },
            {
                "duration" : 21334,
                "id" : 45,
                "name": "Learn French Ep6 - Asking do you Speak English",
                "preview" : images.BiodtvEng4,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+French+Ep6+-+Asking+do+you+Speak+English.mp4"

            },
            {
                "duration" : 21334,
                "id" : 46,
                "name": "Learn Italian Ep7 - Apologies",
                "preview" : images.BiodtvItalian,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Italian+Ep7-+Apologies.mp4"

            },
            {
                "duration" : 21334,
                "id" : 47,
                "name": "Learn Italian Ep8 - How much",
                "preview" : images.BiodtvItalian1,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Italian+Ep8-how+much.mp4"

            },
            {
                "duration" : 21334,
                "id" : 48,
                "name": "Learn Spanish Ep9 - Where are you from",
                "preview" : images.BiodtvASpanish,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Spanish+Ep9+-+Where+are+you+from.mp4"

            },
            {
                "duration" : 21334,
                "id" : 49,
                "name": "Learn Spanish Ep10 - What's up",
                "preview" : images.BiodtvASpanish1,
                "desc" : "",
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/E-Learning/Learn+Spanish+Ep10+-+What's+up.mp4"

            },
        ]
    },
    {
        "id": 6,
        "contents": 11,
        "name": "Yoga",
        "videos": [
            {
                "duration": 21334,
                "id": 50,
                "name": "How to Post Natal Yoga Breathing Exercise",
                "preview": images.BiodtvBreathing,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+Post+Natal+Yoga++Breathing+Exercise.mp4"

            },
            {
                "duration": 21334,
                "id": 51,
                "name": "Howto Post Natal Yoga Breathing Exercise",
                "preview": images.BiodtvNatal,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+Post+Natal+Yoga++Chair+Exercises.mp4"

            },
            {
                "duration": 21334,
                "id": 52,
                "name": "How to Post Natal Yoga Twist Walk",
                "preview": images.BiodtvNatal1,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+Post+Natal+Yoga++Twist+Walk.mp4"

            },
            {
                "duration": 21334,
                "id": 53,
                "name": "How to in 60 seconds",
                "preview": images.Biodtv60,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+in+60+seconds.mp4"

            },
            {
                "duration": 21334,
                "id": 54,
                "name": "How to post natal yoga - Dandasana",
                "preview": images.BiodtvDandasana,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+post+natal+yoga+-+Dandasana.mp4"

            },
            {
                "duration": 21334,
                "id": 55,
                "name": "How to post natal yoga - If baby needs entertainment",
                "preview": images.Biodtvbaby,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/How+to+post+natal+yoga+-+If+baby+needs+entertainment.mp4"

            },
            {
                "duration": 21334,
                "id": 56,
                "name": "Pilates  Breast stroke",
                "preview": images.Biodtvstroke,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/Pilates++Breast+stroke.mp4"

            },
            {
                "duration": 21334,
                "id": 57,
                "name": "Pilates  Half roll back",
                "preview": images.BiodtvPilates,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/Pilates++Half+roll+back.mp4"

            },
            {
                "duration": 21334,
                "id": 58,
                "name": "Pilates  Hip roll",
                "preview": images.BiodtvPilates1,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/Pilates++Hip+roll.mp4"

            },
            {
                "duration": 21334,
                "id": 59,
                "name": "Pilates  Intro push-up",
                "preview": images.BiodtvPilates2,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Yoga/Pilates++Intro+push-up.mp4"

            },
        ]
    },
    {
        "id": 7,
        "contents": 11,
        "name": "Secret Recipes",
        "videos": [
            {
                "duration": 21334,
                "id": 60,
                "name": "Cold Sesame Noodles",
                "preview": images.BiodtvSecret,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Cold+Sesame+Noodles.mov"

            },
            {
                "duration": 21334,
                "id": 61,
                "name": "Corn bread Stuffing",
                "preview": images.BiodtvRecipes,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Corn+bread+Stuffing.mov"

            },
            {
                "duration": 21334,
                "id": 62,
                "name": "Roasted Winter Vegetable",
                "preview": images.BiodtvRoasted,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Roasted+Winter+Vegetable.mov"

            },
            {
                "duration": 21334,
                "id": 63,
                "name": "Salmon Cakes Remoulade",
                "preview": images.BiodtvSalmon,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Salmon+Cakes+Remoulade.mov"

            },
            {
                "duration": 21334,
                "id": 64,
                "name": "Scalloped Potatoes",
                "preview": images.BiodtvScalloped,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Scalloped+Potatoes.mov"

            },
            {
                "duration": 21334,
                "id": 65,
                "name": "Smoked Salmon Appetizer",
                "preview": images.BiodtvSmoked1,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Smoked+Salmon+Appetizer.mov"

            },
            {
                "duration": 21334,
                "id": 66,
                "name": "Spicy Stuffed A corn Squash",
                "preview": images.BiodtvSpicy,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Spicy+Stuffed+A+corn+Squash.mov"

            },
            {
                "duration": 21334,
                "id": 67,
                "name": "Stuffed Mushrooms",
                "preview": images.BiodtvMushrooms,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Stuffed+Mushrooms.mov"

            },
            {
                "duration": 21334,
                "id": 68,
                "name": "Sugar Cookies",
                "preview": images.BiodtvCookies,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Sugar+Cookies.mov"

            },
            {
                "duration": 21334,
                "id": 69,
                "name": "Sugar Glazed Pecans",
                "preview": images.BiodtvPecans,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Food+Recipes/Sugar+Glazed+Pecans.mov"

            },
        ]
    },
    {
        "id": 8,
        "contents": 24,
        "name": "Just For Laugh",
        "videos": [
            {
                "duration": 21334,
                "id": 70,
                "name": "Animals Exoticos",
                "preview": images.BiodtvAnimals,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Animals+Exoticos.mp4"

            },
            {
                "duration": 21334,
                "id": 71,
                "name": "Automatu",
                "preview": images.BiodtvAutomatu,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Automatu.mp4"

            },
            {
                "duration": 21334,
                "id": 72,
                "name": "Babies and Moving Table Part 1",
                "preview": images.BiodtvTable,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Babies_and_Moving_Table_Part+1.mp4"

            },
            {
                "duration": 21334,
                "id": 73,
                "name": "Bad Kids Throw Orange Peels Part 1",
                "preview": images.BiodtvLaugh,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Bad_KidsThrow_Orange_Peels_Part+1.mp4"

            },
            {
                "duration": 21334,
                "id": 74,
                "name": "Big Bunny Scares Babies Part 1",
                "preview": images.BiodtvBunny,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Big+Bunny+Scares+Babies+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 75,
                "name": "Blind Man Groping Part 1",
                "preview": images.BiodtvBlind,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Blind+Man+Groping+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 76,
                "name": "Bodyguards",
                "preview": images.BiodtvBodyguards,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Bodyguards.mp4  "

            },
            {
                "duration": 21334,
                "id": 77,
                "name": "Cake on Face",
                "preview": images.BiodtvCake,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Cake+on+Face.mp4"

            },
            {
                "duration": 21334,
                "id": 78,
                "name": "Can I Click your photo",
                "preview": images.BiodtvClick,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Can+I+Click+your+photo.mp4"

            },
            {
                "duration": 21334,
                "id": 79,
                "name": "Car Parking",
                "preview": images.BiodtvParking,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Car+Parking.mp4"

            },
            {
                "duration": 21334,
                "id": 80,
                "name": "Car for Sale",
                "preview": images.BiodtvSale,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Car+for+Sale.mp4"

            },
            {
                "duration": 21334,
                "id": 81,
                "name": "Man Falling on Ladder",
                "preview": images.BiodtvFalling,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Man+Falling+on+Ladder.mp4"

            },
            {
                "duration": 21334,
                "id": 82,
                "name": "Man in Garbage Can Part 1",
                "preview": images.BiodtvGarbage,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Man+in+Garbage+Can+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 83,
                "name": "Painting Exhibition",
                "preview": images.BiodtvExhibition,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/PAinting+Exhibition.mp4"

            },
            {
                "duration": 21334,
                "id": 84,
                "name": "Pervert with Handy Cam Part 1",
                "preview": images.BiodtvPervert,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Pervert+with+Handy+Cam+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 85,
                "name": "Reckless Blind Man Part 1",
                "preview": images.BiodtvBlind1,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Reckless+Blind+Man+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 86,
                "name": "Roller Blader Part 1",
                "preview": images.BiodtvABlader,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Roller+Blader+Part1.mp4"

            },
            {
                "duration": 21334,
                "id": 87,
                "name": "Rude Man on Beach Part 1",
                "preview": images.BiodtvRude,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/Rude_Man_on_Beach_Part+1.mp4"

            },
            {
                "duration": 21334,
                "id": 88,
                "name": "Santa Claus The Pervert Part 1",
                "preview": images.BiodtvSantaCluas,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/SantaCluas_The_Pervert_Part+1.mp4"

            },
            {
                "duration": 21334,
                "id": 89,
                "name": "Baby Carier on Car",
                "preview": images.BiodtvJust,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/baby+carier+on+car.mp4"

            },
            {
                "duration": 21334,
                "id": 90,
                "name": "Bleeding Nose",
                "preview": images.Biodtvbleeding,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/bleeding+nose.mp4"

            },
            {
                "duration": 21334,
                "id": 91,
                "name": "Money Falling",
                "preview": images.Biodtvfalling,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/money+falling.mp4"

            },
            {
                "duration": 21334,
                "id": 92,
                "name": "Open This Pen",
                "preview": images.Biodtvpen,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Just+For+Laugh/open+this+pen.mp4"

            },
        ]
    },
    {
        "id": 9,
        "contents": 24,
        "name": "Brain Boost",
        "videos": [
            {
                "duration": 21334,
                "id": 93,
                "name": "Ditch the doctor! How about diagnosis by computer",
                "preview": images.BiodtvBrain,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Ditch+the+doctor!+How+about+diagnosis+by+computer.mp4"

            },
            {
                "duration": 21334,
                "id": 94,
                "name": "ESPN's new deal dives into the world of drone racing",
                "preview": images.BiodtvESPN,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/ESPN+s+new+deal+dives+into+the+world+of+drone+racing.mp4"

            },
            {
                "duration": 21334,
                "id": 95,
                "name": "Ever wished beer could fly Now it can!",
                "preview": images.BiodtvBoost,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Ever+wished+beer+could+fly+Now+it+can!.mp4"

            },
            {
                "duration": 21334,
                "id": 96,
                "name": "Hearing things Here are 5 facts on our inner voice",
                "preview": images.BiodtvHearing,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Hearing+things+Here+are+5+facts+on+our+inner+voice.mp4"

            },
            {
                "duration": 21334,
                "id": 97,
                "name": "Is spotty skin the answer to looking younger",
                "preview": images.Biodtvspotty,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Is+spotty+skin+the+answer+to+looking+younger.mp4"

            },
            {
                "duration": 21334,
                "id": 98,
                "name": "Just why are the Dutch so tall",
                "preview": images.BiodtvDutch,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Just+why+are+the+Dutch+so+tall.mp4"

            },
            {
                "duration": 21334,
                "id": 99,
                "name": "Meet the robot that wants to get to know you",
                "preview": images.Biodtvrobot,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Meet+the+robot+that+wants+to+get+to+know+you.mp4"

            },
            {
                "duration": 21334,
                "id": 100,
                "name": "Star Wars becomes reality in a post-apocalyptic mall",
                "preview": images.BiodtvStar,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Star+Wars+becomes+reality+in+a+post-apocalyptic+mall.mp4"

            },
            {
                "duration": 21334,
                "id": 101,
                "name": "Sugar tax The answer to obesity pandemic",
                "preview": images.Biodtvanswer,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Sugar+tax++The+answer+to+obesity+pandemic.mp4"

            },
            {
                "duration": 21334,
                "id": 102,
                "name": "Swallow this nano chip to know what s making you sick",
                "preview": images.Biodtvnano,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Swallow+this+nano+chip+to+know+what+s+making+you+sick.mp4"

            },
            {
                "duration": 21334,
                "id": 103,
                "name": "The exploding cost of cyberattacks",
                "preview": images.Biodtvexploding,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/The+exploding+cost+of+cyberattacks.mp4"

            },
            {
                "duration": 21334,
                "id": 104,
                "name": "Turns out emails didn't solve our waste paper problems",
                "preview": images.Biodtvsolve,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/Turns+out+emails+didn+t+solve+our+waste+paper+problems.mp4"

            },
            {
                "duration": 21334,
                "id": 105,
                "name": "UK Drone Show The latest consumer industry trends",
                "preview": images.BiodtvDrone,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/UK+Drone+Show++The+latest+consumer+industry+trends.mp4"

            },
            {
                "duration": 21334,
                "id": 106,
                "name": "VR Running of the Bulls! A fearful thrillseekers dream",
                "preview": images.BiodtvRunning,
                "desc": "",
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/bidiotv/Brain+Boost/VR+Running+of+the+Bulls!+A+fearful+thrillseekers+dream.mp4"

            },
        ]
    },
];