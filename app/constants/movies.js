import * as images from '../assets/Images';
export let movies = [
    {
        "id" : 1,
        "contents" : 11,
        "name" : "Arabic Movies",
        "videos" : [
            {
                "duration" : 21334,
                "id" : 6,
                "name" : "7alawet Roo7",
                "preview" : images.ArabicMovies1,
                "desc" : "تدور قصة الفيلم حول روح (هيفاء وهبي) التي تعيش بحي (بولاق الدكرور) برفقة والدة زوجها أم فاروق حيث أن زوجها مسافر خارج البلاد. تتعرض روح للعديد من المتاعب بسبب جيرانها. بخاصة من جارها الذي يطمع في الحصول عليها، بالإضافة إلى محاولات شخص أخر يقوم بالضغط عليها بكافة السبل المشروعة وغير المشروعة من أجل الحصول عليها لكنها تقف له بالمرصاد. يتم نصب فخ لها على يد اثنان من جيرانها (باسم سمرة) و(محمد لطفي) من أجل تحقيق أطماعهما."
            },
            {
                "duration" : 21334,
                "id" : 7,
                "name" : "Shad Ajza2",
                "preview" : images.ArabicMovies2,
                "desc" : "في إطار حركي، تدور أحداث الفيلم حول ضابط الشرطة عمر العطار (محمد رمضان) الذي يتعرض لموقف يقلب حياته المهنية والشخصية رأسًا على عقب، عندما يتم قتل زوجته آية (دنيا سمير غانم) على يد إحدى العصابات، فيسعى للثأر لها، وهو الأمر الذي يدخله في سلسلة من المشكلات المتتالية مع رؤساءه، ويصبح مطاردًا من قبل قوات الأمن."
            },
            {
                "duration" : 21334,
                "id" : 8,
                "name" : "7amati Bet7ebni",
                "preview" : images.ArabicMovies3,
                "desc" : "تدور أحداث الفيلم حول أحد الشباب (حمادة هلال) ، الذي يعمل طبيب تجميل ، يقع في العديد من المشاكل بسبب حبه الشديد للنساء ، قبل أن يقرر الخضوع للعلاج النفسي ، وتتوالى الأحداث إلى أن تكتشف طبيبته النفسية (ميرفت أمين) أنه على علاقة بابنتها (إيمان العاصي) ، فتحاول الوقوف أمام استمرار هذه العلاقة بشتى الطرق الممكنة."
            },
            {
                "duration" : 21334,
                "id" : 9,
                "name" : "3eyal 7arrifa",
                "preview" : images.ArabicMovies4,
                "desc" : "يعمل سباعي مكرونة (محمد لطفي) مدربًا لكرة القدم في نادي الدقي الرياضي، لكنه لا يفلح في صنع أي انجازات تذكر في مسيرته، وهو ما يجعل حبشي غير موافق على زواجه من ابنته أوشا (بوسي)، لكن تتبقى له فرصة أخيرة لاثبات ذاته، وهو أن يتولى مسئولية تدريب فريق كرة القدم النسائية، لكنه يكتشف الكثير من التلاعب والفساد من قبل أحد النافذين في إتحاد كرة القدم."
            },
            {
                "duration" : 21334,
                "id" : 10,
                "name" : "3omar w Salma 3",
                "preview" : images.ArabicMovies5,
                "desc" : "فى هذا الجزء يستمر عمر فى تصرفاته الطائشة وعدم العمل فى وظيفة، ويقرر العمل كمطرب شعبى وترفض سلمى ذلك وترفض ايضا ملاحقته للبنات. فتقرر خلعه وتنجح فى ذلك بمساعدة بناتهما؛ فيقرر عمر عقابها بادعائه خطوبة فتاة على درجة عالية من الجمال، واختطاف البنتين لتأديب سلمى. فتكون المفاجأة ان والد عمر قد لقن الاثنين درسا حتى لا يتلاعبان بالطلاق والخلع مرة اخرى."
            },
            {
                "duration" : 21334,
                "id" : 11,
                "name" : "3shan 5arjeenv",
                "preview" : images.ArabicMovies6,
                "desc" : "رمزي الدسوقي شاب مستهتر في علاقاته النسائية، يقرر رئيسه في العمل تعريفه على ابنته ليلى مراد بغرض تزويجها منه والخلاص من مشاكلها التي لا تنتهي، ومع لقائهما، يترك رجل مجهول حقيبة مليئة باﻷموال على طاولتهما، وهو ما يدخلهما في سلسلة لا تنتهي من المطاردات والمغامرات الكوميدية مع صاحب الحقيبة."
            },
            {
                "duration" : 21334,
                "id" : 12,
                "name" : "7amlet Frezer",
                "preview" : images.ArabicMovies7,
                "desc" : "تتبدل الحالة المناخية لمصر لتتحول إلى صقيع تام، ويقرر مجموعة من ضباط المخابرات السفر إلى إيطاليا في مهمة للحصول على جهاز يمتلك القدرة على إيقاف الصقيع، وينتحلوا شخصيات فريق عمل سينمائي لكي يتظاهروا بتصوير فيلم، ويتعاملوا مع ممثل فاشل يدعى مديح البلبوصي، كما يتعاونوا مع فتاة لديها سوابق في اﻵداب تدعى تباهي."
            },
            {
                "duration" : 21334,
                "id" : 13,
                "name" : "Ahwak",
                "preview" : images.ArabicMovies8,
                "desc" : "شريف قابيل (تامر حسني) طبيب متخصص في جراحات التجميل، يتعرف عن طريق شقيقته نجوى على شابة صغيرة في السن تدعى بسنت ويعجب بها، ولكنه عندما تسنح له الفرصة لمقابلة والدتها العزباء رنا (غادة عادل) والتواجد معها في نفس المكان لفترة، يزداد إعجابه بالأم، فيحاول طيلة الوقت أن يوقعها في حبه خلال تواجده معها بالغردقة بدعوة من الابنة."
            },
            {
                "duration" : 21334,
                "id" : 14,
                "name" : "Bonne Soiree",
                "preview" : images.ArabicMovies9,
                "desc" : "يدور الفيلم حول قضية الميراث، من خلال ثلاث فتيات، خريجة حقوق واختها خريجة دار العلوم وأخرى نرجسية تركت دراستها بكلية التجارة وتزوجت وفشلت ووالدهن. حيث يرثن تركة كبيرة عبارة عن كباريه يدعى بون سواريه, وتتولى اثنتان منهن (غادة عبد الرازق ونهلة ذكي) ادارته بسعادة، حيث يحقق لهما نقلة نوعية على المستوى المادي، وسط اعتراض ونصيحة أختهما الثالثة المتدينة (مي كساب) بأن يتخلصا من هذا الملهى الليلي، إلا أنهما عنيدتان وترفضان تقبل نصيحتها، بل وتقدم كل منهما فقرة في هذا الكباريه. ونتيجة دخولهما هذا العالم الجديد عليهما تتعرضان لمشاكل كثيرة، تنتهي بتركهما هذا المكان والبحث عن عمل شريف."
            },
            {
                "duration" : 21334,
                "id" : 15,
                "name" : "Samir Abu El Neel",
                "preview" : images.ArabicMovies10,
                "desc" : "في إطار كوميدي تدور أحداث الفيلم حول الشاب البخيل سمير أبو النيل (أحمد مكي) الذي يقطن بحي شعبي، ونتيجة لبخله الشديد تقع له العديد من المفارقات والمشاكل مع أهل منطقته، ويزيد من كرههم له لسوء معاملته لهم، وبين ليلة وضحاها يمرض ابن عمه حسين أبو النيل (حسين اﻹمام) ويقرر أن يترك ثروته لسمير الذي يستغل ذلك ويقوم بإنشاء قناة فضائية يناقش فيها أحواله وعلاقاته بأصدقائه وأهل منطقته.. تتوالى الأحداث في سياق كوميدي بعد إنشاء حزب سياسي يدعو له المواطنين."
            },
            {
                "duration" : 21334,
                "id" : 16,
                "name" : "Zanget Setat",
                "preview" : images.ArabicMovies11,
                "desc" : "(على منير الجحش) شاب ثري يتصيد الفتيات، وخاصة من أولئك اللاتي يزرن العيادة النفسية لأبيه زير النساء هو الآخر، وبعد خلاف (علي) مع والده حول إحدى المريضات، يدخل كلاهما في رهان على مصنع يملكه الأب في (انجلترا)، حيث يتوجب عليه أن يساعد مريضا لديه بالشفاء من عقدته مع النساء بأن يخدع ثلاث فتيات تعرف عليهن المريض ورفضوا حبه، وهن: (سهام بدر)، و(شكرية تورتة)، و(سميحة العو)"
            },
    
        ]
    },
    {
        "id" : 2,
        "contents" : 11,
        "name" : "Documentaries",
        "videos" : [
            {
                "duration" : 21334,
                "id" : 17,
                "name" : "Inside the Milky Way",
                "preview" : images.Documentaries1,
                "desc" : "نظامنا الشمسي يقع على مشارف مدينة سماوية واسعة، مجتمع ضخم من النجوم المرتبطة مع بعضها بقوانين الجاذبية فيما بعرف بمجرة درب الحليب. في يومنا هذا، يحاول علماء الفلك بالتعاون مع بعضهم البعض تفكيك الأحجية التي ستسمح لهم برؤية مجرتنا بطريقة جديدة ومثيرة للاهتمام تسمح بنا بأن نأخذ كوكبنا \"الأرض\" في رحلة مذهلة في مجاهل الفضاء الخارجي والزمن، كي نرى كيف تبدو الأحياء الأخرى المختلفة من مدينتنا النجمية الواسعة لو قمنا بزيارتها عن قرب!؟مصور ومعد باحتراف وأناقة، ومن خلال تلك المقابلات الودية مع العلماء الذين سيكشفون لنا متعة اكتشاف المفاهيم العلمية، سيروي لنا هذا العمل المتميز قصة مجرتنا بطريقة واضحة وسلسة وفوق كل ذلك ممتعة ومثيرة، فيما يلقي لنا نظرة ثاقبة على كيف تكونت -هي وكل ما فيها - مجرة درب الحليب."
            },
            {
                "duration" : 21334,
                "id" : 18,
                "name" : "Interfaces - Our Digital Life",
                "preview" : images.Documentaries2,
                "desc" : "لقد أصبح من شبه المستحيل أن يعمل أي شيء في يومنا هذا من دون الحواسيب. بشكل عملي، الحواسيب تتحكم في كل شيء عن طريق الشبكات، الاتصالات، وبروتوكولات إرسال واستقبال اي شيء رقمي. عالمنا يصبح شيئا فشيئا تحت سيطرة المعالجة الحسابية والبرامج. هذا الانتشار الرقمي الذي تغلغل في مفاصل حياتنا، لا يزال مستمرا في النمو أكثر فأكثر ولن يمر وقت طويل، على سبيل المثال، قبل أن تصبح كل الأجهزة المنزلية معتمدة على تكنولوجيا التحكم الرقمي بالكامل، أو حتى تغزو شوارع كاليفورنيا أول سيارة تقود نفسها ذاتيا في عام 2018. كم من \"الرقمنة\" تحتاج البشرية؟ وكم من \"الأنسنة\" تحتاج الحياة الرقمية؟ يضيء لنا هذا العمل الوثائقي المميز مختلف جوانب البيئة الرقمية ويروي لنا قصتها من خلال أربعة أجيال مختلفة مرت بها التكنولوجيا الرقمية.."
            },
            {
                "duration" : 21334,
                "id" : 19,
                "name" : "Open Skies",
                "preview" : images.Documentaries3,
                "desc" : "ما الذي قد يكون مثيراً أكثر من التحليق إلى أراض بعيدة وغامضة؟ ماذا لو أن هذه الرحلة تخللتها مناظر مذهلة من الأعلى للطبيعة الخلابة في الأسفل، متبوعة بمقابلات استثنائية مع سكان تلك المناطق المذهلة؟ من اللقطات الجوية التي تخطف الأنفاس للمساحات الخلابة من الطبيعة مترامية الأطراف إلى تلك اللقاءات والمقابلة مع الناس الذين يشغلونها ويعيشون فيها، هذا البرنامج الفريد في فكرته يأخذنا بشكل مقرب إلى ست أماكن مختلفة من العالم: المغرب، آيسلند، كامبوديا، الأردن، نيوزيلند وتونس ليكشف لنا هذه الأرواح والشخصيات المذهلة التي تشغل هذه الأماكن والعوالم البديعة."
            },
            {
                "duration" : 21334,
                "id" : 20,
                "name" : "China and the Change in the Global System",
                "preview" : images.Documentaries4,
                "desc" : "مقابلة حصرية من جزئين مع الدبلوماسي الفرنسي المتخصص في الشؤون الصينية ليونيل فيرون ، تتناول المقابلة الصين والتغيرات التي تطال النظام العالمي، في ظل تراجع القوة الأميركية. يطلعنا فيرون على رؤيته لتحول النظام العالمي، وتشكل أقطاب جديدة قادرة على منافسة الولايات المتحدة، مستنداً إلى تجربته الكبيرة في كواليس الدبلوماسية الفرنسية، كما يناقش تأثير الولايات المتحدة على تقارب الصين وروسيا. تنعكس التغيرات العالمية هذه على الشرق الأوسط الذي يعيش صراعاً غير بعيد عن تدخلات الدول الكبرى"
            },
            {
                "duration" : 21334,
                "id" : 21,
                "name" : "Coca Cola - The Secret Formula",
                "preview" : images.Documentaries5,
                "desc" : "ترويجاً لشعارها الأحمر والأبيض، تبيع شركة كوكاكولا حوالي المليار والنصف المليار عبوة شراب غازي يومياً. هدف الشركة هو مضاعفة تلك الانتاجية في حلول العام ألفين وعشرين. تنبع قصة النجاح هذه على سر، وهي تركيبة مميزة تم ابتكارها في العام ألف وثمانمئة وستة وثمانون من قبل صيدلي أميركي في أتلانتا، سر قامت شركة الكوكاكولا بحفظه حتى يومنا هذا. ماذا يخبئ هذا السر؟"
            },
            {
                "duration" : 21334,
                "id" : 22,
                "name" : "Jordania 2000 years of History",
                "preview" : images.Documentaries6,
                "desc" : "عرف الموقع منذ إحتلاله في العصر الحجري الحديث \"النيوليتي\"، وتأسس للمرة الأولى على يد السلوقيين أي في العصر الهنلستي. ويعرف أن السلوقيين هم قدامى المحاربين في جيش اسكندر الكبير الذي- وفقا للأسطورة- كان قد غزا سوريا وأطلق على الموقع اسم \"جرسا\". سيطر الأنباط على جراسا، ومن ثم اليهود، فخضعت بذلك للحكم الروماني. وبعد أن غزاها القائد الروماني بومبي عام 63 قبل الميلاد، سقطت المدينة في أيدي الرومان، ومع دخولها في حلف \"الديكابوليس\" أصبحت واحدة من المدن العشرة الأساسية في الأمبراطورية الرومانية. وقد نمت ثروة جرش بفضل أعمالها التجارية وأنشطتها الزراعية في منطقة البحر الأبيض المتوسط. لكنها بلغت ذروتها في القرن الثاني أي في عهد الامبراطور هادريان الذي قضى فيها فترة من العام 129 ميلادي.شيد هارديان عدداً من مشاريع البناء مثل البوابة الجنوبية، وقوس النصر، ومعبد الإله آرتيموس الذي لايزال يحتفظ ببصمته المدهشة إلى يومنا هذا الأمر الذي جعل جرش ثاني أهم مدينة في المحافظة. وبحلول القرن الرابع إنتشرت الديانة المسيحية، فدخلت جرش لاحقاً في مرحلة جديدة من التطور حيث إنتعشت حركة تشييد الكنائس. في القرن السادس أي في عهد الإمبراطور جستنيان بنيت مجموعة من المباني منها الكاتدرائية، وكنسية القديسان كوزموس وداميان، وكنسية القديس يوحنا المعمدان، وكنسية القديس جورج، وكنيسة سانت تيودور وكلها تقع على مقربة من معبد أرتميس. إلا أن غزو الفرس والعرب ثم الأمويين تسبب بتراجع جرش.  وتبع ذلك ضربات من الزلازل المتلاحقة التي بدأت في القرن الثامن عشر وانتهت في القرن الثاني عشر مع نهاية الحروب الصليبية . وهكذا كانت جرش في حالة من الخراب وبقيت أنقاضها مطمورة في  التراب لكن آثارها بقيت شاهدة على عظمتها لقرون على الرغم من مرور الزمن."
            },
            {
                "duration" : 21334,
                "id" : 23,
                "name" : "Mediterranean Sub II",
                "preview" : images.Documentaries7,
                "desc" : "في هذه الرحلة نزور أكثر الأماكن المدهشة في البحر الأبيض المتوسط، ولكن هذه المرة إننا نصور بتقنية الـHD، سمك الإنش، الباراكودا، الانقليس، ثعابين البحر، أسماك الشِفنين العُقابيّ... معًا إضافة إلى العديد من الحيوانات الصغيرة الأخرى، التي لا تقل عنها جمالًا وإثارة للدهشة، مثل الدود البزاق، والروبيان ... حيوانات استعمارية مذهلة مثل المزقييات، ومرجان البحر الأبيض المتوسط​​، والمرجان الأحمر، والمرجان الأسود.... نغوص في عالم المروج والكهوف وحطام السفن، والمنحدرات ... بحر كان أصل الحضارات ومهد القدم والثقافة ومسكن أنواع مختلفة من الحيوانات البحرية. الحمامات الرومانية في (باموكال)، (تركيا)، والصيادين مع فخاخ جراد البحر في (أوستيكا)، (إيطاليا)، وآخر الفقمات المتبقية في (زاكينثوس)، (اليونان) هي أمثلة عن عجائب التقينا بها في هذه الرحلة... كل هذا وأكثر من ذلك بكثير في 26 حلقة مصورة بتقنية الـHD."
            },
            {
                "duration" : 21334,
                "id" : 24,
                "name" : "Nile's Sunken Treasures",
                "preview" : images.Documentaries8,
                "desc" : "على بُعد ثلاثمائة كيلومتر جنوبي القاهرة تقع مدينة أسوان بسكانها المئتي والخمسين ألفاً. هنا يدخل النيل مصر بعد رحلته الطويلة من أعماق أفريقيا. مصر هبة النيل...لقد جعلها النهر غنية وقوية. إنه أقدم طريق سفر في تاريخ البشرية. من شِبه المؤكد أنّ هناك كنوزاً أثرية غارقة في قعر النيل. سُفن بحمولتها الثمينة. لكن لم يقم أحدٌ من علماء الآثار بسبر أغوار مياه النهر...إلى حين بدء هذه المغامرة."
            },
            {
                "duration" : 21334,
                "id" : 25,
                "name" : "Of Sharks and Men",
                "preview" : images.Documentaries9,
                "desc" : "حتى في القرن الحادي عشر، ما زال أمامنا أمور كثيرة لنتعلمها عن عالمنا. تجذب أساطير البحر الإنسان، فينطلق في كل يوم ليستكشفها، ويقترب قدر المستطاع من أسماك القرش الكبيرة، هذه الحيوانات المفترسة المذهلة التي تحرّك أعمق مشاعر الخوف فينا. تتميز هذه الحيوانات الموجودة منذ عصور بسلوك غالباً ما يفتقر إلى تفسير، وهذا ما أثار فضول العلماء في أنحاء العالم كافة، فوضعوا هذه الحيوانات تحت مراقبة مكثفة منذ الحرب العالمية الثانية. أما الرجل الوحيد الذي قرّر أن يقترب منها فهو \"غراهام دوين\"، فكان على مسافة قصيرة منها من دون وسائل حماية. وكان وجهاً لوجه مع مفترسيْن خارقيْن سنتمكّن من خلالهما أن نتعرف إلى القرش الكبير الذائع الصيت. لذلك، قبل أن تختفي أسماك القرش وتذهب ضحية البشر، آن الأوان اليوم لنتغلّب على مخاوفنا ونتعلم قدر المستطاع عن هذه الحيوانات الضخمة المكتنفة بالأسرار."
            },
            {
                "duration" : 21334,
                "id" : 26,
                "name" : "Voyage of the Continents",
                "preview" : images.Documentaries10,
                "desc" : "خلال مليارات الكثيرة من السنين في تاريخ الأرض، لم يتوقف كوكبنا عن تغيير شكله حيث أن قوة الصفائح التكتونية الهائلة قد صقلت وأعادت تشكيل عالمنا مراراً وتكراراً في رحلة لا نهائية.. لطالما خلقت الصفائح التكتونية الحياة على الأرض، ودمرتها أيضاً، فمن براكين هائلة إلى زلازل مدمرة، لا تزال حركات هذه الصفائح تهيمن على التاريخ البشري. هذه قصة دراما متقنة وجمال رفيع عن التفاعل مابين الجيولوجيا والحياة، إنها حقيقةً رحلة القارات!"
            },
            {
                "duration" : 21334,
                "id" : 27,
                "name" : "Voyage to the Deep",
                "preview" : images.Documentaries11,
                "desc" : "إن المحيطات تتغير بفعل تأثير تغير المناخ وممارسات الإنسان. حقيقة واحدة تؤكد على خطورة هذا التغيير: الأنواع الغازية، تلك التي تستعمر المساكن الطبيعية التي لا تعود لها، والآخذة في التزايد في جميع أنحاء العالم. ويعتبر الاتحاد العالمي للطبيعة الأنواع الغريبة الغازية واحدة من أكبر الأسباب الحالية لفقدان التنوع البيولوجي في العالم، وتشير إلى الخسائر الاقتصادية الفادحة حيث تتواجد. يتبع هذا الفيلم الوثائقي ستة من هذه الأنواع في محيطات مختلفة. الأسماك في جزر الكناري التي تعيش الآن في \"كابو دي بالوس\"، طحالب بحر الكاريبي التي تظهر في \"السيشيل\"، سمك موسى بحر الأبيض المتوسط ​​الذي يتكيف للعيش في \"المكسيك\" ويطرد أسماك الشبر الأصلية، نجم البحر الذي ازدادت أعداده فجأة ويهدد الشعاب المرجانية في \"تاهيتي\"... في هذا الفيلم الوثائقي الرائع، نزور أيضًا تلك البلدان بطريقة ممتعة وسهلة، ونكتشف أماكن جديدة وأساليب حياة جديدة."
            },

        ]
    },
    {
        "id": 3,
        "contents": 8,
        "name": "Arabic Entertainment",
        "videos": [
            {
                "id": 28,
                "name": "Hayda 7aki",
                "desc": "Weekly comedy-oriented talk and variety show in which Adel Karam hosts guests to often reveal the funny side of their personality.",
                "preview": images.ArEntertianment1,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3al-3ein.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory":
                    [
            /*{

                "id": 29,
                "name": "3al 3ein",
                "desc": "",
                "preview": images.hyda2,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 30,
                "name": "3ala remch 3younha",
                "desc": "",
                "preview": images.hyda3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3ala_remch_3younha.mp4",
                "duration": 1,

            },
            {
                "id": 31,
                "name": "3ali Lkoufiyi",
                "desc": "",
                "preview": images.hyda4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3ali_Lkoufiyi.mp4",
                "duration": 1,

            },
            {
                "id": 32,
                "name": "3endak bahriya",
                "desc": "",
                "preview": images.hyda5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3endak_bahriya.mp4",
                "duration": 1,

            },
            {
                "id": 33,
                "name": "3endak Hiwaye",
                "desc": "",
                "preview": images.hyda6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3endak-Hiwaye.mp4",
                "duration": 1,

            },
            {
                "id": 34,
                "name": "3omri btada",
                "desc": "",
                "preview": images.hyda7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3omri_btada.mp4",
                "duration": 1,

            },
            {
                "id": 35,
                "name": "3tazalt Al gharam",
                "desc": "",
                "preview": images.hyda8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/3tazalt_Al_gharam.mp4",
                "duration": 1,

            },
            {
                "id": 36,
                "name": "Abouki min",
                "desc": "",
                "preview": images.hyda9,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 37,
                "name": "Ah Ya helou",
                "desc": "",
                "preview": images.hyda10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Abouki_min.mp4",
                "duration": 1,

            },
            {
                "id": 38,
                "name": "Alef W Miyeh",
                "desc": "",
                "preview": images.hyda11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Alef-W-Miyeh.mp4",
                "duration": 1,

            },
            {
                "id": 39,
                "name": "Amantelak lesh Tkhoun",
                "desc": "",
                "preview": images.hyda12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Amantelak_lesh_Tkhoun.mp4",
                "duration": 1,

            },
            {
                "id": 40,
                "name": "Amenli bayt",
                "desc": "",
                "preview": images.hyda13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Amenli_bayt.mp4",
                "duration": 1,

            },
            {
                "id": 41,
                "name": "Ana albi lk Mayal",
                "desc": "",
                "preview": images.hyda14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Ana_albi_lk_Mayal.mp4",
                "duration": 1,

            },
            {
                "id": 42,
                "name": "Arabiyon Ana",
                "desc": "",
                "preview": images.hyda15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Arabiyon-Ana.mp4",
                "duration": 1,

            },
            {
                "id": 43,
                "name": "Asamina",
                "desc": "",
                "preview": images.hyda16,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Asamina.mp4",
                "duration": 1,

            },
            {
                "id": 44,
                "name": "Awel Mara",
                "desc": "",
                "preview": images.hyda17,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Awel-Mara.mp4",
                "duration": 1,

            },
            {
                "id": 45,
                "name": "Bahebak AHH",
                "desc": "",
                "preview": images.hyda18,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Bahebak-AHH.mp4",
                "duration": 1,

            },
            {
                "id": 46,
                "name": "Baheb Fgharamak",
                "desc": "",
                "preview": images.hyda19,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Baheb_Fgharamak.mp4",
                "duration": 1,

            },
            {
                "id": 47,
                "name": "Bektob Esmak",
                "desc": "",
                "preview": images.hyda20,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Bektob_Esmak.mp4",
                "duration": 1,

            },
            {
                "id": 48,
                "name": "Helef Amar",
                "desc": "",
                "preview": images.hyda21,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Helef_Amar.mp4",
                "duration": 1,

            },
            {
                "id": 49,
                "name": "Kifon habayebna",
                "desc": "",
                "preview": images.hyda22,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Kifon_habayebna.mp4",
                "duration": 1,

            },
            {
                "id": 50,
                "name": "La3youni Gharibi",
                "desc": "",
                "preview": images.hyda23,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/La3youni_Gharibi.mp4",
                "duration": 1,

            },
            {
                "id": 51,
                "name": "Libnan",
                "desc": "",
                "preview": images.hyda24,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Libnan.mp4",
                "duration": 1,

            },
            {
                "id": 52,
                "name": "Mama ya Mama",
                "desc": "",
                "preview": images.hyda25,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Mama_ya_Mama.mp4",
                "duration": 1,

            },
            {
                "id": 53,
                "name": "Mawal Emmi",
                "desc": "",
                "preview": images.hyda26,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Mawal_Emmi.mp4",
                "duration": 1,

            },
            {
                "id": 54,
                "name": "Men hawn",
                "desc": "",
                "preview": images.hyda27,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Men_hawn.mp4",
                "duration": 1,

            },
            {
                "id": 55,
                "name": "Sabaya Bladi",
                "desc": "",
                "preview": images.hyda28,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Sabaya_Bladi.mp4",
                "duration": 1,


            },
            {
                "id": 56,
                "name": "Wala Mara",
                "desc": "",
                "preview": images.hyda29,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Wala_Mara.mp4",
                "duration": 1,

            },
            {
                "id": 57,
                "name": "Ya Em Dafayer",
                "desc": "",
                "preview": images.hyda30,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/starsmix/Ya_Em_Dafayer.mp4",
                "duration": 1,

            }
        ]
            },
            {
                "id": 60,
                "name": "Heik Men3'ani",
                "desc": "MTV redefines singing competitions with this new show featuring contestants trying to remember the lyrics to popular songs through various games. Hosted by Maya Diab.",
                "preview": images.ArEntertianment2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/3ajabtini.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory": 
        [
            /*{
                "id": 61,
                "name": "3ajabtini",
                "desc": "",
                "preview": images.heik1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 62,
                "name": "3allem Waladak",
                "desc": "",
                "preview": images.heik2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/3allem_Waladak.mp4",
                "duration": 1,
            },
            {
                "id": 63,
                "name": "3ataki 3omro",
                "desc": "",
                "preview": images.heik3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/3ataki_3omro.mp4",
                "duration": 1,
            },
            {
                "id": 64,
                "name": "3azzetni El Deni",
                "desc": "",
                "preview": images.heik4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/3azzetni_El_Deni.mp4",
                "duration": 1,
            },
            {
                "id": 65,
                "name": "3youn Bahiyya",
                "desc": "",
                "preview": images.heik5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/3youn_Bahiyya.mp4",
                "duration": 1,
            },
            {
                "id": 66,
                "name": "7abayt 3younak",
                "desc": "",
                "preview": images.heik6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/7abayt_3younak.mp4",
                "duration": 1,
            },
            {
                "id": 67,
                "name": "7aji Matar",
                "desc": "",
                "preview": images.heik7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/7aji_Matar.mp4",
                "duration": 1,
            },
            {
                "id": 68,
                "name": "7asibak",
                "desc": "",
                "preview": images.heik8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/7asibak.mp4",
                "duration": 1,
            },
            {
                "id": 69,
                "name": "7ekyou 3enayk",
                "desc": "",
                "preview": images.heik9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/7ekyou_3enayk.mp4",
                "duration": 1,
            },
            {
                "id": 70,
                "name": "Ahla Gharam",
                "desc": "",
                "preview": images.heik10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ahla_Gharam.mp4",
                "duration": 1,
            },
            {
                "id": 71,
                "name": "Ahla Lebnaniye",
                "desc": "",
                "preview": images.heik11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ahla_Lebnaniye.mp4",
                "duration": 1,
            },
            {
                "id": 72,
                "name": "Ahlik Ma Baddoun Yani",
                "desc": "",
                "preview": images.heik12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ahlik_Ma_Baddoun_Yani.mp4",
                "duration": 1,
            },
            {
                "id": 73,
                "name": "Al Kamar Al Sina3i",
                "desc": "",
                "preview": images.heik13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/al_kamar_al_sina3i.mp4",
                "duration": 1,
            },
            {
                "id": 74,
                "name": "Alawa",
                "desc": "",
                "preview": images.heik14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Alawa.mp4",
                "duration": 1,
            },
            {
                "id": 75,
                "name": "Allah Ma3ak",
                "desc": "",
                "preview": images.heik15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Allah_Ma3ak.mp4",
                "duration": 1,
            },
            {
                "id": 76,
                "name": "Allah Shou B7ebak",
                "desc": "",
                "preview": images.heik16,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Allah_Shou_B7ebak.mp4",
                "duration": 1,
            },
            {
                "id": 77,
                "name": "Ashouf Fik Youm",
                "desc": "",
                "preview": images.heik17,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ashouf_Fik_Youm.mp4",
                "duration": 1,
            },
            {
                "id": 78,
                "name": "Assem Shar2i Baladi",
                "desc": "",
                "preview": images.heik18,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Assem_Shar2i_Baladi.mp4",
                "duration": 1,
            },
            {
                "id": 79,
                "name": "Ba7ebak",
                "desc": "",
                "preview": images.heik19,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ba7ebak.mp4",
                "duration": 1,
            },
            {
                "id": 80,
                "name": "Bawastik",
                "desc": "",
                "preview": images.heik20,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Bawastik.mp4",
                "duration": 1,
            },
            {
                "id": 81,
                "name": "Bez3al Menak",
                "desc": "",
                "preview": images.heik21,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Bez3al_Menak.mp4",
                "duration": 1,
            },
            {
                "id": 82,
                "name": "Bfakker Fik",
                "desc": "",
                "preview": images.heik22,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Bfakker_Fik.mp4",
                "duration": 1,
            },
            {
                "id": 83,
                "name": "Bourdayik Ya Bordana",
                "desc": "",
                "preview": images.heik23,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Bourdayik_Ya_Bordana.mp4",
                "duration": 1,
            },
            {
                "id": 84,
                "name": "Bto2mor 3al Rass w 3al 3ayn",
                "desc": "",
                "preview": images.heik24,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/bto2mor_3al_rass_w_3al_3ayn.mp4",
                "duration": 1,
            },
            {
                "id": 85,
                "name": "Byeb3at Allah",
                "desc": "",
                "preview": images.heik25,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Byeb3at_Allah.mp4",
                "duration": 1,
            },
            {
                "id": 86,
                "name": "Byeb3at Allah Kbir",
                "desc": "",
                "preview": images.heik26,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Byeb3at_Allah_Kbir.mp4",
                "duration": 1,
            },
            {
                "id": 87,
                "name": "Chou Ba3melik",
                "desc": "",
                "preview": images.heik27,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/chou_ba3melik.mp4",
                "duration": 1,
            },
            {
                "id": 88,
                "name": "Dakhel El Ghannouj",
                "desc": "",
                "preview": images.heik28,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Dakhel_El_Ghannouj.mp4",
                "duration": 1,
            },
            {
                "id": 89,
                "name": "Ento Al Banat",
                "desc": "",
                "preview": images.heik29,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Ento_Al_Banat.mp4",
                "duration": 1,
            },
            {
                "id": 90,
                "name": "Enzel Ya Gamil",
                "desc": "",
                "preview": images.heik30,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/heikmenghani/Enzel_Ya_Gamil.mp4",
                "duration": 1,
            },

        ]
            },
            {
                "id": 91,
                "name": "Kifni Ma3ak",
                "desc": "Michel Ashi does it again, this time funnier than ever! Candid camera, tricks, twisted interviews, hilarious Vox pop. Expect the best of the worst with a complete new series.",
                "preview": images.ArEntertianment3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_3am_yekhod_cadeaux01.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory": 
        [
            /*{
                "id": 92,
                "name": "Kifne Ma3ak - 3am Yekhod Cadeaux 01",
                "desc": "",
                "preview": images.kifne1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 93,
                "name": "Kifne Ma3ak - 3am Yekhod Cadeaux 02",
                "desc": "",
                "preview": images.kifne2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_3am_yekhod_cadeaux02.mp4",
                "duration": 1,
            },
            {
                "id": 94,
                "name": "Kifne Ma3ak - 3am Yekhod Cadeaux 02",
                "desc": "",
                "preview": images.kifne3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_3am_yekhod_cadeaux2.mp4",
                "duration": 1,
            },
            {
                "id": 95,
                "name": "Kifne Ma3ak - 7ake Bati 201",
                "desc": "",
                "preview": images.kifne4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_7ake_bati201.mp4",
                "duration": 1,
            },
            {
                "id": 96,
                "name": "Kifne Ma3ak - 7ake Bati 202",
                "desc": "",
                "preview": images.kifne5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_7ake_bati202.mp4",
                "duration": 1,
            },
            {
                "id": 97,
                "name": "Kifne Ma3ak - 7arame 01",
                "desc": "",
                "preview": images.kifne6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_7arame01.mp4",
                "duration": 1,
            },
            {
                "id": 98,
                "name": "Kifne Ma3ak - 7arame 02",
                "desc": "",
                "preview": images.kifne7,
                "videoUrl":"https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_7arame02.mp4",
                "duration": 1,
            },
            {
                "id": 99,
                "name": "Kifne Ma3ak - Andaf Zalame",
                "desc": "",
                "preview": images.kifne8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_andaf_zalame.mp4",
                "duration": 1,
            },
            {
                "id": 100,
                "name": "Kifne Ma3ak - Aya Ma3loumet 01",
                "desc": "",
                "preview": images.kifne9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_aya_ma3loumet01.mp4",
                "duration": 1,
            },
            {
                "id": 101,
                "name": "Kifne Ma3ak - Aya Ma3loumet 02",
                "desc": "",
                "preview": images.kifne10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_aya_ma3loumet02.mp4",
                "duration": 1,
            },
            {
                "id": 102,
                "name": "Kifne Ma3ak - Bakil Sobato",
                "desc": "",
                "preview": images.kifne11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_bakil_sobato.mp4",
                "duration": 1,
            },
            {
                "id": 103,
                "name": "Kifne Ma3ak - Best Name",
                "desc": "",
                "preview": images.kifne12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_best_name.mp4",
                "duration": 1,
            },
            {
                "id": 104,
                "name": "Kifne Ma3ak - Betheb Nemzah Ma3ak",
                "desc": "",
                "preview": images.kifne13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_betheb_nemzah_ma3ak.mp4",
                "duration": 1,
            },
            {
                "id": 105,
                "name": "Kifne Ma3ak - Bonjour",
                "desc": "",
                "preview": images.kifne14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_bonjour.mp4",
                "duration": 1,
            },
            {
                "id": 106,
                "name": "Kifne Ma3ak - Boxe 01",
                "desc": "",
                "preview": images.kifne15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_boxe01.mp4",
                "duration": 1,
            },
            {
                "id": 107,
                "name": "Kifne Ma3ak - Boxe 02",
                "desc": "",
                "preview": images.kifne16,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_boxe02.mp4",
                "duration": 1,
            },
            {
                "id": 108,
                "name": "Kifne Ma3ak - Chi M3alla2 Khayt",
                "desc": "",
                "preview": images.kifne17,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_chi_m3alla2_khayt.mp4",
                "duration": 1,
            },
            {
                "id": 109,
                "name": "Kifne Ma3ak - Chips 01",
                "desc": "",
                "preview": images.kifne18,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_chips01.mp4",
                "duration": 1,
            },
            {
                "id": 110,
                "name": "Kifne Ma3ak - Chips 02",
                "desc": "",
                "preview": images.kifne19,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_chips02.mp4",
                "duration": 1,
            },
            {
                "id": 111,
                "name": "Kifne Ma3ak - Chlite",
                "desc": "",
                "preview": images.kifne20,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/kifnema3ak/kifne_ma3ak_chlite.mp4",
                "duration": 1,
            },
        ]
            },
            {
                "id": 112,
                "name": "Dancing with the Stars",
                "desc": "Enjoy even more sizzling salsas, sambas and spray-tans as a brand-new cast vies for the coveted Mirror ball Trophy on Dancing with the Stars returns for Season 25! Tom Bergeron and Erin Andrews are back to guide us through this exciting new season along with your favourite judges. Of course, it's still all about the fans who ultimately help determine which teams make the cut each week with their votes during the biggest dance party on the planet!",
                "preview": images.ArEntertianment4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_chacha-amina-achraf.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory":
        [
            /*{
                "id": 113,
                "name": "DWTS - Chacha Amina Achraf",
                "desc": "",
                "preview": images.dwt1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 114,
                "name": "DWTS - Chacha Dalida Khalil",
                "desc": "",
                "preview": images.dwt2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_chacha-dalida-khalil.mp4",
                "duration": 1,
            },
            {
                "id": 115,
                "name": "DWTS - Chacha Jane Konsol",
                "desc": "",
                "preview": images.dwt3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_chacha-jane-konsol.mp4",
                "duration": 1,
            },
            {
                "id": 116,
                "name": "DWTS - Chacha Jean Paul Bitar",
                "desc": "",
                "preview": images.dwt4,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 117,
                "name": "DWTS - Foxtrot Anthony Touma",
                "desc": "",
                "preview": images.dwt5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_foxtrot-anthony-touma.mp4",
                "duration": 1,
            },
            {
                "id": 118,
                "name": "DWTS - Foxtrot Mohamed Attieh",
                "desc": "",
                "preview": images.dwt6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_foxtrot-mohamed-attieh.mp4",
                "duration": 1,
            },
            {
                "id": 119,
                "name": "DWTS - Salsa Anthony Touma",
                "desc": "",
                "preview": images.dwt7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_salsa-anthony-touma.mp4",
                "duration": 1,
            },
            {
                "id": 120,
                "name": "DWTS - Salsa Mohamed Attieh",
                "desc": "",
                "preview": images.dwt8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_salsa-mohamed-attieh.mp4",
                "duration": 1,
            },
            {
                "id": 121,
                "name": "DWTS - Salsa Rony Fahed",
                "desc": "",
                "preview": images.dwt9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_salsa-rony-fahed.mp4",
                "duration": 1,
            },
            {
                "id": 122,
                "name": "DWTS - Tango Carmen Lebbos",
                "desc": "",
                "preview": images.dwt10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_tango-carmen-lebbos.mp4",
                "duration": 1,
            },
            {
                "id": 123,
                "name": "DWTS - Tango Leila Ben Khalifa",
                "desc": "",
                "preview": images.dwt11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_tango-leila-ben-khalifa.mp4",
                "duration": 1,
            },
            {
                "id": 124,
                "name": "DWTS - Tango Reine Achkar",
                "desc": "",
                "preview": images.dwt12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_tango-reine-achkar.mp4",
                "duration": 1,
            },
            {
                "id": 125,
                "name": "DWTS - Tango Rima-Fakih",
                "desc": "",
                "preview": images.dwt13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_tango-rima-fakih.mp4",
                "duration": 1,
            },
            {
                "id": 126,
                "name": "DWTS - Waltz Gabriel Yammine",
                "desc": "",
                "preview": images.dwt14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_waltz-gabriel-yammine.mp4",
                "duration": 1,
            },
            {
                "id": 127,
                "name": "DWTS - Waltz Ibrahim Said",
                "desc": "",
                "preview": images.dwt15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/Dancing+With+The+Star/dwts_s3_waltz-ibrahim-said.mp4",
                "duration": 1,
            },
        ]
            },
            {
                "id": 128,
                "name": "I am a woman",
                "desc": null,
                "preview": images.ArEntertianment5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/iamwoman/Define_me_1.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory": 
        [
            /*{
                "id": 129,
                "name": "Define Me 1",
                "desc": "",
                "preview": images.woman1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 130,
                "name": "Define Me 2",
                "desc": "",
                "preview": images.woman2,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 131,
                "name": "Define Me 3",
                "desc": "",
                "preview": images.woman3,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 132,
                "name": "Define Me - Nutrition 1",
                "desc": "",
                "preview": images.woman4,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 133,
                "name": "Define Me - Nutrition 2",
                "desc": "",
                "preview": images.woman5,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 134,
                "name": "Define Me - Nutrition 3",
                "desc": "",
                "preview": images.woman6,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 135,
                "name": "Glam Me Up Jennifer Lopez 6",
                "desc": "",
                "preview": images.woman7,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 136,
                "name": "Glam Me Up Kelly Osbourne 3",
                "desc": "",
                "preview": images.woman8,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 137,
                "name": "Glam Me Up Kim Kardashian 1",
                "desc": "",
                "preview": images.woman9,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 138,
                "name": "Men Stripped 1",
                "desc": "",
                "preview": images.woman10,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 139,
                "name": "Men Stripped 2",
                "desc": "",
                "preview": images.woman11,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 140,
                "name": "Men Stripped 3",
                "desc": "",
                "preview": images.woman12,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 141,
                "name": "Out There 1",
                "desc": "",
                "preview": images.woman13,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 142,
                "name": "Out There 2",
                "desc": "",
                "preview": images.woman14,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 143,
                "name": "Out There 3",
                "desc": "",
                "preview": images.woman15,
                "videoUrl": "",
                "duration": 1,
            },
        ]
            },
            {
                "id": 144,
                "name": "Jamaluki m3 dima",
                "desc": "The best Arabic beauty trends with Dima, enjoy exploring the tips and tricks right on your mobile.",
                "preview": images.ArEntertianment6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Classy+smoky+look.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory":
        [
            /*{
                "id": 145,
                "name": "Classy Smoky Look",
                "desc": "",
                "preview": images.jama1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 146,
                "name": "Concealer",
                "desc": "",
                "preview": images.jama2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Concealer.mp4",
                "duration": 1,
            },
            {
                "id": 147,
                "name": "Contouring Techniques",
                "desc": "",
                "preview": images.jama3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Contouring+techniques.mp4",
                "duration": 1,
            },
            {
                "id": 148,
                "name": "Cream Foundation",
                "desc": "",
                "preview": images.jama4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Cream+foundation.mp4",
                "duration": 1,
            },
            {
                "id": 149,
                "name": "Daily Make Up (Gray)",
                "desc": "",
                "preview": images.jama5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Daily+make-up+(gray).mp4",
                "duration": 1,
            },
            {
                "id": 150,
                "name": "Daily Make Up (Pink)",
                "desc": "",
                "preview": images.jama6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Daily+make-up+(pink).mp4",
                "duration": 1,
            },
            {
                "id": 151,
                "name": "Daily Make Up (Violet)",
                "desc": "",
                "preview": images.jama7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Daily+make-up+(violet).mp4",
                "duration": 1,
            },
            {
                "id": 152,
                "name": "Daily Make Up",
                "desc": "",
                "preview": images.jama8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Daily+make-up.mp4",
                "duration": 1,
            },
            {
                "id": 153,
                "name": "Friday Hangout With Friends",
                "desc": "",
                "preview": images.jama9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Friday+hangout+with+friends.mp4",
                "duration": 1,
            },
            {
                "id": 154,
                "name": "How To Clean Your Fake Eyelashes",
                "desc": "",
                "preview": images.jama10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/How+to+clean+your+fake+eyelashes.mp4",
                "duration": 1,
            },
            {
                "id": 155,
                "name": "How To Make Up On Your Graduation Day",
                "desc": "",
                "preview": images.jama11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/How+to+make+up+on+your+graduation+day.mp4",
                "duration": 1,
            },
            {
                "id": 156,
                "name": "How To Make Your Eyes Look Bigger",
                "desc": "",
                "preview": images.jama12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/How+to+make+your+eyes+look+bigger.mp4",
                "duration": 1,
            },
            {
                "id": 157,
                "name": "How To Put Eyeliner Perfectly",
                "desc": "",
                "preview": images.jama13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/How+to+put+eyeliner+perfectly.mp4",
                "duration": 1,
            },
            {
                "id": 158,
                "name": "How To Put Fake Eyelashes",
                "desc": "",
                "preview": images.jama14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/How+to+put+fake+eyelashes.mp4",
                "duration": 1,
            },
            {
                "id": 159,
                "name": "Lining Your Eyes",
                "desc": "",
                "preview": images.jama15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/lining+your+eyes.mp4",
                "duration": 1,
            },
            {
                "id": 160,
                "name": "Look Great On Your Birthday",
                "desc": "",
                "preview": images.jama16,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Look+great+on+your+birthday.mp4",
                "duration": 1,
            },
            {
                "id": 161,
                "name": "Magnificient Evening Make Up",
                "desc": "",
                "preview": images.jama17,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Magnificient+evening+make-up.mp4",
                "duration": 1,
            },
            {
                "id": 162,
                "name": "Quick & Easy Eyebrow",
                "desc": "",
                "preview": images.jama18,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Quick+%26+easy+eyebrow.mp4",
                "duration": 1,
            },
            {
                "id": 163,
                "name": "Quick Make Up For Work",
                "desc": "",
                "preview": images.jama19,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Quick+make-up+for+work.mp4",
                "duration": 1,
            },
            {
                "id": 164,
                "name": "Smoky Evening Make Up",
                "desc": "",
                "preview": images.jama20,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Smoky+evening+makeup.mp4",
                "duration": 1,
            },
            {
                "id": 165,
                "name": "Soft & Easy Make Up",
                "desc": "",
                "preview": images.jama21,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Soft+%26+easy+make+up.mp4",
                "duration": 1,
            },
            {
                "id": 166,
                "name": "Soft Evening Make Up",
                "desc": "",
                "preview": images.jama22,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Soft+evening+make-up.mp4",
                "duration": 1,
            },
            {
                "id": 167,
                "name": "Spring & Summer Look",
                "desc": "",
                "preview": images.jama23,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Spring+%26+summer+look.mp4",
                "duration": 1,
            },
            {
                "id": 168,
                "name": "Using Black Eyeliner",
                "desc": "",
                "preview": images.jama24,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Using+black+eyeliner.mp4",
                "duration": 1,
            },
            {
                "id": 169,
                "name": "Wearing Red Lipstick",
                "desc": "",
                "preview": images.jama25,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/jamalouki/Wearing+red+lipstick.mp4",
                "duration": 1,
            },
        ]
            },
            {
                "id": 170,
                "name": "Mariam Advice",
                "desc": "The best Arabic beauty trends with Mariam, enjoy exploring the tips and tricks right on your mobile.",
                "preview": images.ArEntertianment7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-+3AMALIYAT+CHAD+AL+BACHARA.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory":
        [
            /*{
                "id": 171,
                "name": "Nasayeh Maria - 3amaliyat Chad Al Bachara",
                "desc": "",
                "preview": images.mariam1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 172,
                "name": "Nasayeh Maria - Al Azafer",
                "desc": "",
                "preview": images.mariam2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-+AL+AZAFER.mp4",
                "duration": 1,
            },
            {
                "id": 173,
                "name": "Nasayeh Maria - Al Bachara Al 7assasa",
                "desc": "",
                "preview": images.mariam3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-+AL+BACHARA+AL+7ASSASA.mp4",
                "duration": 1,
            },
            {
                "id": 174,
                "name": "Nasayeh Maria - Al Bachara Al Jafa",
                "desc": "",
                "preview": images.mariam4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-+AL+BACHARA+AL+JAFA.mp4",
                "duration": 1,
            },
            {
                "id": 175,
                "name": "Nasayeh Maria - Al Bachara Al Zouhniya",
                "desc": "",
                "preview": images.mariam5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-+AL+BACHARA+AL+ZOUHNIYA.mp4",
                "duration": 1,
            },
            {
                "id": 176,
                "name": "Nasayeh Maria - Beauty Products",
                "desc": "",
                "preview": images.mariam6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Beauty+Products.mp4",
                "duration": 1,
            },
            {
                "id": 177,
                "name": "Nasayeh Maria - Blackheads",
                "desc": "",
                "preview": images.mariam7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Blackheads.mp4",
                "duration": 1,
            },
            {
                "id": 178,
                "name": "Nasayeh Maria - Cucumber & Beauty",
                "desc": "",
                "preview": images.mariam8,
                "videoUrl":  "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Cucumber+%26+Beauty.mp4",
                "duration": 1,
            },
            {
                "id": 179,
                "name": "Nasayeh Maria - Eyes & Neck Care",
                "desc": "",
                "preview": images.mariam9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Eyes+%26+Neck+Care.mp4",
                "duration": 1,
            },
            {
                "id": 180,
                "name": "Nasayeh Maria - Facial",
                "desc": "",
                "preview": images.mariam10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Facial.mp4",
                "duration": 1,
            },
            {
                "id": 181,
                "name": "Nasayeh Maria - Feet Care",
                "desc": "",
                "preview": images.mariam11,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Feet+Care.mp4",
                "duration": 1,
            },
            {
                "id": 182,
                "name": "Nasayeh Maria - Oily Skin",
                "desc": "",
                "preview": images.mariam12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Oily+Skin.mp4",
                "duration": 1,
            },
            {
                "id": 183,
                "name": "Nasayeh Maria - Solarium",
                "desc": "",
                "preview": images.mariam13,
                "videoUrl": "",
                "duration": 1,
            },
            {
                "id": 184,
                "name": "Nasayeh Maria - Taches Brunes",
                "desc": "",
                "preview": images.mariam14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Taches+Brunes.mp4",
                "duration": 1,
            },
            {
                "id": 185,
                "name": "Nasayeh Maria - Taches Seniles",
                "desc": "",
                "preview": images.mariam15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/nasayehmaria/Nasayeh+Maria+-Taches+Seniles.mp4",
                "duration": 1,
            },
        ]
            },
            {
                "id": 186,
                "name": "Shine with Mariam",
                "desc": "Enjoy The best Arabic beauty trends with Mariam, enjoy exploring the tips and tricks right on your mobile.",
                "preview": images.ArEntertianment8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/3abeye.mp4",
                "duration": 1,
                "categoryId": 7,
                "subCategory":
        [
            /*{
                "id": 187,
                "name": "3abeye",
                "desc": "",
                "preview": images.shine1,
                "videoUrl": "",
                "duration": 1,
            },*/
            {
                "id": 188,
                "name": "Ahmar Al Chifah",
                "desc": "",
                "preview": images.shine2,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AHMAR+AL+CHIFAH-.mp4",
                "duration": 1,
            },
            {
                "id": 189,
                "name": "Al Alwan",
                "desc": "",
                "preview": images.shine3,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+ALWAN.mp4",
                "duration": 1,
            },
            {
                "id": 190,
                "name": "Al Anaka",
                "desc": "",
                "preview": images.shine4,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+ANAKA.mp4",
                "duration": 1,
            },
            {
                "id": 191,
                "name": "Al Khasr Al 3ali",
                "desc": "",
                "preview": images.shine5,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+KHASR+AL+3ALI.mp4",
                "duration": 1,
            },
            {
                "id": 192,
                "name": "Al Lawn Il-Abyad",
                "desc": "",
                "preview": images.shine6,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+LAWN+IL+ABYAD.mp4",
                "duration": 1,
            },
            {
                "id": 193,
                "name": "Al Malabes Al Asesiye Lil 3AMAL",
                "desc": "",
                "preview": images.shine7,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+MALABES+AL+ASESIYE+LIL+3AMAL.mp4",
                "duration": 1,
            },
            {
                "id": 194,
                "name": "Al Tannoura Al Tawile",
                "desc": "",
                "preview": images.shine8,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AL+TANNOURA+AL+TAWILE.mp4",
                "duration": 1,
            },
            {
                "id": 195,
                "name": "Alwan Kaws Al Kousah",
                "desc": "",
                "preview": images.shine9,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/ALWAN+KAWS+AL+KOUSAH.mp4",
                "duration": 1,
            },
            {
                "id": 196,
                "name": "Azya2 Al Tamannat",
                "desc": "",
                "preview": images.shine10,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/AZYA2+AL+TAMANNAT.mp4",
                "duration": 1,
            },
            {
                "id": 197,
                "name": "Broche",
                "desc": "",
                "preview": images.shine11,
                "videoUrl":  "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/broche.mp4",
                "duration": 1,
            },
            {
                "id": 198,
                "name": "Cha3rouki Yawm Zafafouki",
                "desc": "",
                "preview": images.shine12,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/CHA3ROUKI+YAWM+ZAFAFOUKI.mp4",
                "duration": 1,
            },
            {
                "id": 199,
                "name": "Fasatin Al A3rass",
                "desc": "",
                "preview": images.shine13,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/FASATIN+AL+A3RASS.mp4",
                "duration": 1,
            },
            {
                "id": 200,
                "name": "Floral Prints",
                "desc": "",
                "preview": images.shine14,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/FLORAL+PRINTS.mp4",
                "duration": 1,
            },
            {
                "id": 201,
                "name": "Houdoum Fasl Il sayf",
                "desc": "",
                "preview": images.shine15,
                "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Videos/shinewithmiriam/HOUDOUM+FASL+IL+SAYF.mp4",
                "duration": 1,
            },
        ]
            }
        ]

    },
    // {
    //     "id" : 3,
    //     "contents" : 3,
    //     "name" : "American TV Series",
    //     "videos" : [
    //         {
    //             "duration" : 1800,
    //             "id" : 31,
    //             "name" : "Flashpoint | CBS US",
    //             "preview" : "../assets/images/movies/american-tv-series/flashpoint.jpeg",
    //             "desc" : "When the Strategic Response Unit arrives, the rules change. The bad guy's caught - he just doesn't know it yet. The SRU, inspired by Toronto's Emergency Task Force, is a handpicked team of elite cops. High risk is their business and these men and women do it all: rescue hostages, bust gangs, defuse bombs. Talk down suicidal teens. Protect the Pope. They handle state-of-the-art weapons - sniper rifles, flash-bang grenades, tasers. They climb the sides of buildings. See through walls. But"
    //         },
    //         {
    //             "duration" : 3600,
    //             "id" : 32,
    //             "name" : "Hemlock Grove",
    //             "preview" : "../assets/images/movies/american-tv-series/hemlock.jpeg",
    //             "desc" : "A teenage girl is brutally murdered, sparking a hunt for her killer. But in a town where everyone hides a secret, will they find the monster among them?"
    //         },
    //         {
    //             "duration" : 3600,
    //             "id" : 33,
    //             "name" : "Hannibal",
    //             "preview" : "../assets/images/movies/american-tv-series/hannibal.jpeg",
    //             "desc" : "Explores the early relationship between the renowned psychiatrist and his patient, a young FBI criminal profiler, who is haunted by his ability to empathize with serial killers."
    //         }
    //     ]
    // },
    {
        "id" : 4,
        "contents" : 4,
        "name" : "Hollywood Blockbusters",
        "videos" : [
            {
                "duration" : 21334,
                "id" : 202,
                "name" : "Dark Touch",
                "preview" : images.Hollwood1,
                "desc" : "An isolated country house takes on a monstrous life of its own. Objects and furniture attack the inhabitants, leaving 11-year-old Niamh the only survivor of a bloody massacre that destroys her parents and her little brother, Ciaran. The police ignore her when she tries to tell them about the murderous rage of the house. She is taken in by family friends who try to give her some semblance of a normal life. But Niamh cannot find peace. All around her, signs of danger continue to"
            },
            {
                "duration" : 45223,
                "id" : 203,
                "name" : "Airborne",
                "preview" : images.Hollwood2,
                "desc" : "A plane takes off during a snowstorm and soon reports both pilots are dead. As the passengers slowly die off, they wish they had never left the ground."
            },
            {
                "duration" : 33213,
                "id" : 204,
                "name" : "The Broken",
                "preview" : images.Hollwood3,
                "desc" : "A woman is thrust into a world where everything is subtly unfamiliar in this edgy thriller from writer and director Sean Ellis. Gina McVay (Lena Headey) is a radiologist who is enjoying dinner one evening with her family and her boyfriend Stefan (Melvil Poupaud) when a mirror shatters for no apparent reason. After a few moments, no one thinks much of it, but the next day Gina is leaving work and she sees something even more troubling -- a woman who looks just like her, driving a car identical to her own. Curious, Gina sneaks into the doppelganger's apartment and sees a photo of herself and her father in the hallway. Seriously rattled, Gina runs out and drives away, only to get in an accident that lands her in the hospital. After she's released, Gina asks Stefan if she can stay with him, but while he looks the same, his personality and behavior are quite different from the way she remembers him, and she begins having vivid nightmares which become all the more terrifying when the same horrific images begin popping up in her waking hours. The Broken received its American premiere at the 2008 Sundance Film Festival."
            },
            {
                "duration" : 31344,
                "id" : 205,
                "name" : "The History of Love",
                "preview" : images.Hollwood4,
                "desc" : "What if a book written before World War II in a little village in Eastern Europe could shine through the years, crossing continents and surviving violent conflicts, to guide the journey of three generations? This book is entitled THE HISTORY OF LOVE. New York, the 21st century. Leo, an old Polish Jewish immigrant, lives with the memory of the great love of his life. Despite his painful history, he cheerfully navigates life’s ups and downs with indefatigable, crazy humour, alongside his"
            },
            {
                "duration" : 31344,
                "id" : 206,
                "name" : "One Last Breath",
                "preview" : images.Hollwood5,
                "desc" : "Year's after suspicious circumstances claim the life of her mother, young Alex Hart decides to take matters into her own hands to learn the truth about her father Raymond. Her quest leads her into the terrifying conspiracy of a pharmaceutical giant and it's CEO who's hiding the fact that Raymond is being tortured by an experimental new treatment deep inside the walls of the company"
            },
            // {
            //     "duration" : 31344,
            //     "id" : "holly-block-1"
            //     "name" : "Fourth Angel",
            //     "preview" : "../assets/images/movies/hollywood-blockbusters/Fourth-Angel.jpg",
            //     "desc" : "Jack Elgin is a magazine editor who surprises his wife and three kids with a vacation to India. While their plane is refuelling in Cyprus, the Elgin's jetliner is suddenly taken over by terrorists. All hell breaks loose and Jack's wife and two daughters are killed. Back in London, Jack comforts his traumatized young son and tries to make sense of this tragedy, but is further horrified when the terrorists are released. When Jack decides to take justice in his own hands, he must"
            // },
            {
                "duration" : 31344,
                "id" : 207,
                "name" : "Riders (Steal)",
                "preview" : images.Hollwood6,
                "desc" : "Slim, Otis, Frank and Alex (three boys and a girl) form a group of snowboard and skate young bank robbers. They are known as masters of the runaway art. Slim, the thinking head hatched a plan for their final retirement: five consecutive burglars, in five days, involving 20 million dollars. But this time, they're gonna have to face both the police - and the mafia!"
            },
            {
                "duration" : 31344,
                "id" : 208,
                "name" : "Under Suspicion",
                "preview" : images.Hollwood7,
                "desc" : "A lawyer is asked to come to the police station to clear up a few loose ends in his witness report of a foul murder. This will only take ten minutes, they say, but it turns out to be one loose end after another, and the ten minutes he is away from his speech become longer and longer..."
            },
            {
                "duration" : 31344,
                "id" : 209,
                "name" : "White Bird in a Blizzard",
                "preview" : images.Hollwood8,
                "desc" : "Kat Connors is 17 years old when her perfect homemaker mother, Eve, a beautiful, enigmatic, and haunted woman, disappears - just as Kat is discovering and relishing her newfound sexuality. Having lived for so long in a stifled, emotionally repressed household, she barely registers her mother's absence and certainly doesn't blame her doormat of a father, Brock, for the loss. In fact, it's almost a relief. But as time passes, Kat begins to come to grips with how deeply Eve's"
            },
            {
                "duration" : 31344,
                "id" : 210,
                "name" : "The Revolt",
                "preview" : images.Hollwood9,
                "desc" : "A nation that neglects its history is a nation with no past, with no memory, unable to learn from mistakes in times past and unable to benefit from the achievements and glorious acts of former generations.\""
            },
            {
                "duration" : 31344,
                "id" : 211,
                "name" : "Haunter",
                "preview" : images.Hollwood10,
                "desc" : "In this reverse ghost story, teenage Lisa Johnson (Breslin) and her family died in 1986 under sinister circumstances but remain trapped in their house, unable to move on. Over a period of six “days”, Lisa must reach out from beyond the grave to help her present-day, living counterpart, Olivia, avoid the same fate Lisa and her family suffered."
            },
            {
                "duration" : 31344,
                "id" : 212,
                "name" : "Maniac",
                "preview" : images.Hollwood11,
                "desc" : "Just when the streets seemed safe, a serial killer with a fetish for scalps is back and on the hunt. Frank is the withdrawn owner of a mannequin store, but his life changes when young artist Anna appears asking for his help with her new exhibition. As their friendship develops and Frank's obsession escalates, it becomes clear that she has unleashed a long-repressed"
            },
            {
                "duration" : 31344,
                "id" : 213,
                "name" : "3 Holiday Tails",
                "preview" : images.Hollwood12,
                "desc" : "Rod and Katherine Wright are enjoying their retirement in Florida – or so they’re telling each other. Actually, Rod’s been bored with nothing to do, and Katherine misses her kids back home, especially with Christmas right around the corner. At least they’ve made friends with their new neighbor Lisa, who’s a golden retriever fan like them – Rod and Katherine’s dog Jake has three new puppy playmates, Mario, Luigi and Pasquale. But when Jake and the puppies take off during a walk with"
            },
            {
                "duration" : 31344,
                "id" : 214,
                "name" : "Universal Soldier III: Regeneration ( A new beginning)",
                "preview" : images.Hollwood13,
                "desc" : "When terrorists threaten nuclear catastrophe, the world's only hope is to reactivate decommissioned Universal Soldier Luc Deveraux. Rearmed and reprogrammed, Deveraux must take on his nemesis from the original Universal Soldier and a next-generation \"UniSol"
            },
            {
                "duration" : 31344,
                "id" : 215,
                "name" : "Valhalla Rising",
                "preview" : images.Hollwood14,
                "desc" : "1000 AD. For years, One Eye, a mute warrior of supernatural strength, has been held prisoner by the Norse chieftain Barde. Aided by Are, a boy slave, One Eye slays his captor and together he and Are escape, beginning a journey into the heart of darkness."
            },
            {
                "duration" : 31344,
                "id" : 216,
                "name" : "The Good Girl",
                "preview" : images.Hollwood15,
                "desc" : "The plot revolves around a young married woman whose mundane life takes a turn for the worse when she strikes up a passionate and illicit affair with an oddball discount-store stock boy who thinks he's Holden Caulfield."
            },
            {
                "duration" : 31344,
                "id" : 217,
                "name" : "The Dealiest Sea",
                "preview" : images.Hollwood16,
                "desc" : "Based on a true story of a fishing boat out of Kodiak, Alaska that encountered a storm and was hit by a rogue wave. The crew, believing that their vessel was about to capsize, abandoned ship in the Bering sea."
            },
        ]
    },
    // {
    //     "id" : 3,
    //     "contents" : 5,
    //     "name" : "International Movies",
    //     "videos" : [
    //         {
    //             "duration" : 28330,
    //             "id" : 21,
    //             "name" : "The Last Days",
    //             "preview" : "../assets/images/movies/international-movies/the-last-days.jpeg",
    //             "desc" : "2013. A mysterious epidemic spreads across the planet. Humanity develops an irrational fear of open spaces that leads to instant death. Soon, the world's remaining population is trapped inside buildings. As Barcelona descends into chaos, Marc embarks on a quest to find Julia, his missing girlfriend, without ever setting foot outside."
    //         },
    //         {
    //             "duration" : 37212,
    //             "id" : 22,
    //             "name" : "The Good Neighbor",
    //             "preview" : "../assets/images/movies/international-movies/the-good-neightbor.jpeg",
    //             "desc" : "2013. A mysterious epidemic spreads across the planet. Humanity develops an irrational fear of open spaces that leads to instant death. Soon, the world's remaining population is trapped inside buildings. As Barcelona descends into chaos, Marc embarks on a quest to find Julia, his missing girlfriend, without ever setting foot outside."
    //         },
    //         {
    //             "duration" : 38240,
    //             "id" : 23,
    //             "name" : "Graduation aka Bacalaureat",
    //             "preview" : "../assets/images/movies/international-movies/graduation.jpeg",
    //             "desc" : "Romeo Aldea (49), a physician living in a small mountain town in Transylvania, has raised his daughter Eliza with the idea that once she turns 18, she will leave to study and live abroad. His plan is close to succeeding -•Eliza has won a scholarship to study psychology in the UK. She just has to pass her final exams – a formality for such a good"
    //         },
    //         {
    //             "duration" : 38240,
    //             "id" : 24,
    //             "name" : "Colt 45",
    //             "preview" : "../assets/images/movies/international-movies/colt-45.jpeg",
    //             "desc" : "Police armourer and firearms instructor Vincent Milès is a shooting ace. Only 25 years old, his prowess is the envy of the world's finest but, to the incomprehension of his colleagues, Vincent refuses to join the police action units. Trapped in a situation primed to blow sky high, Vincent is left with no choice but to embrace the darkness within him if"
    //         },
    //         {
    //             "duration" : 38240,
    //             "id" : 25,
    //             "name" : "Hierro",
    //             "preview" : "../assets/images/movies/international-movies/hierro.jpeg",
    //             "desc" : "After her son goes missing, a broken mother returns months later to the island of El Hierro to identify a body. She finds out her son is not the only one missing."
    //         }
    //     ]
    // },
    {
        "id" : 5,
        "contents" : 9,
        "name" : "Bollywood Movies",
        "videos" : [
            {
                "duration" : 10841,
                "id" : 218,
                "name" : "1920 - London",
                "preview" : images.Bollywood1,
                "desc" : "Shivangi lives happily in London with her husband Veer. On witnessing Veer's strange behavior one day, she discovers that an evil spirit has possessed him and seeks help from an exorcist."
            },
            {
                "duration" : 10740,
                "id" : 219,
                "name" : "Bodyguard",
                "preview" : images.Bollywood2,
                "desc" : "Lovely Singh is extremely respectful of Mr. Rana and his daughter Divya. While appointed as Divya's bodyguard, he falls in love with a girl he has never met, unaware that she is none other than Divya."
            },
            {
                "duration" : 11020,
                "id" : 220,
                "name" : "Commando 2",
                "preview" : images.Bollywood3,
                "desc" : "Commando 2 traces a black money trail as Karan (Vidyut Jammwal) a commando, cops Bakhtawar (Freddy Daruwala) and Bhawna Reddy (Adah Sharma) are assigned to arrest Vicky Chadha (Thakur Anoop Singh) and international money launderer in Thailand. While Karan and Bakhtawar don’t see eye to eye, Bhawna’s too stupid to lead a mission. A major twist in tale comes when Vicky’s wife Maria (Esha Gupta) claims that she and Vicky are innocent. There’s double crossing, love affairs and a much bigger truth yet to unfold. Will Karan succeed in his mission is what is left to see"
            },
            {
                "duration" : 11055,
                "id" : 221,
                "name" : "Ghayal Once Again",
                "preview" : images.Bollywood4,
                "desc" : "When a video of his mentor's murder surfaces, an investigative journalist (Sunny Deol) takes action to find the killers."
            },
            {
                "duration" : 10800,
                "id" : 222,
                "name" : "Golmaal Again",
                "preview" : images.Bollywood5,
                "desc" : "GOLMAAL AGAIN is the story of five friends who encounter strange occurrences when they visit their orphanage. Gopal (Ajay Devgn), Madhav (Arshad Warsi), Lucky (Tusshar Kapoor), Laxman Prasad Apte (Shreyas Talpade) and Laxman Sharma (Kunal Kemmu) are orphans who grew up in an orphanage owned by Jamnadas (Uday Tikekar). As kids, they were close to each other but then they became rivals. Gopal and Laxman Prasad Apte separated while Madhav, Lucky and Laxman Sharma went their own way. When they all grew up, they start working with land sharks and their job revolved around getting the plots vacated. This also created further friction among them. But when they learn that Jamnadas has passed away, they decide to iron out their differences and go together to the orphanage to pay their respects. They stay at Colonel Chouhan’s (Sachin Khedekar) bungalow located right next to Jamnadas Orphanage, where their childhood friend Anna (Tabu) is also residing. Anna has a special power – she can communicate with the dead. At the Colonel’s bungalow, Gopal gets attracted to the caretaker Damini (Parineeti Chopra). Meanwhile, Vasu Reddy (Prakash Raj), a builder and a close friend of Jamnadas announces that Jamnadas gave away the land to him where the orphanage is situated. He also says that he has decided to shift the kids at Bengaluru where he’ll provide them with better facilities. Gopal is ghost-fearing person and he starts to see the spirit of Jamnadas’s dead daughter (Ashwini Kalsekar). However, the truth is something else and when the five friends find out, it shocks them like never before. What is that truth and what happens next forms the rest of the film."
            },
            {
                "duration" : 17845,
                "id" : 223,
                "name" : "Holiday",
                "preview" : images.Bollywood6,
                "desc" : "After a bomb destroys the bus on which he was riding, a military officer (Akshay Kumar) foregoes his vacation to hunt down a terrorist and the sleeper cells under his command."
            },
            {
                "duration" : 17845,
                "id" : 224,
                "name" : "Naam Shabana",
                "preview" : images.Bollywood7,
                "desc" : "This Spin-off from Baby (2015) provides the back-story of intelligence agent Shabana, and chronicles how she becomes a spy."
            },
            {
                "duration" : 17845,
                "id" : 225,
                "name" : "Singham Returns",
                "preview" : images.Bollywood8,
                "desc" : "Singham returns as the brave and honest policeman who only resorts to force when necessary."
            },
            {
                "duration" : 17845,
                "id" : 226,
                "name" : "Wazir",
                "preview" : images.Bollywood9,
                "desc" : "A brave ATS officer (Farhan Akhtar) and a disabled grandmaster (Amitabh Bachchan) face a mysterious and dangerous opponent (Neil Nitin Mukesh)."
            }
        ]
    },


];