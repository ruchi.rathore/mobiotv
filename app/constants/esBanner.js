// Final ID - 106
import * as images from '../assets/Images';
export let esBanner = [
    {
        "duration": 21334,
        "id": 20,
        "bannerImg": images.esmobiotvSlide3,
        "name": "Pasión por el flamenco",
        "year": 2015,
        "preview": images.esmobiotvPasion,
        "desc": "A look at the history and traditions of flamenco music and dance.",
        "videoUrl": ""
    },
    {
            "duration": 21334,
            "id": 1,
            "name": "Pablo Escobar, el patron del mal-Episode-1",
            "year": 2012,
            "bannerImg": images.esmobiotvSlide1,
            "preview": images.esmobiotvPablo,
            "desc": "PABLO ESCOBAR: EL PATRON DEL MAL, PARTE 1-3 Based on the best seller \" La Parabola de Pablo \" by journalist and ex Major of Medellin, Alonso Salazar, the series from Juana Uribe and Camilo Cano is based on real life events and is backed up by an impressive journalist documentation that makes it the most ambitious TV series from Colombia. In the series we are presented with a provocative and realist look at the life of one of the most famous criminals of the XX Century: Pablo Escobar. Considered by some a hero and by others the most violent of \"Capos\" ever. ",
            "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Pablo+Escobar+Capitulo/Pablo+Escobar+Capitulo+1.mp4",
            "subCategory": [
                {
                    "duration": 21334,
                    "id": 28,
                    "name": "Pablo Escobar, el patron del mal-Episode-2",
                    "year": 2012,
                    "preview": images.esmobiotvPablo,
                    "desc": "PABLO ESCOBAR: EL PATRON DEL MAL, PARTE 1-3 Based on the best seller \" La Parabola de Pablo \" by journalist and ex Major of Medellin, Alonso Salazar, the series from Juana Uribe and Camilo Cano is based on real life events and is backed up by an impressive journalist documentation that makes it the most ambitious TV series from Colombia. In the series we are presented with a provocative and realist look at the life of one of the most famous criminals of the XX Century: Pablo Escobar. Considered by some a hero and by others the most violent of \"Capos\" ever. ",
                    "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/Pablo+Escobar+Capitulo/Pablo+Escobar+Capitulo+2.mp4",
                    "subCategory": []
                },
            ]
        },
            // {
            //     "duration": 21334,
            //     "id": 10,
            //     "name": "The Skin I Live In",
            //     "year": 2011,
            //     "bannerImg": images.esmobiotvSlide2,
            //     "preview": images.esmobiotvTheSkin,
            //     "desc": "Ever since his beloved wife was horribly burned in an auto accident, Dr. Robert Ledgard (Antonio Banderas), a skilled plastic surgeon, has tried to develop a new skin that could save the lives of burn victims. Finally, after 12 years, Ledgard has created a skin that guards the body, but is still sensitive to touch. With the aid of his faithful housekeeper (Marisa Paredes), Ledgard tests his creation on Vera (Elena Anaya), who is held prisoner against her will in the doctor's mansion.",
            //     "videoUrl": "https://s3-eu-west-1.amazonaws.com/mobiotv-contents/Spanish/The+Skin+I+Live+In/The+Skin+I+Live+In.mp4"
            // },



]