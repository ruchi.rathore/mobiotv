import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';

// Styles
import { styles } from "../../style/appStyles";
import liveChannelStyle from "../../style/liveChannelStyle";

// Other data/helper functions
import NavigationService from "../../utils/NavigationService";
import { channelLogo } from "../../assets/Images";

class VODCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favoriteSwitch: false
        };
    }

    clickedButton() {

    }

    _handleFavoriteClicked() {

    }

    _onPressButton() {
        NavigationService.navigate('Play');
    }

    _onViewAllButtonPress() {
        NavigationService.navigate('Channels');
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _renderDuration(item) {
        const { duration } = this.props;
        return (
            duration ? 
                <View style={liveChannelStyle.videoDurationView}>
                    <Text style={[styles.avRegular, liveChannelStyle.videoDuration]}>{item.duration}</Text>
                </View>
                :
                <View style={liveChannelStyle.liveNow}>
                    <Icon name='rss' size={13} style={{ backgroundColor: 'transparent', paddingRight: 5 }} color='#fff' />
                    <Text style={[styles.avRegular, liveChannelStyle.videoDuration]}>Live Now</Text>
                </View>
        );
    }

    _renderChannelLogo() {
        const { isChannelLogo } = this.props;
        return (
            isChannelLogo ?
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: '60%', height: '40%', resizeMode: 'contain' }} source={channelLogo} />
                </View>
                : ''
        );
    }

    render() {
        const { data, categoryName, banner, banner1, isTvVideo, backgroundOpacity } = this.props;

        return (
            <View>
                <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                    <View>
                        <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                            {categoryName}
                        </Text>
                    </View>
                    <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this)}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name='th' size={15} style={{ backgroundColor: 'transparent', paddingRight: 5 }} color='#d51a92' />
                            <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                View All
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView horizontal={true} >
                    {isTvVideo ?
                        <View style={{ flexDirection: 'row' }}>
                            {
                                data.map((item, index) => {
                                    const favoriteIconColor = item.favorite ? "#f00b64" : "#fff";
                                    const finalImage = (index % 2) === 0 ? banner : banner1;
                                    return (
                                        <View style={liveChannelStyle.tvThmbnail} key={index}>
                                            <TouchableOpacity onPress={this._onPressButton.bind(this)}>
                                                <ImageBackground style={liveChannelStyle.tvImageBackground} source={finalImage}>
                                                    <View style={{flex: 1, backgroundColor: backgroundOpacity}}>
                                                        <View style={liveChannelStyle.tvFavoriteView}>
                                                            <TouchableHighlight onPress={this._handleFavoriteClicked.bind(this)}>
                                                                <Icon name='heart' size={15} style={{ backgroundColor: 'transparent' }} color={favoriteIconColor} />
                                                            </TouchableHighlight>
                                                        </View>
                                                        {this._renderChannelLogo()}
                                                        <View style={liveChannelStyle.videoTitleView}>
                                                            <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{item.title}</Text>
                                                            {this._renderDuration(item)}
                                                        </View>
                                                    </View>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        :
                        <View style={{ flexDirection: 'row' }}>
                            {
                                data.map((item, index) => {
                                    const favoriteIconColor = item.favorite ? "#f00b64" : "#fff";
                                    const finalImage = (index % 2) === 0 ? banner : banner1;
                                    return (
                                        <View style={liveChannelStyle.imageThmbnail} key={index}>
                                            <TouchableOpacity onPress={this._onPressButton.bind(this)}>
                                                <ImageBackground style={liveChannelStyle.imageBackground} source={finalImage}>
                                                    <View style={liveChannelStyle.favoriteView}>
                                                        <TouchableHighlight onPress={this._handleFavoriteClicked.bind(this)}>
                                                            <Icon name='heart' size={15} style={{ backgroundColor: 'transparent' }} color={favoriteIconColor} />
                                                        </TouchableHighlight>
                                                    </View>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }
                        </View>
                    }
                </ScrollView>
            </View>
        );
    }
}

export default VODCategory;
