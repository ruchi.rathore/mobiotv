import React, { Component } from "react";
import { Image, View, Text, TouchableHighlight } from "react-native";

import NavigationService from '../../utils/NavigationService';

// Styles
import { styles } from "../../style/appStyles";
import footerStyle from "../../style/footerStyle";
import Globals from  '../../constants/Globals';
// Other data/helper functions
import {console_log} from "../../utils/helper";

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.clickedButton = this.clickedButton.bind(this);
    }

    onClickFooter(screen){
        if(this.props.from !== screen){
            this.clickedButton(screen);
        }
    }

    clickedButton(screen) {
        NavigationService.navigate("StaticScreens", {from : screen});
    }

    render() {
        return (
            <View style={footerStyle.footerView}>
                {/*<View>*/}
                    {/*<Text style={[styles.avRegular, footerStyle.footerFirstText]}>{Globals.type === 'es'? "Un nuevo mundo de mobiletainment" : "A New World of Mobiletainment"}</Text>*/}
                {/*</View>*/}
                {/*<View style={{ paddingTop: 15, paddingBottom: 15, alignItems: 'center' }}>*/}
                    {/*<View>*/}
                        {/*<TouchableHighlight underlayColor='transparent' activeOpacity={0.6} onPress={()=> this.onClickFooter(Globals.type === 'es'? 'Sobre nosotros'  : "About")}>*/}
                            {/*<Text style={[styles.avRegular, footerStyle.footerText, {color : this.props.from === 'Sobre nosotros' || this.props.from === 'About'? '#d51a92' :'#727272'}]}>{Globals.type === 'es'? "Sobre nosotros" : "About"}</Text>*/}
                        {/*</TouchableHighlight>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<TouchableHighlight underlayColor='transparent' activeOpacity={0.6} onPress={()=> this.onClickFooter(Globals.type === 'es'? "Política de privacidad" : 'Privacy Policy')}>*/}
                            {/*<Text style={[styles.avRegular, footerStyle.footerText, {color : this.props.from === 'Política de privacidad' || this.props.from === 'Privacy Policy'? '#d51a92' :'#727272'}]}>{Globals.type === 'es'? "Política de privacidad" : 'Privacy Policy'}</Text>*/}
                        {/*</TouchableHighlight>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<TouchableHighlight underlayColor='transparent' activeOpacity={0.6} onPress={()=> this.onClickFooter(Globals.type === 'es'? "Términos y condiciones" : "Terms & Conditions")}>*/}
                            {/*<Text style={[styles.avRegular, footerStyle.footerText, {color : this.props.from === 'Términos y condiciones' || this.props.from === 'Terms & Conditions'? '#d51a92' :'#727272'}]}>{Globals.type === 'es'? "Términos y condiciones" : "Terms & Conditions"}</Text>*/}
                        {/*</TouchableHighlight>*/}
                    {/*</View>*/}
                {/*</View>*/}
                <View>
                    <Text style={[styles.avRegular, footerStyle.footerCopyRightsText, {paddingTop: 5}]}>{Globals.type === 'es'? "2018 © MobioTV. Todos los derechos reservados" : '2018 © MobioTV. All Rights Reserved.'}</Text>
                </View>
            </View>
        );
    }
}

export default Footer;
