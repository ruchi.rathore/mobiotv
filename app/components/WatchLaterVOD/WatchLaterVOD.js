import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';

// Components

// Styles
import { styles } from "../../style/appStyles";
import favoriteStyles from "../../style/favoriteStyle";

// Other data/helper functions
import { tv_4 } from "../../assets/Images";

class WatchLaterVOD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favoriteSwitch: false
        }
    }
   
    onExploreClicked() {
    }

    onPressButton() {

    }

    _renderContent() {
        const { data } = this.props;
        if (data.length) {
            return (
                <View style={[favoriteStyles.renderContent]}>
                    {
                        data.map((item, index) => {
                            const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                            return (
                                <View key={item.id} style={favoriteStyles.favoriteItem}>
                                    <View style={favoriteStyles.favLiveChannel}>
                                        <TouchableOpacity onPress={this.onPressButton.bind(this)}>
                                            <ImageBackground style={[favoriteStyles.favoriteItemImageB]} source={tv_4}>
                                                <View style={[favoriteStyles.favoriteItemImageInnerView]} />
                                            </ImageBackground>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={favoriteStyles.favoriteTitle }>
                                        <Text numberOfLines={1} style={[styles.avRegular, favoriteStyles.favoriteItemTitle]}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, paddingTop: 0 }}>
                                        <TouchableOpacity onPress={this.onPressButton.bind(this)}>
                                            <Icon name='heart' size={16} style={[favoriteStyles.favouriteHeartIcon]} color={favoriteIconColor} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
            )
        } else {
            return (
                <View>
                    <View style={favoriteStyles.noChannelsViewTitle}>
                        <Text style={[styles.avRegular, favoriteStyles.noData]}>No Videos Added To Watch Later Yet</Text>
                    </View>
                    <View style={favoriteStyles.noChannelsViewDesc}>
                        <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>Add your favorite videos to access and watch easily without any hassles.</Text>
                    </View>
                </View>
            )
        }
    }
    render() {
        return (
            <View style={{ flex: 1, marginTop: '5%' }}>
                    <View style={{ backgroundColor: 'black' }}>
                        {this._renderContent()}
                        <View style={favoriteStyles.exploreButtonView}>
                            <TouchableHighlight onPress={this.onExploreClicked.bind(this)}>
                                <View style={favoriteStyles.exploreVideoButton}>
                                    <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                        Explore Videos
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
            </View>
        )
    }
}

export default WatchLaterVOD;
