
import React from 'react';

import {Image, Platform} from 'react-native';
import { TabNavigator,TabBarBottom } from "react-navigation";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import MaterialIcon from 'react-native-vector-icons/dist/MaterialIcons';

// Const Values
//import * as colors from '../utils/colors';
//import * as FontSizes from '../utils/fontsSizes';

// Components
import Account from '../../containers/Accounts';
import VOD from '../../containers/VOD';
import Home from '../../containers/Home';
import SmartTV from '../../containers/SmartTV';
import Search from '../../components/Search/Search';
import WatchLater from '../../containers/WatchLater';
import History from '../../containers/History';
import Globals from '../../constants/Globals';
import HomeStack from './HomeStack';
import HistoryStack from './HistoryStack';


const TabWatch = TabNavigator({
    Home: {
        screen: Home,
        key: 'Home',
        navigationOptions: {
            tabBarLabel :  'Home',
            tabBarIcon: ({ focused, tintColor }) => <MaterialIcon name={'home'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#DB178D" : '#898989'} />
        },

    },
    Search: {
        screen: Search,
        navigationOptions: {
            tabBarLabel:  'Search',
            tabBarIcon: ({ focused, tintColor }) => <FeatherIcon name={'search'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#DB178D" : '#898989'} />
        },
    },
    History: {
        screen: History,
        navigationOptions: {
            tabBarLabel: 'History',
            tabBarIcon: ({ focused, tintColor }) => <MaterialIcon name={'history'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#DB178D" : '#898989'} />
        },
    },
    Account: {
        screen: Account,
        navigationOptions: {
            tabBarLabel: 'Account',
            tabBarIcon: ({ focused, tintColor }) => <Icon name={'user-circle'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#DB178D" : '#898989'} />
        },
    },
    SmartTV: {
        screen: SmartTV,
        navigationOptions: {
            tabBarLabel: 'SmartTV',
            tabBarIcon: ({ focused, tintColor }) => <Icon name={'user-circle'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#DB178D" : '#898989'} />
        },
    },
}, {
    tabBarPosition: 'bottom',
    animationEnabled: false,
    //lazyLoad: false,
    lazy: false,
    overflow: 'hidden',
    //swipeEnabled: false,
    tabBarOptions: {
        initialRouteName: 'VOD',
        showIcon: true,
        inactiveTintColor : '#898989',
        activeTintColor: '#DB178D',
        labelStyle: {
            fontSize: Globals.DeviceType=== 'Phone' ? 11: 19,
            fontFamily: 'AvenirNextLTW01RegularRegular',
            marginTop: Globals.DeviceType=== 'Phone' ? Platform.OS == "ios" ? -5  : 1 : 1
        },
        style: {
            backgroundColor: '#000',

            // height: Globals.DeviceType === 'Phone' ? 43: 70
        },
        indicatorStyle: {
            backgroundColor: '#000',
        },
    },
});

export default TabWatch;
