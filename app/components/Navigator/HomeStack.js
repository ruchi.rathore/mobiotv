import React from 'react';

import { StackNavigator } from 'react-navigation';

// Components
import Channels from "../../containers/ChannelList";
import VideoList from "../../containers/VideoList";
import Play from "../../containers/Play";
import PlayVOD from "../../containers/PlayVOD";
import PlayOthers from "../../containers/PlayOthers";
import VOD from '../../containers/VOD';
// Styles

// Other data/helper functions

const HomeStack = StackNavigator({
    VOD: {
        screen: VOD,
        navigationOptions: {
            title: 'VOD',
            header: null,
            gesturesEnabled: true
        }
    },
    Channels: {
        screen: Channels,
        navigationOptions: {
            title: 'Channels',
            header: null,
            gesturesEnabled: true
        }
    },
    VideoList: {
        screen: VideoList,
        navigationOptions: {
            title: 'VideoList',
            header: null,
            gesturesEnabled: true
        }
    },
    Play: {
        screen: Play,
        navigationOptions: {
            title: 'Play',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    PlayVOD: {
        screen: PlayVOD,
        navigationOptions: {
            title: 'Play VOD',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    PlayOthers: {
        screen: PlayOthers,
        navigationOptions: {
            title: 'Play Others',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
},
{initialRouteName: 'VOD'});


export default HomeStack;
