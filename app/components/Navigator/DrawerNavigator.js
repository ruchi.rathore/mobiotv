import React from 'react';

import { DrawerNavigator } from 'react-navigation';

// Components
import Home from '../../containers/Home';
import Account from '../../containers/Accounts';
import LiveChannels from '../../containers/LiveChannels';
import Play from '../../containers/Play';
import SideBar from '../Sidebar/Sidebar';
import StaticScreens from '../../containers/StaticScreens';
import WatchLater from '../../containers/WatchLater';
import History from '../../containers/History';
import VOD from '../../containers/VOD';

// Styles

// Other data/helper functions

const MainDrawerRouter = DrawerNavigator(
    {
        Home: {
            screen: Home
        },
        LiveChannels: {
            screen: LiveChannels
        },
        VOD: {
            screen: VOD
        }
    },
    {
        initialRouteName: "Home",
        contentOptions: {
            activeTintColor: "#e91e63"
        },
        contentComponent: SideBar
    }
);

export default MainDrawerRouter;
