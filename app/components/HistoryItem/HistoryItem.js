import React, { Component } from 'react';
import {
    TouchableHighlight,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Image
} from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// Styles
import historyStyles from "../../style/historyStyle";
import { styles } from "../../style/appStyles";
import {HistoryStyles} from '../../style/historyStyle';

// Other data/helper functions
import { banner, channelLogo } from "../../assets/Images";
import { show, hide } from '../../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';
import * as vars from '../../constants/api';
import {thumbnail} from "../../assets/Images";
import { console_log, fromNow } from '../../utils/helper';
import NavigationService from "../../utils/NavigationService";
import Globals from  '../../constants/Globals';
import {esmobiotv} from '../../constants/esmobiotv';
import {bidiotvMoviesData} from '../../constants/bidiotvmovies';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';

class HistoryItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
         }
    }

    onPressButton(data) {
        //console_log(data)
        this.props.getVideoOrChannelRelatedData(data);
        if (data.channel) {
            NavigationService.navigate('Play');
        } else {
            NavigationService.navigate('PlayOthers');
        }
    }

    onPressButtonOthers(data) {
        //console_log(data)
        this.props.getVideoOrChannelRelatedData(data);
            NavigationService.navigate('PlayVOD');
    }

    onPressButtonOthers_vd(data) {
        //console_log(data)
        this.props.getVideoOrChannelRelatedData(data);
        NavigationService.navigate('PlayOthers');
    }

    getCreatedDate=(vid)=>{
        let id;
        this.props.historyVideos.shows.map((shows,i)=>{
            if(shows.showId === vid){
                id =  shows.createdAt;
            }
        })
        return id;
    }

    getCreatedDateVideo=(vid)=>{
        let id;
        this.props.historyVideos.videos.map((videos,i)=>{
            if(videos.videoId === vid) {
                id = videos.createdAt;
            }
        })
        return id;
    }

    render() {
        let historyVideos;
        let historyShows;
        let historyVideoIds = this.props.historyVideos.videos.map(v => v.videoId);
        let historyShowsIds =  this.props.historyVideos.shows.map(v => v.showId);

        let bidiotvHistory = [].concat.apply([], bidiotvMoviesData.map((c) => c.videos)).filter((v) => {
            if (~historyVideoIds.indexOf(v.id)) {
                return v;
            }
        });

        let esdomainHistory = [].concat.apply([], esmobiotv.map((c) => c.videos)).filter((v) => {
            if (~historyShowsIds.indexOf(v.id)) {
                return v;
            }
        });


        if(Globals.type == 'es'){
            historyShows = esdomainHistory
        }
        else if(Globals.type == 'uk'){
            historyVideos = bidiotvHistory;
        }

        const { data } = this.props;
         return (
            <View style={{flex: 1, backgroundColor: '#000'}}>
                {((this.props.historyVideos.channels && this.props.historyVideos.channels.length > 0) || (historyVideos && historyVideos.length > 0) || (historyShows && historyShows.length > 0)) ?
                    <ScrollView >
                        {this.props.historyVideos.channels ?this.props.historyVideos.channels.map((channel, index) => {
                        return (
                            <View style={historyStyles.historyItem} key={index} >
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity onPress={this.onPressButton.bind(this, {channel: channel})} style = {{ marginTop: 10, marginBottom: 10 }} >
                                        <ImageBackground style={historyStyles.imageBackground} resizeMode={"contain"} source={channel.channelImage ? {uri: vars.BASE_URL+"uploads/"+channel.channelImage} : thumbnail}>
                                            <View style={historyStyles.bgOpacity}>
                                                <Image style={historyStyles.channelLogo} source={{uri:channel.channelLogo}}/>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity onPress={this.onPressButton.bind(this, {channel: channel})} style={historyStyles.historyTitle}>
                                    <Text
                                        style={[styles.avRegular, historyStyles.historyItemTitle]}>{channel.channelName}</Text>
                                    <Text
                                        style={[styles.avRegular, historyStyles.historyItemDuration]}>{(channel.createdAt)? fromNow(channel.createdAt) :""}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                        })
                            : null
                        }
                        {Globals.type == 'es' ?
                            historyShows.map((video, index) => {
                               return (
                                <View key={index} style={{borderBottomColor :  '#606060',
                                    borderBottomWidth: 1,
                                    paddingBottom: 5,
                                    paddingTop: 5}}>
                                <View style={historyStyles.historyItem} key={index} >
                                    <View style={{ flex: Globals.type == 'es' ? 1 : 2 }}>
                                        <TouchableOpacity style = {{ marginTop: 10, marginBottom: 10 }} onPress={Globals.type ===  'es' ?
                                            this.onPressButtonOthers.bind(this, {video: {
                                                id: video.id,
                                                name: video.name,
                                                preview: video.preview,
                                                duration: video.duration,
                                                subCategory: video.subCategory
                                            }}) : this.onPressButtonOthers_vd.bind(this, {video: {
                                                id: video.videoId,
                                                name: video.name,
                                                preview: video.preview,
                                                duration: video.duration
                                            }})}>
                                            <ImageBackground style={[Globals.type == 'es' ?  historyStyles.imageBackgroundVOD : historyStyles.imageBackgroundVOD_BD,{marginLeft: 10}]} resizeMode={"contain"} source={Globals.url ===  'http://uk.mobiotv.com' ? video.preview : {uri: video.preview}}>

                                            </ImageBackground>
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity style={historyStyles.historyTitle} onPress={Globals.type ===  'es' ?
                                        this.onPressButtonOthers.bind(this, {video: {
                                            id: video.id,
                                            name: video.name,
                                            preview: video.preview,
                                            duration: video.duration,
                                            subCategory: video.subCategory
                                        }}) : this.onPressButtonOthers_vd.bind(this, {video: {
                                            id: video.videoId,
                                            name: video.name,
                                            preview: video.preview,
                                            duration: video.duration
                                        }})}>
                                        <Text style={[styles.avRegular, historyStyles.historyItemTitle]}>{video.name}</Text>
                                        <Text style={[styles.avRegular, historyStyles.historyItemDuration]}>{fromNow(this.getCreatedDate(video.id))}</Text>
                                    </TouchableOpacity>
                                </View>
                                </View>
                                    )
                            }) :
                            historyVideos.map((video, index) => {
                                return (
                                    <View key={index} style={{borderBottomColor :  '#606060',
                                        borderBottomWidth: 1,
                                        paddingTop: 5
                                    }}>
                                        <View style={[historyStyles.historyItem, {height: 110}]} key={index} >
                                            <View style={{ flex: Globals.type == 'es' ? 1 : 2 }}>
                                                <TouchableOpacity style = {{ marginTop: 10, marginBottom: 10 }} onPress={
                                                    this.onPressButtonOthers_vd.bind(this, {video: {
                                                        id: video.id,
                                                        name: video.name,
                                                        preview: video.preview,
                                                        duration: video.duration
                                                    }})}>
                                                    <ImageBackground style={[Globals.type == 'es' ?  historyStyles.imageBackgroundVOD : historyStyles.imageBackgroundVOD_BD,{marginLeft: 10}]} resizeMode={"contain"} source={Globals.url ===  'http://uk.mobiotv.com' ? video.preview : {uri: video.preview}}>

                                                    </ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                            <TouchableOpacity style={historyStyles.historyTitle} onPress={
                                                this.onPressButtonOthers_vd.bind(this, {video: {
                                                    id: video.id,
                                                    name: video.name,
                                                    preview: video.preview,
                                                    duration: video.duration
                                                }})}>
                                                <Text style={[styles.avRegular, historyStyles.historyItemTitle]}>{video.name}</Text>
                                                <Text style={[styles.avRegular, historyStyles.historyItemDuration]}>{fromNow(this.getCreatedDateVideo(video.id))}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            })
                        }
                   </ScrollView>

                        :
                        <View style = {{ flex: 1, alignItems: 'center' }}>
                            <MaterialIcons name={'history'} size={Globals.DeviceType=== 'Phone' ? 100: 150} style={{ backgroundColor: 'transparent', alignSelf: 'center', marginTop: '48%' }} color= {'#510635'} />
                            <Text style={[styles.avRegular,{color: '#A4A4A4', fontSize: 18}]}>{Globals.type === 'es'? "Sin historial de videos todavía" : 'No history of videos yet'}</Text>
                        </View>
                }
            </View>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        header: state.HeaderReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryItem);




