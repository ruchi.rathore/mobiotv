import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
// Components

// Styles
import { styles } from "../../style/appStyles";
import relatedVideosStyles from "../../style/relatedVideosStyles";

// Other data/helper functions
import data from '../../data/moviesData.json';
import { tv_3, tv_4 } from "../../assets/Images";
import * as vars from '../../constants/api';
import { messages } from '../../constants/messages';
import { show, hide } from '../../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';
import { addFavoriteChannel, addLikesChannels } from '../../actions/FavoriteActions';
import { showMessage } from '../../actions/FlashMessageActions';
import { setHeaderTitle } from '../../actions/HeaderActions';
import { addChannelHistory } from '../../actions/HistoryActions';
import { console_log } from '../../utils/helper';

class RelatedVideos extends Component {
    constructor(props){
        super(props);
        this.state = {
            favoriteSwitch: false
        };
    }

    _onPressButton() {
        this.props.navigation.navigate('Play', {from: this.props.from});
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    render() {
        return (
            <View style={[relatedVideosStyles.wrapperView]}>
                <View style={[relatedVideosStyles.textAndSwitchView]}>
                    <View>
                        <Text style={[styles.avRegular, relatedVideosStyles.headerText]}>
                            {this.props.from == 'channel' ? "Related Channels" : "Related Videos"}
                        </Text>
                    </View>
                    <View style={[relatedVideosStyles.switchWrapper]}>
                        <Text style={[styles.avRegular, relatedVideosStyles.subHeaderText]}>
                            Autoplay
                        </Text>
                        <Switch style={[relatedVideosStyles.switch]} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                    </View>
                </View>
                {
                    data.relatedVideos.map((item, index) => {
                        const favoriteIconColor = item.favorite ? "#f00b64" : "#fff";
                        const finalImage = (index % 2) === 0 ? tv_4 : tv_3;
                        return (
                            <View style={[relatedVideosStyles.relatedItemView]} key={item.id}>
                                <View style={[relatedVideosStyles.imageWrapper]}>
                                    <TouchableOpacity onPress={this._onPressButton.bind(this)}>
                                        <ImageBackground style={relatedVideosStyles.imageBackground} source={finalImage}></ImageBackground>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                <Text style={[relatedVideosStyles.relatedItemTitle]}>7amati34545</Text>
                                <View style={[relatedVideosStyles.titleIcon]}>
                                    <MaterialIcons name='play-circle-outline' size={20} style={[relatedVideosStyles.relatedItemIcon]} color='white' />
                                    <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{item.title}</Text>
                                </View>
                                </View>
                                <View style={[relatedVideosStyles.relatedItemIconWrapper]}>
                                    <Icon name='heart' size={16} style={[relatedVideosStyles.relatedItemIcon]} color={favoriteIconColor} />
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        showMessage,
        addChannelHistory,
        addLikesChannels
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(RelatedVideos);

