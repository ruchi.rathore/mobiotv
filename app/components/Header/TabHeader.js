import React, { Component } from "react";
import { Keyboard, Animated, StatusBar, Dimensions, Image, View, Text, TouchableHighlight, TouchableOpacity, Platform, TextInput } from "react-native";
import { Header, Button, Body, Left, Right, Item, Input } from "native-base";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavigationService from '../../utils/NavigationService';

// Styles
import { styles } from '../../style/appStyles';
import headerStyle from '../../style/headerStyle';
import { showSearchBar, HideSearchBar, onShowSearchView } from '../../actions/HeaderActions';
import { searchText } from '../../actions/SearchActions';
// Other data/helper functions
import { headerLogo, loginLogo } from "../../assets/Images";
import {console_log} from "../../utils/helper";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Globals from '../../constants/Globals';


class TabHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchBar: false,
            x: new Animated.Value(0),
        };
    }

    searchOpen() {
        //this.slide();
        // this.props.showSearchBar(!this.props.header.showSearchBar);
        // this.props.HideSearchBar(false);
        // this.props.onShowSearchView(true);
        // this.clearText();

        NavigationService.navigate('Search');

    }

    componentWillReceiveProps(newProps){
        //console.log('this.props.showSearchBar:', newProps.header.hideSearchBar);
        if(newProps.header.hideSearchBar){
            this.slideOut();
        }
    }

    clearText() {
        this.props.searchText('');
    }

    onChange(text) {
        this.props.searchText(text);
    }

    _renderLeftSection(checkLeft) {
        const { isDrawer } = this.props;
        // if (isDrawer) {
        //     return(null)
        //
        //     // return (
        //     //     <TouchableHighlight underlayColor="transparent" activeOpacity={0.2} style={[headerStyle.iconsView]} onPress={() => {NavigationService.navigate("DrawerOpen")}}>
        //     //         <FeatherIcon name="menu" size={26} style={{ backgroundColor: 'transparent', marginLeft: 10 }} color="#fff" />
        //     //     </TouchableHighlight>
        //     // )
        // } else {
        return (
            checkLeft === 'Video On Demand' ?
                <TouchableHighlight underlayColor="transparent" activeOpacity={0.2} style={[headerStyle.iconsView]} onPress={() => NavigationService.goBack()}>
                    <Icon name="angle-left" size={26} style={{ backgroundColor: 'transparent', marginLeft: 10 }} color="#fff" />
                </TouchableHighlight>
                : null
        )
        //}
    }


    slide = () => {
        this.setState({x: deviceWidth});
        this.TextInput.focus();
        // Animated.timing(this.state.x, {
        //     toValue: deviceWidth,
        //     duration: 400,
        // }).start(()=> this.TextInput.focus());

    };

    slideOut = () => {
        this.setState({x: 0});
        this.TextInput.blur();

        // Animated.timing(this.state.x, {
        //     toValue: 0,
        //     duration: 300,
        // }).start(()=> this.TextInput.blur());
        //this.TextInput.blur();

    };

    render() {
        const { isDrawer, isTitle, title, isSearch, rightLabel, rightClick, showSearch } = this.props;
        return (
               <View style={headerStyle.header}>
                    <View style={[headerStyle.header, {flexDirection: 'row', justifyContent: 'center'}]}>
                        {Platform.OS == 'ios'?
                            <StatusBar backgroundColor="#fff" barStyle="light-content"/>
                            : <StatusBar backgroundColor="#000000" barStyle="light-content"/>}
                        <View style={headerStyle.headerBg}>
                                <View style={[headerStyle.leftIconView]}>
                                    <Text style={[styles.avRegular, headerStyle.title, {marginLeft: 10}]}>{title}</Text>
                                </View>
                            <View style={ headerStyle.rightIconView}>
                                {showSearch ?
                                    null
                                    : <TouchableOpacity  style={[headerStyle.iconsView]} onPress={() => {rightClick()}}>
                                        {title === 'Historia' ?
                                            <FeatherIcon name={'trash-2'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent', alignSelf: 'flex-end', marginTop: Globals.DeviceType==='Phone' ? 3 : 1}} color= {'#a8a8a8'} />
                                            :
                                            <Text style={[headerStyle.rightText, {alignSelf: 'flex-end', color: '#a8a8a8', marginTop: Globals.DeviceType==='Phone' ? 4 : 1}]}>{rightLabel}</Text>

                                        }

                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                    </View>
                </View>



        );
    }
}


const mapStateToProps = (state) => {
    return {
        header: state.HeaderReducer,
        search: state.SearchReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        searchText,
        showSearchBar,
        HideSearchBar,
        onShowSearchView
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(TabHeader);
