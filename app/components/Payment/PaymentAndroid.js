import React, { Component } from "react";
import InAppBilling from "react-native-billing";

export const PaymentAndroid = async(success, error) => {

    await InAppBilling.close();
    try {
        await InAppBilling.open();
  //console.log('in await: ', await InAppBilling.isSubscribed("ten_dollar_mobiotv"));
        if (!await InAppBilling.isSubscribed("ten_dollar_mobiotv")) {
            const details = await InAppBilling.subscribe("ten_dollar_mobiotv");
           // console.log('You purchased: ', details);
            success(details);
        }
        else{
              success( await InAppBilling.getSubscriptionTransactionDetails("ten_dollar_mobiotv"));
        }
    } catch (err) {
        console.log('error:', err);
        error(err);
    } finally {
        //await InAppBilling.consumePurchase("ten_dollar_mobiotv");
        await InAppBilling.close();
    }

}

