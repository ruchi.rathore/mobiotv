import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text } from "react-native";


// Components
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

// Styles
import { styles } from "../../style/appStyles";

// Other data/helper functions
import { logo } from "../../assets/Images";


class Template extends Component {
    constructor(props) {
        super(props);
        this.state = {
            variable: true
        }
    }

    render() {
        return(
            <View></View>
        )
    }
}

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Template);