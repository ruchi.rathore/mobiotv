import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';
import Globals from '../../constants/Globals';
// Components

// Styles
import { styles } from "../../style/appStyles";
import sliderStyles from "../../style/sliderStyles";

// Other data / functions
import * as SliderImg from "../../assets/Images";
import {esBanner} from "../../constants/esBanner";
import NavigationService from "../../utils/NavigationService";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';

class Slider extends Component {
    constructor(props) {
        super(props);
        this.renderDot = this.renderDot.bind(this);
        this.renderActiveDot = this.renderActiveDot.bind(this);
    }

    renderDot() {
        return (
            <View style={[sliderStyles.dots]} />
        )
    }

    renderActiveDot() {
        return (
            <View style={[sliderStyles.dotsActive]} />
        )
    }

    render() {
        return (
            Globals.type === 'es' ?
                        <Swiper
                            loop={true}
                            autoplay={true}
                            autoplayTimeout={4}
                            dot={this.renderDot()}
                            paginationStyle={[sliderStyles.paginationStyle]}
                            activeDot={this.renderActiveDot()}>
                            {
                                esBanner.map((item,i)=>(
                                    <TouchableOpacity key={i} style={sliderStyles.indicatorViewPage} onPress={()=>{NavigationService.navigate('PlayVOD');
                                        this.props.getVideoOrChannelRelatedData({video: item});}}>
                                        <Image style={[sliderStyles.slides]} resizeMode="contain"  source={ item.bannerImg } ></Image>
                                    </TouchableOpacity>
                                    )
                            )}
                        </Swiper>
                :
                <Swiper
                    loop={true}
                    autoplay={true}
                    autoplayTimeout={4}
                    dot={this.renderDot()}
                    paginationStyle={[sliderStyles.paginationStyle]}
                    activeDot={this.renderActiveDot()}>
                    <View style={sliderStyles.indicatorViewPage}>
                        <Image style={[sliderStyles.slides]} resizeMode="contain"  source={ SliderImg.bidioSlide1 } ></Image>
                    </View>
                    <View style={sliderStyles.indicatorViewPage}>
                        <Image style={[sliderStyles.slides]} resizeMode="contain" source={  SliderImg.bidioSlide2 } ></Image>
                    </View>
                    <View style={sliderStyles.indicatorViewPage}>
                        <Image style={[sliderStyles.slides]} resizeMode="contain" source={ SliderImg.bidioSlide3 } ></Image>
                    </View>
                    <View style={sliderStyles.indicatorViewPage}>
                        <Image style={[sliderStyles.slides]} resizeMode="contain" source={ SliderImg.bidioSlide4 } ></Image>
                    </View>
                </Swiper>

        );
    }
}

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Slider);