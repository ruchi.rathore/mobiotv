import * as action_types from './action_types';
import {console_log} from '../utils/helper';

export const getVideoOrChannelRelatedData = (videoOrChannelRelatedData) => {
    return {
        type: action_types.GET_PLAY_PAGE_DATA,
        data: {
            data: videoOrChannelRelatedData
        }
    }
};