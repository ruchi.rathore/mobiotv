import * as action_types from './action_types';
import {console_log} from "../utils/helper";

export const addChannelHistory = (channels) => {
    return {
        type: action_types.ADD_HISTORY_CHANNEL,
        data: channels
    }
};

export const addVideoHistory = (videos) => {
    return {
        type: action_types.ADD_HISTORY_VIDEO,
        data: videos
    }
};

export const addShowHistory = (shows) => {
    return {
        type: action_types.ADD_HISTORY_SHOW,
        data: shows
    }
};

export const clearHistory = () => {
    return {
        type: action_types.CLEAR_HISTORY
    }
};