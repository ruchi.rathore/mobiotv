import * as action_types from '../actions/action_types';
import { console_log } from "../utils/helper";

const initialState = {
    country: []
};

export const CountryReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        default:
            return state;
    }
};