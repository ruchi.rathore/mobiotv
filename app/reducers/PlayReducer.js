import * as action_types from '../actions/action_types';
import { console_log } from '../utils/helper';

var initialState = {
    data: {}
};

export const PlayReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case action_types.GET_PLAY_PAGE_DATA:
            return {
                ...state,
                data: Object.assign({}, state.data, action.data.data)
            };
        default:
            return state;
    }
};