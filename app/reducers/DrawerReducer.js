import { OPEN_DRAWER, CLOSE_DRAWER } from '../actions/action_types';

export type State = {
    drawerState: string,
    drawerDisabled: boolean
}

const initialState = {
    drawerState: 'closed',
    drawerDisabled: true,
};

export default function (state:State = initialState, action): State {
    switch (action.type) {
        case OPEN_DRAWER: {
            return {
                ...state,
                drawerState: 'opened'
            }
        }
        case CLOSE_DRAWER: {
            return {
                ...state,
                drawerState: 'closed'
            }
        }
        default: {
            return state;
        }
    }
}
