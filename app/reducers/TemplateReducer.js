import * as action_types from '../actions/action_types';
import { console_log } from "../utils/helper";

const initialState = {
    variable: true
};

const TemplateReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case action_types.TEMPLATE_ACTIONS:
            return {
                variable: action.data
            };
        default:
            return state;
    }
};

export default TemplateReducer;