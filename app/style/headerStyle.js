import {StyleSheet, Dimensions, Platform} from 'react-native';
import * as FontSizes from '../utils/fontsSizes';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Globals from '../constants/Globals';

export default headerStyle = StyleSheet.create({
    header: {
        backgroundColor: '#000000',
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
        height: Platform.OS == "ios" ? ((deviceHeight == 812) ? 95 : 65) : 45
    },
    SearchDefaultBar: {
        backgroundColor: '#323232',
        height: Platform.OS == "ios" ? 40: 50,
        marginTop: Platform.OS == "ios" ?  ((deviceHeight == 812) ? 55 : 25) : 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5
    },
    dfltSearch: {
        color: '#585858',
        fontSize: FontSizes.large,
        height: '100%',
        textAlign: 'center',
        lineHeight: Platform.OS == "ios" ? 37 : 35,
        marginLeft: 5
    },
    defltsearchIcn: {
        marginTop: 1,
        marginLeft: Globals.DeviceType == 'Phone' ? '39%' : '45%'
    },
    title: {
        color: 'white',//
        fontSize: FontSizes.large,
        height: '100%',
        textAlign: 'center',
        textAlignVertical: 'center',
        lineHeight: Globals.IphoneX ? 72 : Platform.OS == "ios" ? 50 : 55
    },
    logo: {
        height: Globals.DeviceType === 'Phone'? '50%': '60%',
        resizeMode: 'contain',
        justifyContent: 'center',
        marginTop: '1%'
    },
    titleView: {
        flex: 4,
        alignItems: 'center',
        //justifyContent: 'center',
    },
    leftIconView: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        height: '100%'
    },
    iconsView: {
        width: '78%',
        paddingTop: 10,
        //paddingBottom: 10,
        marginRight: 10
        //backgroundColor:'red'
    },
    rightIconView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    rightText: {
        color: 'white',
        fontSize: FontSizes.small,
        textAlign: 'left',
        justifyContent: 'center'
    },
    headerBg: {
        marginTop: Platform.OS == "ios" ? ((deviceHeight == 812) ? 35 : 20) : 0,
        flexDirection: 'row',
        width: '100%',
    },
    headerSearch: {
        marginTop: Platform.OS == "ios" ? ((deviceHeight == 812) ? 55 : 25) : 5,
        flexDirection: 'row',
        backgroundColor: '#323232',
        borderColor: '#585858',
        borderWidth: 1,
        borderRadius: 3,
        width:  Globals.DeviceType === 'Phone'? '80%': '85%',
        height: Platform.OS == "ios" ? ((deviceHeight == 812) ? "40%" : '60%') : '85%',
        marginLeft: 10
    },
    btn: {
        marginLeft: 10,
        marginTop: Platform.OS == "ios" ? ((deviceHeight == 812) ? 24 : 10) : 0,
        width: '20%',
        height: '100%'
    },
    cancelTxt: {
        color: '#fff',
        fontSize: FontSizes.small,
    },
    searchTxt: {
        color: '#fff',
        width: Globals.DeviceType === 'Phone'? '80%': '90%',
        marginLeft: 5,
        height: '100%',
        fontSize: FontSizes.small,
    },
    searchIcn: {
        marginTop:  7,
        marginLeft: 5
    },
});
