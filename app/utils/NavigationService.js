//common navigation service param.
import { NavigationActions } from 'react-navigation';

let _navigator;

let setTopLevelNavigator = (navigatorRef) => {
    _navigator = navigatorRef
};

let navigate = (routeName, params) => {
    _navigator.dispatch(
        NavigationActions.navigate({
            type: NavigationActions.NAVIGATE,
            key: null,
            routeName,
            params
        })
    );
};

let reset = (routeName, params) => {
    _navigator.dispatch(
        NavigationActions.reset({
            index: 0,
            //key: null,
            actions: [NavigationActions.navigate({
                routeName: routeName,
                params: params
            })]
        })
    )
};

let goBack = () => {
    _navigator.dispatch(
        NavigationActions.back()
    );
};

export default NavigationService = {
    navigate,
    setTopLevelNavigator,
    reset,
    goBack
};