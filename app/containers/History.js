import React, { Component } from "react";
import { View, TouchableHighlight, Text, Image, ImageBackground, ScrollView, TouchableOpacity } from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import axios from 'axios';
import * as vars from '../constants/api';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { setHeaderTitle } from '../actions/HeaderActions';
import { clearHistory, addVideoHistory, addChannelHistory, addShowHistory, } from '../actions/HistoryActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
// Components
import TabHeader from '../components/Header/TabHeader';
import Footer from '../components/Footer/Footer';
import HistoryItems from '../components/HistoryItem/HistoryItem';

// Styles
import HistoryStyles from "../style/historyStyle";
import { styles } from "../style/appStyles";

// Other data/helper functions
import data from '../data/moviesData.json';
import { console_log, fromNow } from '../utils/helper';
import Globals from  '../constants/Globals';

class History extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);
    }

    clearHistory() {
       // console_log('delet')
        axios.delete(vars.BASE_API_URL+"/history")
            .then((response) => {
                //console_log(response);
            })
            .catch((error) => {
                console_log(error);
            });
        this.props.clearHistory();
    }

    componentWillMount() {
        console_log('**user' , this.props.historyVideos.videos)
        axios.get(vars.BASE_API_URL+"/history")
            .then((history) => {
                if (history.data.data) {
                    this.props.addChannelHistory(history.data.data.channels);
                    this.props.addVideoHistory(history.data.data.videos);
                    this.props.addShowHistory(history.data.data.shows);
                }
            })
            .catch((error) => {
                this.props.hide();
                console_log(error);
            });

    }

    render() {
        return (
            <Container>
                <TabHeader
                    isDrawer={false}
                    isTitle={true}
                    title={Globals.type === 'es'? 'Historia' : 'History'}
                    isSearch={true}
                    showSearch={false}
                    rightLabel={Globals.type === 'es'? 'Eliminar' : 'Clear' }
                    rightClick={this.clearHistory.bind(this)}
                />
                {/*<ScrollView style={HistoryStyles.content} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 80}}>*/}
                    {/*<View style={[HistoryStyles.historyContent, {flex: 3}]}>*/}
                        <HistoryItems data={this.props.historyVideos.channels}/>
                    {/*</View>*/}

                    {/*<Footer />*/}
                {/*</ScrollView>*/}
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        header: state.HeaderReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        setHeaderTitle,
        clearHistory,
        addChannelHistory,
        addVideoHistory,
        addShowHistory,
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(History);
