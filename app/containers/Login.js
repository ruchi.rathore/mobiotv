import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import { Platform, StatusBar, TouchableOpacity, View, TouchableHighlight, TextInput, Text, Image, ImageBackground, AsyncStorage, KeyboardAvoidingView } from "react-native";

// Components
import Input from '../components/Input/Input';
import Loader from '../components/Loader/Loader';
import NavigationService from '../utils/NavigationService';

// Styles
import { styles } from "../style/appStyles";
import loginStyles from "../style/loginStyles";

// Actions
import { checkAccess } from '../actions/WelcomeActions';
import { show, hide } from '../actions/ActivityIndicatorActions';

// Other data/helper functions
import { loginLogo, background, logo } from "../assets/Images";
import { console_log } from "../utils/helper";
import * as vars from '../constants/api';
import {PaymentAndroid} from '../components/Payment/PaymentAndroid';
import {PaymentIOS} from '../components/Payment/PaymentIOS';
global.PaymentRequest = require('react-native-payments').PaymentRequest;
import Globals from  '../constants/Globals';
import SplashScreen from 'react-native-splash-screen'

//import PaymentRequest from 'react-native-payments';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accessCode: '',
            error: ''
        };
    }

    componentDidMount() {
        Platform.OS !== 'ios' ?  SplashScreen.hide() : null;
    }

    getAccessToken = () => {
        if (this.state.accessCode !== '') {
            const url = vars.BASE_API_URL + `/checkAccess?uid=${this.state.accessCode.toLowerCase()}`;
            this.props.show();
            axios.get(url)
                .then(res => {
                    if (res.data.success === true) {
                        this.props.checkAccess(res.data.data.token);
                        AsyncStorage.setItem('@AccessToken:key', res.data.data.token);
                        //NavigationService.reset("Drawer");
                        NavigationService.reset("TabNav");
                    } else {
                        this.setState({error: 'Please fill valid Access Code'});
                        this.props.hide();
                    }
                    //this.props.hide();
                })
        } else {
            this.setState({error: 'Please fill valid Access Code'});
        }
    }

    inAppPayment=()=>{
        if(Platform.OS==='ios') {
            PaymentIOS();
            /*const APPLE_PAY = {
                supportedMethods: ['apple-pay'],
                data: {
                    merchantIdentifier: 'merchant.com.MobioTV.app',
                    supportedNetworks: ['visa', 'mastercard', 'amex'],
                    countryCode: 'MY',
                    currencyCode: 'MYR'
                }
            }

            const ANDROID_PAY = {
                supportedMethods: ['android-pay'],
                data: {
                    merchantIdentifier: '13170441324646634616',
                    supportedNetworks: ['visa', 'mastercard', 'amex'],
                    countryCode: 'MY',
                    currencyCode: 'MYR'
                }
            }

            const METHOD_DATA = [
                APPLE_PAY,
                ANDROID_PAY
            ];


            const DETAILS = {
                id: 'basic-example',
                displayItems: [
                    {
                        label: 'Get Subscription',
                        amount: {currency: 'USD', value: '15.00'}
                    }
                ],
                total: {
                    label: 'MobioTV',
                    amount: {currency: 'USD', value: '15.00'}
                }
            };

            const OPTIONS = {
                requestPayerEmail: true,
                requestPayerPhone: true,
            };


            const paymentRequest = new PaymentRequest(METHOD_DATA, DETAILS);
            paymentRequest.show().then(paymentResponse => paymentResponse.complete("success"));
            //paymentRequest.show();paymentRequest.abort()*/
        }
        else{
            PaymentAndroid(
                (data)=>{
                    //console.log('here***', data);
                    if(data.purchaseState === 'PurchasedSuccessfully'){
                        this.setState({accessCode: 'fdf098fcc6'});
                        this.getAccessToken();
                    }
                },
                (error)=>{
                    //alert(error);
                    console.log(error);
                });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={loginStyles.content} behavior={Platform.OS=='ios'?'padding':''}>

                <ImageBackground source={background} style={{flex: 1}}>
                    <View style={loginStyles.logoView}>
                        <Image source={logo} style={loginStyles.logo} />
                    </View>
                    <View style={loginStyles.inputView}>
                        <Text style={[styles.avRegular, loginStyles.userNameText]}>{Globals.type == 'es' ? "CÓDIGO DE ACCESO" : 'ACCESS CODE'}</Text>
                        <View style={loginStyles.usernameView}>
                            {(this.state.error != '') ?
                                <Text style={[loginStyles.errorStyles]}>{this.state.error}</Text>
                                : null}
                            <TextInput
                                value={this.state.accessCode}
                                style={[styles.avRegular, loginStyles.inputV]}
                                onChangeText={(accessCode) => this.setState({ accessCode })}
                                keyboardType={"default"}
                                defaultValue={this.state.accessCode}
                                maxLength={40}
                                multiline={false}
                                autoCapitalize = "none"
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={loginStyles.loginButtonView}>
                        <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={() => this.getAccessToken()}>
                            <View style={loginStyles.loginButton}>
                                <Text style={[loginStyles.buttonText, styles.avRegular]}>
                                    {Globals.type == 'es' ? 'CONTINUAR' : 'CONTINUE'}
                                </Text>
                            </View>
                        </TouchableHighlight>

                        <TouchableOpacity style={{marginTop: 20, height: 40, alignSelf:'center'}} onPress = {()=> this.inAppPayment()}>
                            <Text style = {{color: '#fff'}}>{Globals.type == 'es' ? "Nuevo usuario" : "New User?" }
                                <Text style = {{color: 'red'}}>{Globals.type == 'es' ?  " Suscríbete aquí" : " Subscribe here" }</Text>
                            </Text>
                        </TouchableOpacity>

                    </View>

                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
            </KeyboardAvoidingView>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        access: state.WelcomeReducer,
        loader: state.ActivityIndicatorReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        checkAccess,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

