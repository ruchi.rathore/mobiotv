import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Video from 'react-native-af-video-player';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Orientation from 'react-native-orientation';
// Components
import Header from '../components/Header/PlayHeader';
import Footer from '../components/Footer/Footer';
import RelatedVideos from '../components/RelatedVideos/RelatedVideos';

// Styles
import { styles } from "../style/appStyles";
import PlayStyle from "../style/playStyle";

// Other data/helper functions
import data from '../data/moviesData.json';
import { playBackground, movie_1, tv_4, tv_3 } from "../assets/Images";
import NavigationService from "../utils/NavigationService";
import * as vars from '../constants/api';
import relatedVideosStyles from '../style/relatedVideosStyles';
import { messages } from '../constants/messages';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addLikesVideos, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { addChannelHistory, addVideoHistory, addShowHistory } from '../actions/HistoryActions';
import { console_log } from '../utils/helper';
import Loader from '../components/Loader/Loader';
import {movies} from '../constants/movies';
import {esmobiotv} from '../constants/esmobiotv';
import {bidiotvMoviesData} from '../constants/bidiotvmovies';
import Search from '../components/Search/Search';
import MessageBar from '../components/Message/Message';
import Globals from  '../constants/Globals';
import ReadMore from 'react-native-read-more-text';
import LinearGradient from 'react-native-linear-gradient';
//import DeviceType from '../../App';
let movies1 = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;
let bidiotvMovies = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;

class PlayVOD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            favoriteSwitch: false,
            isPlaying: false,
            playUrl: '',
            videoData:props.play.data.video,
            tempVideoData: props.play.data.video.subCategory,
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
            curentCategory:[]
        };
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _onPressButton(data) {
        //console_log('ghere***', data);
        if(this.state.tempVideoData !== "" && this.state.tempVideoData !== undefined) {
            let tempArray = this.state.tempVideoData;
            tempArray[data.index]= this.state.data.video;
            this.setState({
                data: Object.assign({video: data.video}),
                tempVideoData:tempArray,
            })
        }
        this.setState({isPlaying: false});
        setTimeout(() => {
            this.Scrollview.scrollTo({x: 0,y:0, animated: true});
        }, 1)

        //this.Scrollview.contentOffset({x: 0,y:0})
        this.props.getVideoOrChannelRelatedData(data);
    }

    componentWillMount() {
       // console.log('this.props.play:', this.props.play)
        if (this.props.play.data.video) {
            this.getVideoCategories(this.props.play.data.video.id);
        }
    }

    componentDidMount() {
        //Orientation.lockToLandscape();
        //this.props.show();
        //setTimeout(() => {
            this.props.hide();
            this.setState({dataLoad: true});
        //}, 1500);
    }

    componentWillReceiveProps(nextProps) {

        if (this.props.play.data.video.id != nextProps.play.data.video.id) {
            this.getVideoCategories(nextProps.play.data.video.id);
        }
    }

     isArabic = (text) =>{
        var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
        result = pattern.test(text);
        return result;
    }

    getVideoCategories(videoId) {
       // console_log('videoId', videoId)
        let videoCategory = movies1.find((category) => {
            let videoIds = category.videos.map(v => v.id);
            if (~videoIds.indexOf(videoId)) {
                return category;
            }
        });
        if(videoCategory !== undefined){
            let video = videoCategory.videos.find((v) => v.id == videoId);
            this.setState({
                data: Object.assign({video: video}, {category: videoCategory}),
                curentCategory:videoCategory
            });
        }


    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    addRemoveFavorite(video) {
            this.videoFavorite(video);
    }


    videoFavorite(video) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == video.id;
        });
        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            this.props.showMessage({
                message: messages.addToFavorites,
                type: true
            });
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            this.props.showMessage({
                message: messages.removeFromFavorites,
                type: false
            });
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                   // console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    playVideo(video) {
        //if(this.state.curentCategory.name=='Arabic Entertainment') {
            this.setState({
                isPlaying: !this.state.isPlaying
            });
            video.video.videoUrl !== undefined && video.video.videoUrl !== '' ? this.props.navigation.navigate('Player', {url: video.video.videoUrl}) : null;
        //}
        this.addHistoryVideo();
    }

    addHistoryVideo() {
        let video = this.state.data.video;
        let historyVideos = this.props.historyVideos.shows;
        let indexOf = historyVideos.findIndex((f) => {
            return f.videoId == video.id;
        });
        let videoToBeUpdated = {
            showId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        //console_log('videoToBeUpdated:', video);

        if (indexOf == -1) {
            historyVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/history/shows", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    //console_log(error);
                });
        }

        this.props.addShowHistory(historyVideos);
    }

    addLike(video) {
        this.videoLike(video);
    }

    videoLike(video) {
        let likedVideos = this.props.favorite.videosLiked;
        let indexOf = likedVideos.findIndex((f) => {
            return f.videoId == video.id;
        });

        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            likedVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/likes/videos", videoToBeUpdated)
                .then((response) => {
                   // console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        } else {
            likedVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/likes/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addLikesVideos(likedVideos);
    }

    isVideoLiked(videoId) {
        let indexOf = this.props.favorite.videosLiked.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _handleFavoriteClicked() {

    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    _renderPlayContent(video) {
        //alert('here');
        //console_log('this.state.playUrl', this.state.playUrl)
        return (
            /*((this.state.isPlaying && video.video.videoUrl !== undefined )) ?
                <View>
                    <Video
                        url={ video.video.videoUrl }
                        autoPlay={true}
                        rotateToFullScreen={true}
                    />
                </View>
                :*/
                <View>
                    <View style={PlayStyle.imageContainer}>
                        <Image style={{ height: 220, width: 150 }} source={video.video.preview} />
                    </View>
                    <View style={{ paddingTop: 40 }}>
                        <View style={PlayStyle.playButtonView}>
                            <TouchableOpacity onPress={this.playVideo.bind(this, video)} underlayColor="transparent" activeOpacity={0.6}>
                                <View style={[PlayStyle.playButton, { backgroundColor : video.video.videoUrl === '' ? '#D67A7A' : '#db0101'}]}>
                                    <Icon name="play" size={18} style={{ backgroundColor: 'transparent', paddingRight: 10 }} color="#fff" />
                                    <Text style={[PlayStyle.buttonText, styles.avRegular]}>
                                        {video.video.videoUrl === '' ? "No disponible en demostración" :  "Reproducir"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

        )
    }

    LoadRelatedVOD = (video) =>{
       // console_log('inide load data')
        return(
            <View>
                {this.state.tempVideoData !== "" && this.state.tempVideoData !== undefined?
                    <View>
                        {this.state.tempVideoData.map((item, index) => {
                            return (
                                <View  style={[relatedVideosStyles.relatedItemView, {borderBottomColor : '#606060'} ]} key={index}>
                                    <View style={{flexDirection: 'row', alignItems: 'center',}} >
                                    <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})} style={[relatedVideosStyles.imageWrapper]}>
                                            <ImageBackground style={relatedVideosStyles.imageBackground} resizeMode="stretch" source={item.preview}></ImageBackground>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})} style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                        <Text style={[relatedVideosStyles.relatedItemTitle]}>{item.name}</Text>
                                        <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{'Hollywood Blockbuster'}</Text>
                                        <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{'PG, 1hr 38min'}</Text>
                                        <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{'478 likes | 32 Views'}</Text>
                                        <View style={[relatedVideosStyles.titleIcon]}>
                                            <MaterialIcons name='play-circle-outline' size={20} style={[relatedVideosStyles.relatedItemIcon]} color='white' />
                                            <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{item.duration}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[relatedVideosStyles.relatedItemIconWrapper]} onPress={this.addRemoveFavorite.bind(this, item)}>
                                        <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 21} style={[relatedVideosStyles.relatedItemIcon]} color={this.isVideoFavorite(item.id) ? "#FFC107" : "#fff"} />
                                    </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })
                        }
                    </View>
                    :
                    <View>
                        {video.category.videos.map((item, index) => {
                            if (item.id != video.video.id) {
                                return (
                                    <View style={[relatedVideosStyles.relatedItemView , {borderBottomColor :  '#606060'} ]} key={index}>
                                        <View style={{flexDirection: 'row'}} onPress={this._onPressButton.bind(this, {video: item})}>
                                        <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})} style={[relatedVideosStyles.imageWrapper,{marginTop: '3%', marginBottom: '3%'}]}>
                                            <View>
                                                <ImageBackground style={[relatedVideosStyles.imageBackground]} resizeMode={"stretch"} source={item.preview}>
                                                    {/*<View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>*/}
                                                </ImageBackground>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})} style={{flexDirection: 'column',flex: 5,paddingLeft: '2%',paddingVertical: '3%'}}>
                                            <Text style={[styles.avRegular, relatedVideosStyles.relatedItemTitle]}>{item.name}</Text>
                                            <Text style={[styles.avRegular, relatedVideosStyles.relatedItemSubTitle, { paddingLeft: '0%', marginTop: 10}]}>{'Hollywood Blockbuster'}</Text>
                                            <Text style={[styles.avRegular, relatedVideosStyles.relatedItemSubTitle, { paddingLeft: '0%',}]}>{'PG, 1hr 38min'}</Text>
                                            <Text style={[styles.avRegular, relatedVideosStyles.relatedItemSubTitle, { paddingLeft: '0%',}]}>{'478 likes | 32 Views'}</Text>
                                            <View style={[relatedVideosStyles.titleIcon]}>
                                                <FeatherIcon name='play-circle' size={Globals.DeviceType === 'Phone' ?  23 : 30} style={[relatedVideosStyles.relatedItemIcon, {marginTop: 6}]} color='white' />
                                                {/*<Text style={[relatedVideosStyles.relatedItemSubTitle]}>{item.duration}</Text>*/}
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[relatedVideosStyles.relatedItemIconWrapper, {marginTop: '3%'}]} onPress={this.addRemoveFavorite.bind(this, item)}>
                                            <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 23} style={[relatedVideosStyles.relatedItemIcon]} color={this.isVideoFavorite(item.id) ? "#FFC107" : "#fff"} />
                                        </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            }
                        })
                        }
                    </View>
                }
            </View>
        )
    }

    _renderTruncatedFooter = (handlePress) => {
        return (
            <Text style={[styles.avRegular, {color: '#fff', marginTop: 5, alignSelf: 'flex-end'}]} onPress={handlePress}>
                Ver mas
            </Text>
        );
    }

    _renderRevealedFooter = (handlePress) => {
        return (
            <Text style={[styles.avRegular, {color: '#fff', marginTop: 5, alignSelf: 'flex-end'}]} onPress={handlePress}>
                Ocultar
            </Text>
        );
    }

    render() {
        let video = this.state.data;
        return (
            <Container>
                <ImageBackground source={playBackground} style={{ zIndex: 999 }}>
                    <Header
                        isDrawer={false}
                        isTitle={true}
                        title={video.video.name}
                        isSearch={true}
                        showSearch={true}
                        rightLabel=''
                    />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
                {/*<Search from={"videos"}/>*/}
                <View style={PlayStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView ref={component => this.Scrollview = component} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 80}}>
                        <LinearGradient colors={['#225F94',  'transparent', '#000', '#000', ]}
                                        style={{ flex: 3, zIndex: 10, marginTop: 0, }}
                                        >
                        {/*<ImageBackground source={playBackground} style={{ flex: 1, zIndex: 9, marginTop: 0 }}>*/}

                            <View>
                                {this._renderPlayContent(video)}
                            </View>
                        {/*</ImageBackground>*/}
                        <View style={{ paddingRight: '4%', marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '4%'}} >
                                <View style={{ flex: 5 }}>
                                    <Text numberOfLines={5} style={[styles.avBold, PlayStyle.videoTitle]}>{video.video.name}</Text>
                                </View>
                                <View style={{ alignItems: 'center', flex: Globals.DeviceType === 'Phone' ?  2 : 1 }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addRemoveFavorite.bind(this, video.video)}>
                                        <Icon name='star' size={20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(video.video.id) ? "#FFC107" : "#fff"} />
                                    </TouchableOpacity>
                                    <Text style={[styles.avRegular, PlayStyle.watchLaterText]}>Ver más tarde</Text>
                                </View>
                                <View style={PlayStyle.rateView}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addLike.bind(this, video.video)}>
                                        <Icon name='thumbs-up' size={22} style={{ backgroundColor: 'transparent' }} color = {this.isVideoLiked(video.video.id) ? "#699bff" : "#fff"} />
                                    </TouchableOpacity>
                                    <Text style={[styles.avRegular, PlayStyle.rateText]}>{'    ' + 'Puntuación'}</Text>
                                </View>
                            </View>
                            <View style={[PlayStyle.descriptionView, {width: '100%'}]}>
                                <View style={PlayStyle.durationView}>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>PG-13 | </Text>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>2h 6min | </Text>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>{video.video.year}‎</Text>
                                </View>
                                <View>
                                    <Text style={[styles.avRegular, PlayStyle.durationText, { color: '#2770ba' }]}>{(video.category) ? video.category.name : ""}</Text>
                                </View>
                            </View>
                            <View style = {{justifyContent: 'center', marginTop: 12, marginBottom : 12,  alignSelf: 'center', marginLeft: Globals.DeviceType === 'Phone' ? 13 : 27}}>
                                <ReadMore
                                    renderTruncatedFooter={this._renderTruncatedFooter}
                                    renderRevealedFooter={this._renderRevealedFooter}
                                    numberOfLines={4}>
                                <Text style = {[styles.avRegular,{color : '#ffffff', fontSize: 12, textAlign: this.isArabic(video.video.desc) ? 'right' : 'justify' }]}>{(video.video.desc)? video.video.desc : ""}</Text>
                            </ReadMore>
                            </View>
                        </View>
                        {/*<View style={{ flex: 1, alignItems: 'flex-start', paddingBottom: '4%', paddingLeft: '4%' }}>*/}
                            {/*<View style={PlayStyle.videoInfoView}>*/}
                                {/*<Text style={[styles.avRegular, PlayStyle.infoTitle]}>Director:</Text>*/}
                                {/*<Text style={[styles.avRegular, PlayStyle.infoContent]}>NA</Text>*/}
                            {/*</View>*/}
                            {/*<View style={PlayStyle.videoInfoView}>*/}
                                {/*<Text style={[styles.avRegular, PlayStyle.infoTitle]}>Escritor:</Text>*/}
                                {/*<Text style={[styles.avRegular, PlayStyle.infoContent]}>NA</Text>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                        {this.state.curentCategory?
                        <View style={[relatedVideosStyles.wrapperView]}>
                            <View style={[relatedVideosStyles.textAndSwitchView]}>
                                <View>
                                    <Text style={[styles.avRegular, relatedVideosStyles.headerText]}>
                                        {"Videos relacionados"}
                                    </Text>
                                </View>
                                <View style={[relatedVideosStyles.switchWrapper]}>
                                    <Text style={[styles.avRegular, relatedVideosStyles.subHeaderText]}>
                                        Auto-reproducción
                                    </Text>
                                    <Switch style={[relatedVideosStyles.switch]} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                                </View>
                            </View>
                                {this.state.dataLoad? this.LoadRelatedVOD(video) : null}
                        </View>
                            : null}
                        <View style={{ paddingBottom: 20 }}>
                            {/*<Footer />*/}
                        </View>
                        </LinearGradient>
                    </ScrollView>
                </View>
            </Container >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
        addChannelHistory,
        addVideoHistory,
        addShowHistory,
        addLikesVideos
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayVOD);
