import React, { Component } from "react";
import { Dimensions,View, TouchableHighlight, TextInput, Text, Image, ImageBackground, ScrollView, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import axios from 'axios';
// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import FavoriteChannels from '../components/WatchLaterChannels/WatchLaterChannels';
import FavoriteVODs from '../components/WatchLaterVOD/WatchLaterVOD';
import Loader from '../components/Loader/Loader';
// Styles
import { styles } from "../style/appStyles";
import liveChannelStyle from "../style/liveChannelStyle";
import favoriteStyles from "../style/favoriteStyle";
import { movies } from '../constants/movies';
import { thumbnail } from "../assets/Images";
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import NavigationService from "../utils/NavigationService";
import { messages } from '../constants/messages';
import { console_log } from '../utils/helper';
import * as vars from '../constants/api';
import Search from '../components/Search/Search';
// Other data/helper functions
import data from '../data/moviesData.json';
import MessageBar from '../components/Message/Message';
import Globals from "../constants/Globals";
import { esmobiotv } from '../constants/esmobiotv';
import { bidiotvMoviesData } from '../constants/bidiotvmovies';
let bidiotvMovies = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;
//import DeviceType from '../../App';

class WatchLater extends Component {
    constructor(props) {
        super(props);
        this.state = {
            liveChannelTab: true,
            videoOnDemand: false,
            color: '',
            message:'',
            showMessage:false,
        }
    }

    componentDidMount() {
        this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);
    }

    onExploreClicked() {
        //console_log(this.state.liveChannelTab)
        if (this.state.liveChannelTab){
            NavigationService.navigate('LiveChannels');
        }
        else{
            NavigationService.navigate('VOD');
        }
    }

    onPressButton(data) {
        if (this.state.liveChannelTab) {
            NavigationService.navigate('Play');
        }
        else{
            NavigationService.navigate('PlayOthers');
        }
        this.props.getVideoOrChannelRelatedData(data);
    }

    onPressButtonVideo(data){
        NavigationService.navigate('PlayVOD');
        this.props.getVideoOrChannelRelatedData(data);
    }

    onPressButtonOthers(data){
        NavigationService.navigate('PlayOthers');
        this.props.getVideoOrChannelRelatedData(data);
    }


    channelFavorite(data) {
        let favoriteChannels = this.props.favorite.channels;
        let indexOf = favoriteChannels.findIndex((f) => {
            return f.channelId == data.channel.channelId;
        });
        let channelToBeUpdated = {
            channelId: data.channel.channelId,
            channelName: data.channel.channelName,
            channelLang: data.channel.channelLang,
            channelImage: data.channel.channelImage,
            channelLogo: data.channel.channelLogo,
            channelGrade: data.channel.channelGrade,
            channelStatus: data.channel.channelStatus,
            channelBaseline: data.channel.channelBaseline,
            channelCategories: data.channel.channelCategories,
            isFavorite: true
        };

        if (indexOf == -1) {
            favoriteChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/channels", channelToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteChannel(favoriteChannels);
    }

    _handleFavoriteClicked(data) {
       // console.log(JSON.stringify(data));
        this.videoFavorite(data.video);
    }

    videoFavorite(data) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == data.id;
        });
        let videoToBeUpdated = {
            videoId: data.id,
            duration: data.duration,
            name: data.name,
            preview: data.preview
        };

        if (indexOf == -1) {
            this.props.showMessage({
                message: messages.addToFavorites,
                type: true
            });
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            this.props.showMessage({
                message: messages.removeFromFavorites,
                type: false
            });
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    isChannelFavorite(channelId) {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    render() {
        const liveChannelStyleNew = this.state.liveChannelTab ? favoriteStyles.liveChannelSelected : favoriteStyles.liveChannelDeselected;
        const VODStyle = this.state.videoOnDemand ? favoriteStyles.VODSelected : favoriteStyles.VODDeselected;
        const liveChannelText = this.state.liveChannelTab ? '#f7843e' : '#fff';
        const VODText = this.state.videoOnDemand ? '#d51a92' : '#fff';

        let favoriteChannelIds = this.props.favorite.channels.map(c => c.channelId);
        let favoriteChannels = this.props.category.channels.filter((channel) => {
            if (~favoriteChannelIds.indexOf(channel.channelId)) {
                return channel;
            }
        });
        let favoriteVideoIds = this.props.favorite.videos.map(v => v.videoId);
        let favoriteVideos = [].concat.apply([], this.props.category.videos.map((c) => c.videos)).filter((v) => {
            if (~favoriteVideoIds.indexOf(v.id)) {
                return v;
            }
        });
        let favouriteRecentlyAdded = [].concat.apply([], movies.map((c) => c.videos)).filter((v) => {
            if (~favoriteVideoIds.indexOf(v.id)) {
                return v;
            }
        });

        let bidiotvfavourite = [].concat.apply([], bidiotvMovies.map((c) => c.videos)).filter((v) => {
            if (~favoriteVideoIds.indexOf(v.id)) {
                return v;
            }
        });

        return (
            <Container>
                <ImageBackground  style={{ zIndex: 999 }}>
                <Header
                    isDrawer={false}
                    isTitle={true}
                    showSearch={true}
                    title={Globals.type=== 'es' ?  'Favoritos': 'Favorites'}
                    isSearch={true}
                    rightLabel=''
                />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
                {/*<Search from={""}/>*/}
                <View style={favoriteStyles.content}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView style={{marginTop: 25}} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 100}}>
                        {Globals.url ===  'http://uk.mobiotv.com' ?
                            <View style={{ flex: 3, backgroundColor: 'black' }}>
                                {(bidiotvfavourite.length > 0) ?
                                    Globals.type === 'es' ?
                                <View style={{backgroundColor: 'black', flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                                    {bidiotvfavourite.map((video, index) => {//const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                                        return (
                                            <View style={liveChannelStyle.imageVideoThmbnail}
                                                  key={index}>
                                                <TouchableOpacity onPress={this.onPressButtonVideo.bind(this, {video: video})}>
                                                    <ImageBackground style={[liveChannelStyle.imageVideoListBackground]} resizeMode= 'stretch' source={video.preview}>
                                                        <TouchableOpacity style={liveChannelStyle.tvFavoriteVideoBg} onPress={this._handleFavoriteClicked.bind(this, {video: video})}>
                                                            <View style={liveChannelStyle.tvFavoriteView}>
                                                                <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{backgroundColor: 'transparent'}} color={this.isVideoFavorite(video.id) ? "#FFC107" : "#fff"}/>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })
                                    }
                                </View>
                                        :
                                        <View style={[favoriteStyles.renderContentNew]}>
                                            {bidiotvfavourite.map((video, index) => {//const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                                                return (
                                                    <View key={index} style={favoriteStyles.favoriteItem}>
                                                        <TouchableOpacity onPress={this.onPressButtonOthers.bind(this, {video: video})}  style={[favoriteStyles.favoriteItemImage]}>
                                                            <View >
                                                                <ImageBackground style={[favoriteStyles.favoriteItemImageB]} resizeMode= 'stretch' source={video.preview}>
                                                                    <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>
                                                                </ImageBackground>
                                                            </View>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={this.onPressButtonOthers.bind(this, {video: video})}  style={favoriteStyles.favoriteTitle}>
                                                            <Text numberOfLines={1} style={[styles.avRegular, favoriteStyles.favoriteItemTitle]}>{video.name}</Text>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{ flex: 2 }} onPress={this._handleFavoriteClicked.bind(this, {video: video})}>
                                                            <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={[favoriteStyles.favouriteHeartIcon, {alignSelf: 'flex-end'}]} color={this.isVideoFavorite(video.id) ? "#FFC107" : "#fff"} />
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            })
                                            }
                                        </View>
                                :
                                <View>
                                    <View style={favoriteStyles.noChannelsViewTitle}>
                                        <Text style={[styles.avRegular, favoriteStyles.noData]}>{Globals.type=== 'es' ?  "Ningún video añadido para ver más tarde" : "No Videos Added To Watch Later Yet"}</Text>
                                    </View>
                                    <View style={favoriteStyles.noChannelsViewDesc}>
                                        <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>{Globals.type=== 'es' ?  "Agrega tus videos favoritos para poder verlos fácilmente y sin problemas." : "Add your favorite videos to access and watch easily without any hassles."}</Text>
                                    </View>
                                </View>
                                }
                                <TouchableOpacity  onPress={()=> NavigationService.goBack("null")} style={[favoriteStyles.exploreButtonView]}>
                                    <View style={[favoriteStyles.exploreButton, {backgroundColor: "#e83e8c"}]}>
                                        <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                            {Globals.type=== 'es' ? "Explora más videos" : "Explore Videos"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            :
                            <View style={{ flex: 3, backgroundColor: 'black' }}>
                                <View style={{ flex: 1, flexDirection: 'row', height: 40 }}>
                                    <TouchableOpacity style={[favoriteStyles.tabStyle, liveChannelStyleNew]} onPress={() => { this.setState({ liveChannelTab: true, videoOnDemand: false }) }}>
                                        <Text style={{ color: liveChannelText, fontSize: 14, textAlign: 'center' }}>Live Channels</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[favoriteStyles.tabStyle, VODStyle]} onPress={() => { this.setState({ liveChannelTab: false, videoOnDemand: true }) }}>
                                        <Text style={{ color: VODText, fontSize: 14, textAlign: 'center' }}>Video on Demand</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={ favoriteStyles.favoriteContentView }>
                                    { (this.state.liveChannelTab) ?
                                        (favoriteChannels.length > 0) ?
                                            <View style={[favoriteStyles.renderContent]}>
                                                {
                                                    favoriteChannels.map((channel, index) => {
                                                        //const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                                                        return (
                                                            <TouchableOpacity key={index} style={favoriteStyles.favoriteItem} onPress={this.onPressButton.bind(this, {channel: channel})}>
                                                                <View style={[favoriteStyles.favoriteItemImage]} >
                                                                    <ImageBackground style={[favoriteStyles.favoriteItemImageB]}  source={{uri:channel.channelImage ? vars.BASE_URL+"uploads/"+channel.channelImage : thumbnail}}>
                                                                        <View style={[favoriteStyles.favoriteItemImageInnerView]}>
                                                                            <View style={[favoriteStyles.favouriteItemLogoWrapperView]}>
                                                                                <Image style={[favoriteStyles.favoriteItemChannelLogo]} source={{uri:channel.channelLogo}} />
                                                                            </View>
                                                                        </View>
                                                                    </ImageBackground>
                                                                </View>
                                                                <View style={favoriteStyles.favoriteTitle}>
                                                                    <Text style={[styles.avRegular, favoriteStyles.favoriteItemTitle]}>{channel.channelName}</Text>
                                                                </View>
                                                                <TouchableOpacity style={{ flex: 2 }} onPress={this.channelFavorite.bind(this, {channel: channel})}>
                                                                    <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 21} style={[favoriteStyles.favouriteHeartIcon, {alignSelf: 'flex-end'}]} color={this.isChannelFavorite(channel.channelId) ? "#FFC107" : "#fff"} />
                                                                </TouchableOpacity>
                                                            </TouchableOpacity>
                                                        )
                                                    })
                                                }
                                            </View>
                                            :
                                            <View>
                                                <View style={favoriteStyles.noChannelsViewTitle}>
                                                    <Text style={[styles.avRegular, favoriteStyles.noData]}>No Channels Added To Watch Later Yet</Text>
                                                </View>
                                                <View style={favoriteStyles.noChannelsViewDesc}>
                                                    <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>Add your favorite channels to access and watch easily without any hassles.</Text>
                                                </View>
                                            </View>
                                        :
                                        (favoriteVideos.length > 0 || favouriteRecentlyAdded.length > 0) ?
                                            <View style={[favoriteStyles.renderContent]}>
                                                {favoriteVideos.map((video, index) => {//const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                                                    return (
                                                        <TouchableOpacity onPress={this.onPressButton.bind(this, {video: video})} key={index} style={favoriteStyles.favoriteItem}>
                                                            <View style={[favoriteStyles.favoriteItemImage]}>
                                                                <View >
                                                                    <ImageBackground style={[favoriteStyles.favoriteItemImageB]} resizeMode= 'stretch' source={{uri: video.preview}}>
                                                                        <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>
                                                                    </ImageBackground>
                                                                </View>
                                                            </View>
                                                            <View style={favoriteStyles.favoriteTitle}>
                                                                <Text numberOfLines={1} style={[styles.avRegular, favoriteStyles.favoriteItemTitle]}>{video.name}</Text>
                                                            </View>
                                                            <TouchableOpacity style={{ flex: 2 }} onPress={this._handleFavoriteClicked.bind(this, {video: video})}>
                                                                <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 21} style={[favoriteStyles.favouriteHeartIcon, {alignSelf: 'flex-end'}]} color={this.isVideoFavorite(video.id) ? "#FFC107" : "#fff"} />
                                                            </TouchableOpacity>
                                                        </TouchableOpacity>
                                                    )
                                                })
                                                }
                                                {favouriteRecentlyAdded.map((video, index) => {//const favoriteIconColor = item.favorite ? '#ed145b' : '#fff';
                                                    return (
                                                        <TouchableOpacity key={index} style={favoriteStyles.favoriteItem} onPress={this.onPressButtonVideo.bind(this, {video: video})}>
                                                            <View style={[favoriteStyles.favoriteItemImage]}>
                                                                <View >
                                                                    <ImageBackground style={[favoriteStyles.favoriteItemImageB]} source={video.preview}>
                                                                        <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>
                                                                    </ImageBackground>
                                                                </View>
                                                            </View>
                                                            <View style={favoriteStyles.favoriteTitle}>
                                                                <Text numberOfLines={1} style={[styles.avRegular, favoriteStyles.favoriteItemTitle]}>{video.name}</Text>
                                                            </View>
                                                            <TouchableOpacity style={{ flex: 1 }} onPress={this._handleFavoriteClicked.bind(this, {video: video})}>
                                                                <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 21} style={[favoriteStyles.favouriteHeartIcon]} color={this.isVideoFavorite(video.id) ? "#FFC107" : "#fff"} />
                                                            </TouchableOpacity>
                                                        </TouchableOpacity>
                                                    )
                                                })
                                                }
                                            </View>
                                            :
                                            <View>
                                                <View style={favoriteStyles.noChannelsViewTitle}>
                                                    <Text style={[styles.avRegular, favoriteStyles.noData]}>No Channels Added To Watch Later Yet</Text>
                                                </View>
                                                <View style={favoriteStyles.noChannelsViewDesc}>
                                                    <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>Add your favorite channels to access and watch easily without any hassles.</Text>
                                                </View>
                                            </View>
                                    }
                                </View>
                                <TouchableOpacity  onPress={this.onExploreClicked.bind(this)} style={[favoriteStyles.exploreButtonView]}>
                                    <View style={[favoriteStyles.exploreButton, {backgroundColor: this.state.liveChannelTab == true ? '#f7843e' : "#e83e8c"}]}>
                                        <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                            {this.state.liveChannelTab == true ? 'Explore Channels' : 'Explore Videos'}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                        <Footer />
                    </ScrollView>

                </View>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(WatchLater);
