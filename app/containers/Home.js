import React, { Component } from "react";
import { Image, View, StatusBar, Linking, TouchableHighlight, Text, ImageBackground } from "react-native";
import { Container, H3, Button, Title, Body, Left, Right } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/dist/Entypo';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import Loader from '../components/Loader/Loader';
import NavigationService from '../utils/NavigationService';
import * as vars from '../constants/api';

// Styles
import { styles } from "../style/appStyles";
import homeStyles from "../style/homeStyles";
import Orientation from 'react-native-orientation';

// Actions
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getDetails, getInterests } from '../actions/AccountActions';
import { getVideosPackages, getVideos, getTVCategories, getChannels } from '../actions/CategoryActions';
import { addFavoriteChannel, addFavoriteVideo, addLikesChannels, addLikesVideos } from '../actions/FavoriteActions';
import { addVideoHistory, addChannelHistory } from '../actions/HistoryActions';

// Other data/helper functions
import { banner } from "../assets/Images";
import { console_log } from "../utils/helper";

class Home extends Component {
    constructor(props) {
        super(props);
        this.navigateChannels = this.navigateChannels.bind(this);
        this.navigateSmartTV = this.navigateSmartTV.bind(this);
        this.navigateVOD = this.navigateVOD.bind(this);
        this.navigateWatchLater = this.navigateWatchLater.bind(this);
        this.state = {

        }
    }

    navigateChannels() {
        NavigationService.navigate("LiveChannels");
    }

    navigateVOD() {
        NavigationService.navigate("VOD");
    }

    navigateSmartTV() {
        NavigationService.navigate('SmartTV');
    }

    navigateWatchLater() {
        NavigationService.navigate("WatchLater");
    }

    componentDidMount() {
        //alert('here');
        Orientation.lockToPortrait();
        axios.defaults.headers.common['authorization'] = this.props.access;
        if(this.props.category.categories.length === 0){
            this.props.show();
            axios.all([axios.get(vars.BASE_API_URL+'/getUserProfile'), axios.get(vars.BASE_API_URL+'/interests'), axios.get(vars.BASE_API_URL+'/categories'), axios.get(vars.BASE_API_URL+'/channels'), axios.get(vars.BASE_API_URL+'/packages'), axios.get(vars.BASE_API_URL+'/favorites'), axios.get(vars.BASE_API_URL+'/history'), axios.get(vars.BASE_API_URL+'/likes')])
                .then(axios.spread((userProfile, interests, categories, channels, packages, favorites, history, likes) => {
                    // for user details
                    if (userProfile.data.data) {
                        this.props.getDetails(userProfile.data.data);
                    }

                    // for interests
                    if (interests.data.data) {
                        this.props.getInterests(interests.data.data);
                    }

                    // for channel categories
                    if (categories.data.data) {
                        this.props.getTVCategories(categories.data.data);
                    }

                    // for channels
                    if (channels.data.data) {
                        this.props.getChannels(channels.data.data);
                    }

                    // for packages of Video On Demand
                    if (packages.data.data.packages) {
                        this.props.getVideosPackages(packages.data.data.packages);

                        // for Videos On Demand
                        axios.all(packages.data.data.packages.map(l => axios.get(vars.BASE_API_URL+'/packages/'+l.id)))
                            .then(axios.spread((...res) => {
                                // all requests are now complete
                                res.map((packageDetails) => {
                                    if (packageDetails) {
                                        this.props.getVideos(packageDetails.data.data.package);
                                    }
                                });
                            }));
                    }

                    // for favorites
                    if (favorites.data.data) {
                        this.props.addFavoriteChannel(favorites.data.data.channels);
                        this.props.addFavoriteVideo(favorites.data.data.videos);
                    }

                    // for history
                    if (history.data.data) {
                        this.props.addChannelHistory(history.data.data.channels);
                        this.props.addVideoHistory(history.data.data.videos);
                    }

                    // for likes
                    if (likes.data.data) {
                        this.props.addLikesChannels(likes.data.data.channels);
                        this.props.addLikesVideos(likes.data.data.videos);
                    }

                    this.props.hide();
                }));
        }
    }

    render() {
        return (
            <Container>
                <Header
                    isDrawer={true}
                    isTitle={false}
                    title=''
                    isSearch={false}
                    rightLabel=''
                />

                <View style={homeStyles.content}>
                    <View style={homeStyles.imageView}>
                        <ImageBackground style={{ width: '100%', height: '100%', justifyContent: 'center' }} source={banner}>
                            <Text style={[styles.avRegular, homeStyles.bannerText]}>
                                Say Hello To The Future Of Mobiletainment
                            </Text>
                        </ImageBackground>
                    </View>

                    <View style={homeStyles.buttonView}>
                        <View>
                            <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={this.navigateChannels}>
                                <View style={[homeStyles.homePageButton, homeStyles.liveTvButton]}>
                                    <FeatherIcon name="radio" size={28} style={homeStyles.iconLiveTv} color="#fff" />
                                    <Text style={[styles.avRegular, homeStyles.buttonText]}>
                                        LIVE CHANNELS
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={this.navigateVOD}>
                                <View style={[homeStyles.homePageButton, homeStyles.VODbutton]}>
                                    <EntypoIcon name="controller-play" size={28} style={homeStyles.iconLiveTv} color="#fff" />
                                    <Text style={[styles.avRegular, homeStyles.buttonText]}>
                                        VIDEO-ON-DEMAND
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={homeStyles.subButtonsView}>
                            <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={this.navigateWatchLater} style={{ flex: 1 }}>
                                <View style={homeStyles.subButtons}>
                                    <Icon name="heart" size={15} style={{ backgroundColor: 'transparent' }} color="#fff" />
                                    <Text style={[styles.avRegular, homeStyles.subButtonsText]}>Watch Later</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={this.navigateSmartTV}  style={{ flex: 1 }}>
                                <View style={homeStyles.subButtons}>
                                    <Icon name="list-alt" size={15} style={{ backgroundColor: 'transparent' }} color="#fff" />
                                    <Text style={[styles.avRegular, homeStyles.subButtonsText]}>Smart TV</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <Footer />
                </View>
                <Loader visible={this.props.loader.isLoading} />
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        access: state.WelcomeReducer.token,
        loader: state.ActivityIndicatorReducer,
        category: state.CategoryReducer,
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getDetails,
        getInterests,
        getVideosPackages,
        getVideos,
        getTVCategories,
        getChannels,
        addFavoriteChannel,
        addFavoriteVideo,
        addChannelHistory,
        addVideoHistory,
        addLikesChannels,
        addLikesVideos
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

