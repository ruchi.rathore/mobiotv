import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, ImageBackground, ScrollView, FlatList, TouchableOpacity, Image, TouchableHighlight } from "react-native";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Loader from '../components/Loader/Loader';
import axios from 'axios';
// Components
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import MessageBar from '../components/Message/Message';
// Styles
import { styles } from "../style/appStyles";
import smartTVStyles from "../style/smartTVStyles";
import { show, hide } from '../actions/ActivityIndicatorActions';
import { smartTvData } from '../actions/SmartTVActions';
import { addFavoriteChannel } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
// Other data/helper functions
import data from "../data/moviesData.json";
import { messages } from '../constants/messages';
import NavigationService from "../utils/NavigationService";
import { console_log, isBetween, formatDate } from '../utils/helper';
import * as vars from '../constants/api';
import {thumbnail} from "../assets/Images";


class SmartTVGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            channelId: this.props.navigation.state.params.channel,
            channel: {},
            currentTime: new Date()
        };
        this.getSmartTvChannelGuide = this.getSmartTvChannelGuide.bind(this);
        this.isChannelFavorite = this.isChannelFavorite.bind(this);
    }

    componentWillMount() {
        // if (this.props.location.pathname.split('/')[2]){
        //     this.setState({
        //         channelId: this.props.location.pathname.split('/')[2]
        //     });
        //     this.getSmartTvChannelGuide(this.props.location.pathname.split('/')[2]);
        // } else {
        //     this.props.history.goBack();
        // }

    }

    getSmartTvChannelGuide() {
        this.props.show();
        axios.get(vars.BASE_API_URL+'/smartTvGuide')
            .then((response) => {
                if (response.data.success){
                    this.props.smartTvData(response.data.data);
                }
                this.props.hide();
            })
            .catch((error) => {
                this.props.hide();
                console_log(error);
            });
    }

    isChannelFavorite(channelId) {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    channelFavorite(data, event) {
        event.preventDefault();
        let favoriteChannels = this.props.favorite.channels;
        let indexOf = favoriteChannels.findIndex((f) => {
            return f.channelId == data.channel.channelId;
        });
        let channelToBeUpdated = {
            channelId: data.channel.channelId,
            channelName: data.channel.channelName,
            channelLang: data.channel.channelLang,
            channelImage: data.channel.channelImage,
            channelLogo: data.channel.channelLogo,
            channelGrade: data.channel.channelGrade,
            channelStatus: data.channel.channelStatus,
            channelBaseline: data.channel.channelBaseline,
            isFavorite: true
        };

        if (indexOf == -1) {
            favoriteChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/channels", channelToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteChannel(favoriteChannels);
    }

    getCurrentProgram(channel) {
        if (Array.isArray(channel.channelSummary.programs.program)) {
            return channel.channelSummary.programs.program.filter((program) => {
                if (isBetween(this.state.currentTime, program.start, program.end)) {
                    return program;
                }
            })[0];
        } else {
            return channel.channelSummary.programs.program;
        }
    }

    getNextProgram(channel) {
        if (Array.isArray(channel.channelSummary.programs.program)) {
            let index = channel.channelSummary.programs.program.findIndex((program) => {
                if (isBetween(this.state.currentTime, program.start, program.end)) {
                    return program;
                }
            });
            if (index < channel.channelSummary.programs.program.length-1) {
                return channel.channelSummary.programs.program[index+1];
            } else {
                return channel.channelSummary.programs.program[index];
            }
        } else {
            return channel.channelSummary.programs.program;
        }
    }

    getNextPrograms(channel) {
        if (Array.isArray(channel.channelSummary.programs.program)) {
            let index = channel.channelSummary.programs.program.findIndex((program) => {
                if (isBetween(this.state.currentTime, program.start, program.end)) {
                    return program;
                }
            });
            if (index < channel.channelSummary.programs.program.length-1) {
                return channel.channelSummary.programs.program.slice(index+1);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    getProgress(start, end) {
        let x = formatDate(this.state.currentTime, "X") - formatDate(start, "X"),
            y = formatDate(end, "X") - formatDate(start, "X");
        return x/y*100;
    }



    render() {
        let currentChannel = this.props.smartTV.smartTv.find((channel) => {
            if (channel.channelId == this.state.channelId) {
                return channel;
            }
        });

        let currentProgram = this.getCurrentProgram(currentChannel),
            nextPrograms = this.getNextPrograms(currentChannel);

        return(
            <View style={[smartTVStyles.smartTVView]}>
                <Header
                    isDrawer={false}
                    isTitle={true}
                    title='Smart TV'
                    isSearch={false}
                    rightLabel=''
                />
                <Loader visible={this.props.loader.isLoading} />
                <View style={[smartTVStyles.contentView]}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <View style={smartTVStyles.imageView}>
                        <ImageBackground style={{ width: '100%', height: '100%', justifyContent: 'center' }} source={currentChannel.channelImage ? {uri: vars.BASE_URL+"uploads/"+currentChannel.channelImage} : thumbnail}>
                            <View style={smartTVStyles.bgOpacityTvGuide}>
                                <Image style={smartTVStyles.channelLogoTvGuide} source={{uri:currentChannel.channelLogo}}/>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.avBold, smartTVStyles.bannerText]}>
                                    CHANNEL GUIDE
                                </Text>
                                <View style = {{ marginTop: '26%', marginLeft: '20%' }}>
                                    <View style={smartTVStyles.tvFavoriteView}>
                                        <TouchableHighlight onPress={this.channelFavorite.bind(this, {channel: currentChannel})}>
                                            <Icon name='heart' size={15} style={{ backgroundColor: 'transparent' }} color={this.isChannelFavorite(this.state.channelId) ? "#f00b64" : "#fff"} />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    {currentProgram ?
                        <View style={[smartTVStyles.itemView,{ paddingTop: 10}]}>
                            <ImageBackground style={[smartTVStyles.imageBackground]}
                                             source={currentChannel.channelImage ? {uri: vars.BASE_URL+"uploads/"+currentChannel.channelImage} : thumbnail}>
                                <View style={ smartTVStyles.bgOpacity }></View>
                                <Image style={[smartTVStyles.channelLogo]}
                                       source={{uri:currentChannel.channelLogo}}/>
                            </ImageBackground>
                            <View style={[smartTVStyles.textData]}>
                                <View style={[smartTVStyles.firstLayer]}>
                                    <View style={[smartTVStyles.leftHalf]}>
                                        <Text
                                            style={[styles.avRegular, smartTVStyles.programName, { marginTop: 10, fontSize: 14}]}>{(currentProgram)? currentProgram.name : " "}</Text>
                                        <Text
                                            style={[styles.avRegular, smartTVStyles.nowStreaming]}>
                                            <FeatherIcon name="radio" size={12}
                                                         color="#ed145b"/>{"  "}Now Streaming
                                        </Text>
                                    </View>
                                </View>
                                <View style={[smartTVStyles.secondLayer]}>
                                    <View style={[smartTVStyles.progressBar,{width : this.getProgress(currentProgram.start, currentProgram.end)}]}></View>
                                </View>
                                <View style={[smartTVStyles.thirdLayer, {marginTop: 2}]}>
                                    <Text style={[styles.avRegular, smartTVStyles.startTime]}>{(currentProgram) ? formatDate(currentProgram.start, "HH:mm a"): " "}</Text>
                                    <Text style={[styles.avRegular, smartTVStyles.endTime]}>{(currentProgram) ? formatDate(currentProgram.end, "HH:mm a") : " "}</Text>
                                </View>
                            </View>
                        </View>
                        :
                        null}
                </View>
                    <ScrollView style={[smartTVStyles.contentView, {marginTop: '-15%'}]}>
                        {nextPrograms != null ?
                            nextPrograms.map((program, key) => {
                                return (
                                    <View index= {key} style={[smartTVStyles.itemView]}>
                                        <ImageBackground style={[smartTVStyles.imageBackground]}
                                                         source={typeof program.thumbnail !== "object" ? {uri: program.thumbnail} : thumbnail}>
                                            <View style={ smartTVStyles.bgOpacity }></View>
                                            <Image style={[smartTVStyles.channelLogo]}
                                                   source={{uri:program.channelLogo}}/>
                                        </ImageBackground>
                                        <View style={[smartTVStyles.textData]}>
                                            <View style={[smartTVStyles.thirdLayer]}>
                                                <Text
                                                    style={[styles.avRegular, { flex: 1,
                                                        color: "#ffffff",
                                                        fontSize: 14,
                                                        textAlign: 'left'}]}>{program.name}</Text>
                                                <Text style={[styles.avRegular, smartTVStyles.endTime]}>{formatDate(program.start, "HH:mm a")}</Text>
                                            </View>
                                            {(key == 0) ? <Text style={[styles.avRegular,{ color: 'red', fontSize: 11, marginLeft: '77%', marginTop: 5 }]}>Up Next</Text> : null}
                                        </View>
                                    </View>
                                )
                            })
                            :
                            null
                        }


                    <Footer />
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        smartTvData,
        addFavoriteChannel,
        showMessage
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SmartTVGuide);