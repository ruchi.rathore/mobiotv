import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, ImageBackground, ScrollView, FlatList, TouchableOpacity, Image } from "react-native";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Loader from '../components/Loader/Loader';
import axios from 'axios';
// Components
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";

// Styles
import { styles } from "../style/appStyles";
import smartTVStyles from "../style/smartTVStyles";
import { show, hide } from '../actions/ActivityIndicatorActions';
import { smartTvData } from '../actions/SmartTVActions';

// Other data/helper functions
import data from "../data/moviesData.json";
import NavigationService from "../utils/NavigationService";
import { console_log, isBetween, formatDate } from '../utils/helper';
import * as vars from '../constants/api';
import {thumbnail} from "../assets/Images";


class SmartTV extends Component {
    constructor(props) {
        super(props);
        this.state = {
            variable: true,
            currentTime: new Date()
        }

        this.getCurrentProgram = this.getCurrentProgram.bind(this);
        this.getNextProgram = this.getNextProgram.bind(this);
        this.getProgress = this.getProgress.bind(this);
    }

    componentDidMount() {
        this.getSmartTv();
        this.timeInterval = setInterval(() => {
            this.setState({
                currentTime: new Date()
            })
        }, 1000);
    }

    componentWillUnmount() {
        if (this.timeInterval != null) {
            clearInterval(this.timeInterval);
        }
    }

    viewAllClick(channelId) {
        NavigationService.navigate('SmartTVGuide', {channel: channelId});
    }

    getSmartTv() {
        this.props.show();
        axios.get(vars.BASE_API_URL+'/smartTvGuide')
            .then((response) => {
                ///console_log(response);
                if (response.data.success){
                    this.props.smartTvData(response.data.data);
                }
                this.props.hide();
            })
            .catch((error) => {
                this.props.hide();
                console_log(error);
            });
    }

    getCurrentProgram(channel) {
        //console_log(channel , channel.channelSummary);
        if (Array.isArray(channel.channelSummary.programs.program)) {
            return channel.channelSummary.programs.program.filter((program) => {
                if (isBetween(this.state.currentTime, program.start, program.end)) {
                    return program;
                }
            })[0];
        } else {
            return channel.channelSummary.programs.program;
        }
    }

    getNextProgram(channel) {
        if (Array.isArray(channel.channelSummary.programs.program)) {
            let index = channel.channelSummary.programs.program.findIndex((program) => {
                if (isBetween(this.state.currentTime, program.start, program.end)) {
                    return program;
                }
            });
            if (index < channel.channelSummary.programs.program.length-1) {
                return channel.channelSummary.programs.program[index+1];
            } else {
                return channel.channelSummary.programs.program[index];
            }
        } else {
            return channel.channelSummary.programs.program;
        }
    }

    getProgress(start, end) {
        let x = formatDate(this.state.currentTime, "X") - formatDate(start, "X"),
            y = formatDate(end, "X") - formatDate(start, "X");
        return x/y*100;
    }

    render() {
        return(
            <View style={[smartTVStyles.smartTVView]}>
                <Header
                    isDrawer={false}
                    isTitle={true}
                    title='Smart TV'
                    isSearch={false}
                    rightLabel=''
                />
                <Loader visible={this.props.loader.isLoading} />
                <ScrollView style={[smartTVStyles.contentView]}>
                    {this.props.smartTV.smartTv.length > 0 ?
                        this.props.smartTV.smartTv.map((channel, i) => {
                            if (channel) {
                                let currentProgram = this.getCurrentProgram(channel);
                                let nextProgram = this.getNextProgram(channel);
                                return (
                                                <View key={i} style={[smartTVStyles.itemView]}>
                                                    <ImageBackground style={[smartTVStyles.imageBackground]}
                                                                     source={channel.channelImage ? {uri: vars.BASE_URL+"uploads/"+channel.channelImage} : thumbnail}>
                                                        <View style={ smartTVStyles.bgOpacity }></View>
                                                        <Image style={[smartTVStyles.channelLogo]}
                                                               source={{uri:channel.channelLogo}}/>
                                                    </ImageBackground>
                                                    <View style={[smartTVStyles.textData]}>
                                                        <View style={[smartTVStyles.firstLayer]}>
                                                            <View style={[smartTVStyles.leftHalf]}>
                                                                <Text
                                                                    style={[styles.avRegular, smartTVStyles.nowStreaming]}>
                                                                    <FeatherIcon name="radio" size={12}
                                                                                 color="#ed145b"/>{"  "}Now Streaming
                                                                </Text>
                                                                <Text
                                                                    style={[styles.avRegular, smartTVStyles.programName]}>{(currentProgram)? currentProgram.name : " "}</Text>
                                                            </View>
                                                            <TouchableOpacity style={[smartTVStyles.rightHalf]} onPress={this.viewAllClick.bind(this, channel.channelId)}>
                                                                    <View onPress={this.viewAllClick.bind(this, channel.channelId)}>
                                                                    <Text style={[styles.avRegular, smartTVStyles.viewAll]}>
                                                                        View All
                                                                    </Text>
                                                                    </View>
                                                                <Text style={[styles.avRegular, smartTVStyles.upNext]}>Up Next</Text>
                                                                <Text
                                                                    style={[styles.avRegular, smartTVStyles.programNameNext]}>{(nextProgram) ? nextProgram.name : " "}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={[smartTVStyles.secondLayer]}>
                                                            <View style={[smartTVStyles.progressBar,{width : this.getProgress(currentProgram.start, currentProgram.end)}]}></View>
                                                        </View>
                                                        <View style={[smartTVStyles.thirdLayer]}>
                                                            <Text style={[styles.avRegular, smartTVStyles.startTime]}>{(currentProgram) ? formatDate(currentProgram.start, "HH:mm a"): " "}</Text>
                                                            <Text style={[styles.avRegular, smartTVStyles.endTime]}>{(currentProgram) ? formatDate(currentProgram.end, "HH:mm a") : " "}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                )
                                            }

                                        })
                        :

                        (!this.props.loader.isLoading) ?
                        <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                            <Text style={{color: '#ffffff', fontSize: 18}}>No Data Available</Text>
                        </View>
                            :
                            null
                    }

                    <Footer />
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        smartTvData
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SmartTV);