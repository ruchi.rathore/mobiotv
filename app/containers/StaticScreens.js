import React, { Component } from "react";
import { Image, View, Text, ImageBackground, ScrollView } from "react-native";
import { Container, H3, Button, Title, Body, Left, Right } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';

// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

import NavigationService from '../utils/NavigationService';

// Styles
import { styles } from "../style/appStyles";
import staticScreenStyles from "../style/staticScreenStyles";

// Other data/helper functions
import { logo, background } from "../assets/Images";
import {console_log} from "../utils/helper";

class StaticScreens extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screen : this.props.navigation.state.params.from
        }
    }

    renderPrivacyPolicy() {
        return (
            <View style={{width: '100%'}}>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    We attach great importance to the privacy of its users. To ensure this we have created a policy that is inextricably linked to the General Terms and Conditions of the Provider. You acknowledge that the Provider collects and processed the Registration Information and information on your use of the Service including the personally identifiable information contained therein for the purpose detailed in the Privacy Policy. As a global company, the Provider increasingly goes beyond the borders of one country.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    The Service is supplied as is (at the stage of development it happens to be in) and the Provider accepts no liability nor does it issue any guarantees or warranty in the event that personal configurations, information or messages are not saved, or not saved in due time, or if they are deleted or incorrectly delivered. In order to subscribe to the Service, you are required to have access to the Internet and/or a mobile communications subscription and pay any service fees associated with such access. You are required to pay all expenses incurred in creating such access. You are also responsible for seeing to the connection of any necessary equipment, including any mobile telephone or personal computer or whatever devices that may be necessary needed and are in working order and suitable for use in connection with the Service.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    GENERAL RULES.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    Regarding the use of the Service you agree to the general rules, rules of play, promotional conditions and restrictions established by the Provider. The Provider reserves the right at all times and at its own discretion, without being required to issue prior notice, to amend these general rules, rules of play, promotional conditions and restrictions. The Provider is entitled to annul accounts that have remained inactive for considerable periods of time. You agree with the arrangement whereby the Provider bears no responsibility or liability to you in the event that information sent or provided by means of the Service is not saved or is deleted in error.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    GENERAL INFORMATION.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    These General Terms and Conditions take the place of all previous agreements between you and the Provider. If you use any additional service, material or software from third parties, additional General Terms and Conditions may be applicable to you. European law is applicable to the relationship between you and the Provider, regardless of any provisions of applicable international law. Disputes will be submitted to your national competent court. Any failure by the Provider to exercise or invoke certain rights or stipulations laid down in the General Terms and Conditions in no way constitutes a waiver or renunciation of those rights or stipulations. If a court of competent jurisdiction is of the opinion that any stipulation in the General Terms and Conditions is not applicable, the parties nevertheless agree that the court must attempt to satisfy the wishes of the parties reflected by that stipulation, while the other stipulations of the General Terms and Conditions remain unimpaired.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    SECURITY AND PROTECTION.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    The infrastructure and programmes are constructed and protected in such a way that it can be reasonably assumed that unauthorized persons cannot gain access to any information about users. Please refer to our General Terms and Conditions regarding liability of the Provider.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    Acceptance of this policy means the Provider:
                </Text>
                <View style={staticScreenStyles.pointerView}>
                    <Icon name={'circle'} size={5} color={'#ffffff'} style={staticScreenStyles.pointerIcon}/>
                    <Text style={[styles.avRegular, staticScreenStyles.textDataPointers]}>
                        may use the information or a combination of it to execute the agreement between you and Provider, and to allow you the best possible use of the Provider's website;
                    </Text>
                </View>
                <View style={staticScreenStyles.pointerView}>
                    <Icon name={'circle'} size={5} color={'#ffffff'} style={staticScreenStyles.pointerIcon}/>
                    <Text style={[styles.avRegular, staticScreenStyles.textDataPointers]}>
                        may re-use the information communicated by you on the site or via SMS;
                    </Text>
                </View>
                <View style={staticScreenStyles.pointerView}>
                    <Icon name={'circle'} size={5} color={'#ffffff'} style={staticScreenStyles.pointerIcon}/>
                    <Text style={[styles.avRegular, staticScreenStyles.textDataPointers]}>
                        can inform you about offers, discounts, new developments and additional services offered by the Provider;
                    </Text>
                </View>
                <View style={staticScreenStyles.pointerView}>
                    <Icon name={'circle'} size={5} color={'#ffffff'} style={staticScreenStyles.pointerIcon}/>
                    <Text style={[styles.avRegular, staticScreenStyles.textDataPointers]}>
                        may process the information in connection with the formation or maintenance of a direct relationship between the Provider and/or enterprises selected by it and you for the purpose of recruitment for commercial or charity. The Provider may anonymize that information and use it or offer it to third parties in anonymous form.
                    </Text>
                </View>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    Your acceptance of this policy.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    By using the Service of the Provider you agree with this Privacy Policy and the General Terms and Conditions. If you disagree with this, do not make use of our Service. We reserve the right at any moment to change, modify, extend or limit this Privacy Policy and the General Terms and Conditions. We therefore advise you to read these pages regularly.
                </Text>
            </View>
        )
    }

    renderTermsAndConditions() {
        return (
            <View style={{width: '100%'}}>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    GENERAL TERMS AND CONDITIONS AND PRIVACY POLICY.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    The following General Terms and Conditions are applicable to the products and services supplied via WAP, SMS, GPRS and 3G by Sam Media Ltd., or enterprises affiliated to it, (hereinafter to be referred to as “the Provider”). The Provider is permitted to amend these General Terms and Conditions from time to time without notice. Apart from the General Terms and Conditions, any applicable and publicized rules, promotional conditions, guidelines and provisions pertaining to the services also apply to you and the Provider upon the use of the Services (as defined herein) of the Provider. All those guidelines, rules, promotional conditions and provisions will be considered an inextricable part of these General Terms and Conditions. By availing yourself to use the Services of the Provider, you agree to be bound by these General Terms and Conditions and the guidelines, rules and promotional conditions.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    DESCRIPTION OF THE PROVIDER.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    By means of WAP, SMS, GPRS and 3G services, the Provider supplies its user’s access to a network of on-line sources, including on-line games, text messages, rich content and on-line information material which may be downloaded onto mobile telephone or whatsoever devices (“Services”). The General Terms and Conditions are at all times applicable to all new facilities including facilities which the Provider may implement as part of the Provider’s expansion, extension or improvement of the present Services, including any extension to new functions added by the Provider.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    AVAILABILITY.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    The Service is supplied as is (at the stage of development it happens to be in) and the Provider accepts no liability nor does it issue any guarantees or warranty in the event that personal configurations, information or messages are not saved, or not saved in due time, or if they are deleted or incorrectly delivered. In order to subscribe to the Service, you are required to have access to the Internet and/or a mobile communications subscription and pay any service fees associated with such access. You are required to pay all expenses incurred in creating such access. You are also responsible for seeing to the connection of any necessary equipment, including any mobile telephone or personal computer or whatever devices that may be necessary needed and are in working order and suitable for use in connection with the Service.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    PAYMENT.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    You have access to our on-line services free of charge. With regards to the Services and the use of it, you shall pay the amount corresponding to the applicable rates of the Provider in force at that time and according to the rates charged by your mobile operator. The charges shall be invoiced to you via your mobile operator through the mobile bill you receive from the mobile operator of your network. You pay the Provider all national and local or other taxes, including but not restricted to VAT, any taxes or levies imposed in lieu thereof, which taxes are based on the costs due to the use of the Services, regardless of whether those taxes are levied either now or in the future by European, national or local authorities or by any other body or bodies authorized to levy taxes. The Provider reserves the right to change the rates for the Service by posting details of such changes on its website.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    REGISTRATION OBLIGATION.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    In order to warrant safe and secure use of the Service, you guarantee the following, in addition to paying the applicable charges: (a) your personal information (hereinafter to be referred to as the “Registered Information”) provided on the registration form is correct, true, accurate and complete and (b) you agree to ensure that the Registered Information is at all times correct, up to date, true, accurate and complete. If the Provider has a reasonable suspicion that the Registered Information does not reflect a true state of affairs, or is not accurate or complete, then the Provider is entitled to suspend or terminate your account and to withhold both current and future use of the Service, or any component of it. You yourself are responsible for maintaining the secrecy of any passwords and/or accounts issued to you by the Provider, and you are fully liable for all actions carried out involving the use of your password or account. You undertake at the end of each session to close your account (by closing your browser) and to contact the Provider if you notice or suspect that unauthorized use has been made of your password or account, or that security or protection is no longer guaranteed for any other reason. The Provider cannot and will not be liable for any damage arising from your failure to observe the stipulations of this article.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textDataHeader]}>
                    OWNERSHIP RIGHTS OF THE PROVIDER.
                </Text>
                <Text style={[styles.avRegular, staticScreenStyles.textData]}>
                    You are aware and agree that the Service and software used in connection with the Service, (hereinafter to be referred to as the “Software”), contain confidential information which belongs to the Provider is protected by valid and applicable intellectual and industrial ownership rights legislation and other legislation. You are also aware and agree that copyright, trademarks, service marks, patents or other ownership rights and laws are applicable to Information published in sponsors' advertisements or information offered to you via the Service. You undertake, either partially or wholly, not to amend, rent, rent out, borrow, lend, sell, distribute or create products derived from the Service or the Software except in the event that the Provider has given you explicit written permission to do so. The Provider hereby grants you a personal, non-transferable and non-exclusive right and permission to use the working code of its Software, provided you do not copy or alter any source code whatever, or create a product derived from it, or carry out any reverse engineering or reverse assembly on it or in any other way attempt to find a source code (or permit third parties to do so) (except if and insofar as such acts are permitted by the provisions of any mandatory law that may be in effect), and you agree not sell, encode, sublicense, encumber with security rights or transfer in any other way any rights connected with the Software to any person or entity. You undertake not to alter the Software in any way whatever or to use any altered versions of the Software for the purpose of gaining unauthorized access to the Service or for any other reason. In gaining access to the Service, you undertake only to make use of the interface supplied for that purpose by the Provider. The Provider hereby grants you permission to make one copy only of the Information on the equipment you use for gaining access to the Service, and to use and display the copy of the Information made on that equipment for private purposes.
                </Text>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <Header
                    isDrawer={false}
                    isTitle={true}
                    title={this.props.navigation.state.params.from}
                    isSearch={false}
                    rightLabel=''
                />

                <ImageBackground source={background} style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{paddingTop: 40}}>
                        {this.state.screen === "Sobre nosotros" ?
                            <View style={[staticScreenStyles.staticView]}>
                                <Image source={logo} style={[staticScreenStyles.staticLogo]}/>
                                <Text style={[styles.avRegular, staticScreenStyles.textData]}>Our Story is about Over-The-Top Content (OTT). MobioTV is a TV Channel and Video-On-Demand (VOD) Streaming site provides the most incredible movies, videos and TV channels worldwide to any connected device. Select your TV and VOD to enjoy our international, local and thematic TV channels and On Demand Videos that enrich every subscriber’s need</Text>
                            </View>
                            : <View style={staticScreenStyles.staticView}>
                                {this.state.screen === "Política de privacidad" ? this.renderPrivacyPolicy() : this.renderTermsAndConditions()}
                            </View>
                        }

                        <Footer from={this.props.navigation.state.params.from} />
                    </ScrollView>
                </ImageBackground>
            </Container>
        );
    }
}

export default StaticScreens;
