import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Video from 'react-native-af-video-player';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Orientation from 'react-native-orientation';
// Components
import Header from '../components/Header/PlayHeader';
import Footer from '../components/Footer/Footer';
import RelatedVideos from '../components/RelatedVideos/RelatedVideos';

// Styles
import { styles } from "../style/appStyles";
import PlayStyle from "../style/playStyle";

// Other data/helper functions
import data from '../data/moviesData.json';
import { playBackground, movie_1, tv_4, tv_3 } from "../assets/Images";
import NavigationService from "../utils/NavigationService";
import * as vars from '../constants/api';
import relatedVideosStyles from '../style/relatedVideosStyles';
import { messages } from '../constants/messages';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addLikesVideos, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { addChannelHistory } from '../actions/HistoryActions';
import { console_log } from '../utils/helper';
import Loader from '../components/Loader/Loader';
import {movies} from '../constants/movies';
import Search from '../components/Search/Search';
import MessageBar from '../components/Message/Message';
let movies1 = movies;

class PlayVOD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            favoriteSwitch: false,
            isPlaying: false,
            playUrl: '',
            videoData:props.play.data.video,
            tempVideoData: props.play.data.video.subCategory,
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
            curentCategory:[]
        };
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _onPressButton(data) {
        if(this.state.curentCategory.name=='Arabic Entertainment') {
            let tempArray = this.state.tempVideoData;
            tempArray[data.index]= this.state.data.video;
            this.setState({
                data: Object.assign({video: data.video}),
                tempVideoData:tempArray,
                //curentCategory:
            })
            console_log(this.state.curentCategory, 'tempVideoData', tempArray)
        }
        this.Scrollview.scrollTo({x: 0,y:0, animated: true});
        this.props.getVideoOrChannelRelatedData(data);
        this.setState({isPlaying: false});
    }

    componentWillMount() {
        if (this.props.play.data.video) {
            this.getVideoCategories(this.props.play.data.video.id);
        }
    }

    componentDidMount() {
        //Orientation.lockToLandscape();
        this.props.show();
        setTimeout(() => {
            this.props.hide();
            this.setState({dataLoad: true});
        }, 1500);
    }

    componentWillReceiveProps(nextProps) {

        if (this.props.play.data.video.id != nextProps.play.data.video.id) {
            this.getVideoCategories(nextProps.play.data.video.id);
        }
    }

    isArabic = (text) =>{
        var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
        result = pattern.test(text);
        return result;
    }

    getVideoCategories(videoId) {
        // console_log('videoId', videoId)
        let videoCategory = movies.find((category) => {
            let videoIds = category.videos.map(v => v.id);
            if (~videoIds.indexOf(videoId)) {
                return category;
            }
        });
        if(videoCategory) {
            let video = videoCategory.videos.find((v) => v.id == videoId);
            this.setState({
                data: Object.assign({video: video}, {category: videoCategory}),
                curentCategory:videoCategory
            });
        }
    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    addRemoveFavorite(video) {
        this.videoFavorite(video);
    }


    videoFavorite(video) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == video.id;
        });
        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            this.props.showMessage({
                message: messages.addToFavorites,
                type: true
            });
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            this.props.showMessage({
                message: messages.removeFromFavorites,
                type: false
            });
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    // console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    playVideo(video) {
        if(this.state.curentCategory.name=='Arabic Entertainment'){
            this.setState({
                isPlaying: !this.state.isPlaying
            });
            video.video.videoUrl !== undefined ? this.props.navigation.navigate('Player', {url: video.video.videoUrl}) : null;
        }
    }

    addLike(video) {
        this.videoLike(video);
    }

    videoLike(video) {
        let likedVideos = this.props.favorite.videosLiked;
        let indexOf = likedVideos.findIndex((f) => {
            return f.videoId == video.id;
        });

        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            likedVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/likes/videos", videoToBeUpdated)
                .then((response) => {
                    // console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        } else {
            likedVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/likes/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addLikesVideos(likedVideos);
    }

    isVideoLiked(videoId) {
        let indexOf = this.props.favorite.videosLiked.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _handleFavoriteClicked() {

    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    _renderPlayContent(video) {
        //alert('here');
        //console_log(video.video.videoUrl)
        return (
            /*((this.state.isPlaying && video.video.videoUrl !== undefined )) ?
                <View>
                    <Video
                        url={ video.video.videoUrl }
                        autoPlay={true}
                        rotateToFullScreen={true}
                    />
                </View>
                :*/
            <View>
                <View style={PlayStyle.imageContainer}>
                    <Image style={{ height: 220, width: 150 }} source={video.video.preview} />
                </View>
                <View style={{ paddingTop: 40 }}>
                    <View style={PlayStyle.playButtonView}>
                        <TouchableHighlight onPress={this.playVideo.bind(this, video)} underlayColor="transparent" activeOpacity={0.6}>
                            <View style={PlayStyle.playButton}>
                                <Icon name="play" size={18} style={{ backgroundColor: 'transparent', paddingRight: 10 }} color="#fff" />
                                <Text style={[PlayStyle.buttonText, styles.avRegular]}>
                                    PLAY
                                </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>

        )
    }

    LoadRelatedVOD = (video) =>{
        console_log('inside LoadRelatedVOD ')
        return(
            <View>
                {this.state.curentCategory.name=="Arabic Entertainment" && this.state.tempVideoData!=="" ?
                    <View>
                        {this.state.tempVideoData.map((item, i) => {
                            return (
                                <TouchableOpacity style={[relatedVideosStyles.relatedItemView]} key={i} onPress={this._onPressButton.bind(this, {video: item, index: i})}>
                                    <View style={[relatedVideosStyles.imageWrapper]}>
                                        <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item, index: i})}>
                                            <ImageBackground style={relatedVideosStyles.imageBackground} source={item.preview}></ImageBackground>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                        <Text style={[relatedVideosStyles.relatedItemTitle]}>{item.name}</Text>
                                        <View style={[relatedVideosStyles.titleIcon]}>
                                            <MaterialIcons name='play-circle-outline' size={20} style={[relatedVideosStyles.relatedItemIcon]} color='white' />
                                            <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{item.duration}</Text>
                                        </View>
                                    </View>
                                    <TouchableHighlight style={[relatedVideosStyles.relatedItemIconWrapper]} onPress={this.addRemoveFavorite.bind(this, item)}>
                                        <Icon name='heart' size={16} style={[relatedVideosStyles.relatedItemIcon]} color={this.isVideoFavorite(item.id) ? "#f00b64" : "#fff"} />
                                    </TouchableHighlight>
                                </TouchableOpacity>
                            )
                        })
                        }
                    </View>
                    :
                    <View>
                        {video.category.videos.map((item, index) => {
                            if (item.id != video.video.id) {
                                return (
                                    <TouchableOpacity style={[relatedVideosStyles.relatedItemView]} key={index} onPress={this._onPressButton.bind(this, {video: item})}>
                                        <View style={[relatedVideosStyles.imageWrapper]}>
                                            <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})}>
                                                <ImageBackground style={relatedVideosStyles.imageBackground} source={item.preview}>
                                                    <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                            <Text style={[relatedVideosStyles.relatedItemTitle]}>{item.name}</Text>
                                            <View style={[relatedVideosStyles.titleIcon]}>
                                                <MaterialIcons name='play-circle-outline' size={20} style={[relatedVideosStyles.relatedItemIcon]} color='white' />
                                                <Text style={[relatedVideosStyles.relatedItemSubTitle]}>{item.duration}</Text>
                                            </View>
                                        </View>
                                        <TouchableHighlight style={[relatedVideosStyles.relatedItemIconWrapper]} onPress={this.addRemoveFavorite.bind(this, item)}>
                                            <Icon name='heart' size={16} style={[relatedVideosStyles.relatedItemIcon]} color={this.isVideoFavorite(item.id) ? "#f00b64" : "#fff"} />
                                        </TouchableHighlight>
                                    </TouchableOpacity>
                                )
                            }
                        })
                        }
                    </View>
                }
            </View>
        )
    }

    render() {
        let video = this.state.data;
        console_log('- done:', this.state.curentCategory)
        return (
            <Container>
                <ImageBackground source={playBackground} style={{ zIndex: 999 }}>
                    <Header
                        isDrawer={false}
                        isTitle={true}
                        title={video.video.name}
                        isSearch={true}
                        rightLabel=''
                    />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
                <Search from={"videos"}/>
                <View style={PlayStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView ref={component => this.Scrollview = component}>
                        <ImageBackground source={playBackground} style={{ flex: 1, zIndex: 999 }}>
                            <View>
                                {this._renderPlayContent(video)}
                            </View>
                        </ImageBackground>
                        <View style={{ paddingRight: '4%', paddingTop: '4%' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '4%' }} >
                                <View style={{ flex: 6 }}>
                                    <Text numberOfLines={5} style={[styles.avBold, PlayStyle.videoTitle]}>{video.video.name}</Text>
                                </View>
                                <View style={{ alignItems: 'center', flex: 2 }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addRemoveFavorite.bind(this, video.video)}>
                                        <Icon name='heart' size={20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(video.video.id) ? "#f00b64" : "#fff"} />
                                        <Text style={PlayStyle.watchLaterText}>Watch Later</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={PlayStyle.rateView}>
                                    <TouchableOpacity onPress={this.addLike.bind(this, video.video)}>
                                        <Icon name='thumbs-up' size={22} style={{ backgroundColor: 'transparent' }} color = {this.isVideoLiked(video.video.id) ? "#699bff" : "#fff"} />
                                        <Text style={PlayStyle.rateText}>Rate</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={[PlayStyle.descriptionView, {width: '100%'}]}>
                                <View style={PlayStyle.durationView}>
                                    <Text style={PlayStyle.durationText}>PG-13</Text>
                                    <Text style={PlayStyle.durationText}>2h 6min</Text>
                                    <Text style={PlayStyle.durationText}>15 August 2014 (USA)‎</Text>
                                </View>
                                <View>
                                    <Text style={[PlayStyle.durationText, { color: '#2770ba' }]}>{(video.category) ? video.category.name : ""}</Text>
                                </View>
                            </View>
                            <View style = {{marginTop: 12, marginBottom : 12, paddingLeft: '4%', paddingRight: '4%' }}>
                                <Text style = {[styles.avRegular,{color : '#ffffff', fontSize: 12, textAlign: this.isArabic(video.video.desc) ? 'right' : 'left'}]}>{(video.video.desc)? video.video.desc : ""}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', paddingBottom: '4%', paddingLeft: '4%' }}>
                            <View style={PlayStyle.videoInfoView}>
                                <Text style={[styles.avRegular, PlayStyle.infoTitle]}>Director:</Text>
                                <Text style={[styles.avRegular, PlayStyle.infoContent]}>NA</Text>
                            </View>
                            <View style={PlayStyle.videoInfoView}>
                                <Text style={[styles.avRegular, PlayStyle.infoTitle]}>Writers:</Text>
                                <Text style={[styles.avRegular, PlayStyle.infoContent]}>NA</Text>
                            </View>
                        </View>
                        {video.category?
                            <View style={[relatedVideosStyles.wrapperView]}>
                                <View style={[relatedVideosStyles.textAndSwitchView]}>
                                    <View>
                                        <Text style={[styles.avRegular, relatedVideosStyles.headerText]}>
                                            {"Related Videos"}
                                        </Text>
                                    </View>
                                    <View style={[relatedVideosStyles.switchWrapper]}>
                                        <Text style={[styles.avRegular, relatedVideosStyles.subHeaderText]}>
                                            Autoplay
                                        </Text>
                                        <Switch style={[relatedVideosStyles.switch]} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                                    </View>
                                </View>
                                {this.state.curentCategory? this.LoadRelatedVOD(video) : null}
                            </View>
                            : null}
                        <View style={{ paddingBottom: 20 }}>
                            <Footer />
                        </View>
                    </ScrollView >
                </View>
            </Container >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
        addChannelHistory,
        addLikesVideos
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayVOD);
