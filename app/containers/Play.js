import React, { Component } from "react";
import { Modal, Dimensions, Image, View, WebView, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Video from 'react-native-af-video-player';
import Orientation from 'react-native-orientation';
// Components
import Header from '../components/Header/PlayHeader';
import Footer from '../components/Footer/Footer';

// Styles
import { styles } from "../style/appStyles";
import PlayStyle from "../style/playStyle";
import favoriteStyles from "../style/favoriteStyle";

// Other data/helper functions
import { playBackground, movie_1, tv_4, tv_3, thumbnail } from "../assets/Images";
import * as vars from '../constants/api';
import { messages } from '../constants/messages';
import relatedVideosStyles from '../style/relatedVideosStyles';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addLikesChannels } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { addChannelHistory } from '../actions/HistoryActions';
import { console_log } from '../utils/helper';
import Loader from '../components/Loader/Loader';
import Search from '../components/Search/Search';
import MessageBar from '../components/Message/Message';
import NavigationService from "../utils/NavigationService";
var DeviceInfo =  require('react-native-device-info');

class Play extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            favoriteSwitch: false,
            play: false,
            playUrl: '',
            isPlaying: false,
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
        };
    }

    playVideo() {
        this.setState({ play: true });
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _onPressButton(data) {
        //NavigationService.navigate('Play');
        this.setState({isPlaying: false, playUrl: ''})
        this.Scrollview.scrollTo({x: 0,y:0, animated: true});
        this.props.getVideoOrChannelRelatedData(data);
    }

    _OpenSmartTvClicked() {
        NavigationService.navigate('SmartTV');
    }

    componentDidMount() {
        this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);
        if (this.props.play.data.channel) {
            this.getChannelCategories(this.props.play.data.channel.channelId);
        }
        this.getChannel(this.props.play.data.channel.channelId);
        this.setState({dataLoad: true});
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.play.data.channel.channelId != nextProps.play.data.channel.channelId) {
            this.getChannel(nextProps.play.data.channel.channelId);
            this.getChannelCategories(nextProps.play.data.channel.channelId);
        }
    }

    getChannelCategories(channelId) {
        this.setState({
            data: this.props.category.channels.find((channel) => channel.channelId == channelId)
        });
    }

    isChannelFavorite(channelId) {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    isChannelVideoFavorite(channelId) {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    addRemoveFavorite(channel) {
        this.channelFavorite(channel);
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    channelFavorite(channel) {
        let favoriteChannels = this.props.favorite.channels;
        let indexOf = favoriteChannels.findIndex((f) => {
            return f.channelId == channel.channelId;
        });

        let channelToBeUpdated = {
            channelId: channel.channelId,
            channelName: channel.channelName,
            channelLang: channel.channelLang,
            channelImage: channel.channelImage,
            channelLogo: channel.channelLogo,
            channelGrade: channel.channelGrade,
            channelStatus: channel.channelStatus,
            channelBaseline: channel.channelBaseline,
            isFavorite: true
        };

        if (indexOf == -1) {
            favoriteChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/channels", channelToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteChannel(favoriteChannels);
    }

    playChannel() {
        if(this.state.playUrl){
            this.setState({
                isPlaying: !this.state.isPlaying
            });
            this.props.navigation.navigate('Player', {url: this.state.playUrl});
            this.addHistoryChannel(this.state.data);
        }
    }

    getChannel(channelId) {
        //console_log('channelId:', channelId);
        //console_log(vars.BASE_API_URL+"/getTVStreamingURL/"+channelId+"/WIFI")
        this.props.show();
        const userAgent = DeviceInfo.getUserAgent();
        //console_log('userAgent:', userAgent)
        //axios.headers.post['userAgent'] = userAgent;
        axios.get(vars.BASE_API_URL+"/getTVStreamingURL/"+channelId+"/WIFI",{ headers: { 'User-Agent': userAgent }})
            .then((response) => {
                this.setState({
                    playUrl: response.data.data.url
                });
                this.props.hide();
               // console_log(response);
            })
            .catch((error) => {
                this.props.hide();
                console_log(error);
            });
    }

    addHistoryChannel(channel) {
        let historyChannels = this.props.historyVideos.channels;
        let indexOf = historyChannels.findIndex((h) => {
            return h.channelId == channel.channelId;
        });

        let channelToBeUpdated = {
            channelId: channel.channelId,
            channelName: channel.channelName,
            channelLang: channel.channelLang,
            channelImage: channel.channelImage,
            channelLogo: channel.channelLogo,
            channelGrade: channel.channelGrade,
            channelStatus: channel.channelStatus,
            channelBaseline: channel.channelBaseline,
            isFavorite: true
        };

        if (indexOf == -1) {
            historyChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/history/channels", channelToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addChannelHistory(historyChannels);
    }

    addLike(channel) {
        this.channelLike(channel);
    }

    channelLike(channel) {
        let likedChannels = this.props.favorite.channelsLiked;
        let indexOf = likedChannels.findIndex((f) => {
            return f.channelId == channel.channelId;
        });

        let channelToBeUpdated = {
            channelId: channel.channelId,
            channelName: channel.channelName,
            channelLang: channel.channelLang,
            channelImage: channel.channelImage,
            channelLogo: channel.channelLogo,
            channelGrade: channel.channelGrade,
            channelStatus: channel.channelStatus,
            channelBaseline: channel.channelBaseline
        };

        if (indexOf == -1) {
            likedChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/likes/channels", channelToBeUpdated)
                .then((response) => {
                   // console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        } else {
            likedChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/likes/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addLikesChannels(likedChannels);
    }

    isChannelLiked(channelId) {
        let indexOf = this.props.favorite.channelsLiked.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    onFullScreen(status){
        //alert(status);
    }

    _renderPlayContent(channel) {
        //console.log('***this.state.playUrl**', this.state.playUrl);
       //console.log('this.state.isPlaying:', this.state.isPlaying)
        return (
            /*(this.state.isPlaying && this.state.playUrl !== '') ?
                <View>
                    <Video
                        url={ this.state.playUrl }
                        ref={(ref) => { this.video = ref }}
                        autoPlay={true}
                        rotateToFullScreen={true}
                        onFullScreen={(status)=>{this.onFullScreen(status)}}
                    />
                </View>
                :*/
                <View>
                    <View style={PlayStyle.shadow}>
                        <Image style={{ height: 155, width: 250 }} source={{uri:channel.channelImage ? vars.BASE_URL+"uploads/"+channel.channelImage : thumbnail}} />
                    </View>
                    <View style={{ paddingTop: 40 }}>
                        <View style={PlayStyle.playButtonView}>
                            <TouchableHighlight onPress={this.playChannel.bind(this)} underlayColor="transparent" activeOpacity={0.6}>
                                <View style={PlayStyle.playButton}>
                                    <Icon name="play" size={18} style={{ backgroundColor: 'transparent', paddingRight: 10 }} color="#fff" />
                                    <Text style={[PlayStyle.buttonText, styles.avRegular]}>
                                        PLAY
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
        )
    }

    renderLoadVOD = (channel) =>{
        return(
            <View>
                {channel ?
                    <View>
                        <ImageBackground source={playBackground} style={{ flex: 1, zIndex: 999 }}>
                            <View>
                                {this._renderPlayContent(channel)}
                            </View>
                        </ImageBackground>
                        <View style={{ paddingRight: '4%', paddingTop: '4%' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '4%' }} >
                                <View style={{ flex: 6 }}>
                                    <Text numberOfLines={5} style={[styles.avBold, PlayStyle.videoTitle]}>{channel.channelName}</Text>
                                </View>
                                <View style={{ alignItems: 'center', flex: 2 }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this._OpenSmartTvClicked.bind(this)}>
                                        <Icon name="list-alt" size={20} style={{ backgroundColor: 'transparent' }} color='white' />
                                        <Text style={PlayStyle.watchLaterText}>Smart TV</Text>
                                    </TouchableOpacity>

                                </View>
                                <View style={{ alignItems: 'center', flex: 2 }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addRemoveFavorite.bind(this, channel)}>
                                        <Icon name='star' size={20} style={{ backgroundColor: 'transparent' }} color={this.isChannelFavorite(channel.channelId) ? "#FFC107" : "#fff"} />
                                        <Text style={PlayStyle.watchLaterText}>Watch Later</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={PlayStyle.rateView}>
                                    <TouchableOpacity onPress={this.addLike.bind(this, channel)}>
                                        <Icon name='thumbs-up' size={22} style={{ backgroundColor: 'transparent'}} color = {this.isChannelLiked(channel.channelId) ? "#699bff" : "#fff"} />
                                        <Text style={PlayStyle.rateText}>Rate</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{width: '40%', height: 20}}>
                                    <Image style={{ width: '40%', height: '40%', resizeMode: 'contain', marginLeft: 10 }} source={{uri:channel.channelLogo}} />
                                </View>
                                <View style={PlayStyle.descriptionView}>
                                    <View style={PlayStyle.durationView}>
                                        <Text style={PlayStyle.durationText}>{channel.channelGrade +' | ' }</Text>
                                        <Text style={PlayStyle.durationText}>{channel.channelLang.toUpperCase() +' | '}</Text>
                                        <Icon name='rss' size={13} style={[PlayStyle.durationText, {marginTop: 2}]} />
                                        <Text style={PlayStyle.durationText}>Live Now</Text>
                                    </View>
                                    <View>
                                        <Text style={[PlayStyle.durationText, { color: '#2770ba' }]}>
                                            {channel.channelCategories?
                                                channel.channelCategories.map((category) => {
                                                    return category.category.name
                                                }).join(", ")
                                                : ''
                                            }</Text>
                                    </View>
                                </View>
                            </View>
                            <View style = {{marginTop: 15, marginBottom : 12, paddingLeft: '4%', paddingRight: '4%' }}>
                                <Text style = {[styles.avRegular,{color : '#ffffff', fontSize: 12}]}>{channel.channelBaseline}</Text>
                            </View>
                        </View>
                    </View>
                    :
                    null}
                <View style={[relatedVideosStyles.wrapperView]}>
                    <View style={[relatedVideosStyles.textAndSwitchView]}>
                        <View>
                            <Text style={[styles.avRegular, relatedVideosStyles.headerText]}>
                                {"Related Channels"}
                            </Text>
                        </View>
                        <View style={[relatedVideosStyles.switchWrapper]}>
                            <Text style={[styles.avRegular, relatedVideosStyles.subHeaderText]}>
                                Autoplay
                            </Text>
                            <Switch style={[relatedVideosStyles.switch]} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                        </View>
                    </View>
                    {
                        this.props.category.channels.map((item, index) => {
                            return (
                                <TouchableOpacity style={[relatedVideosStyles.relatedItemView]} key={index} onPress={this._onPressButton.bind(this, {channel: item})}>
                                    <TouchableOpacity style={[relatedVideosStyles.imageWrapper]} onPress={this._onPressButton.bind(this, {channel: item})}>
                                        <ImageBackground style={relatedVideosStyles.imageBackgroundVideo} source={{uri:item.channelImage ? vars.BASE_URL+"uploads/"+item.channelImage : thumbnail}}>
                                            <View style={[favoriteStyles.favoriteItemImageInnerView]}>
                                                <View style={[favoriteStyles.favouriteItemLogoWrapperView, {paddingTop: 15}]}>
                                                    <Image style={[favoriteStyles.favoriteItemChannelLogo]} source={{uri:item.channelLogo}} />
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                    <View style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                        <Text style={[relatedVideosStyles.relatedItemTitle]}>{item.channelName}</Text>
                                        <View style={[relatedVideosStyles.titleIcon]}>
                                            <FeatherIcon name="radio" size={20} style={[relatedVideosStyles.relatedItemIcon, {color: 'red'}]} color='white' />
                                            <Text style={[relatedVideosStyles.relatedItemSubTitle]} numberOfLines={1}>{item.channelBaseline}</Text>
                                        </View>
                                    </View>
                                    <TouchableHighlight style={[relatedVideosStyles.relatedItemIconWrapper]} onPress={this.channelFavorite.bind(this, item)}>
                                        <Icon name='star' size={16} style={[relatedVideosStyles.relatedItemIcon]} color={this.isChannelVideoFavorite(item.channelId) ? "#FFC107" : "#fff"} />
                                    </TouchableHighlight>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </View>
        )
    }


    render() {
        let channel = this.state.data;
        return (
            <Container>
                <ImageBackground source={playBackground} style={{ zIndex: 999 }}>
                    <Header
                        isDrawer={false}
                        isTitle={true}
                        title={channel.channelName}
                        isSearch={true}
                        rightLabel=''
                    />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
                <Search from={"channels"}/>
                <View style={PlayStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView ref={component => this.Scrollview = component}>
                        {this.state.dataLoad ? this.renderLoadVOD(channel) : null}
                        <View style={{ paddingBottom: 20 }}>
                            <Footer />
                        </View>
                    </ScrollView >
                </View>
            </Container>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        showMessage,
        addChannelHistory,
        addLikesChannels
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Play);
