import React, { Component } from "react";
import { Image, View, StatusBar, Linking, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, FlatList, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import VODCategory from '../components/VODCategory/VODCategory';
import Slider from '../components/Slider/Slider';
import Loader from '../components/Loader/Loader';
// Styles
import { styles } from "../style/appStyles";
import liveChannelStyle from "../style/liveChannelStyle";

// Other data/helper functions
import data from '../data/moviesData.json';
import { tv_1, tv_2, thumbnail } from "../assets/Images";
import NavigationService from "../utils/NavigationService";
import { console_log } from '../utils/helper';
import * as vars from '../constants/api';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { setHeaderTitle } from '../actions/HeaderActions';
import { channelLogo } from "../assets/Images";
import Search from '../components/Search/Search';
import { messages } from '../constants/messages';
import MessageBar from '../components/Message/Message';
import Orientation from 'react-native-orientation';

class LiveChannels extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data.moviesData,
            favoriteSwitch: false,
            loading: true,
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
        };

        this.switchFavorite = this.switchFavorite.bind(this);

    }

    _handleFavoriteClicked(data) {
        //console.log(JSON.stringify(data));
        this.channelFavorite(data);
    }

    switchFavorite() {
        //console.log('inside click');
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _onPressButton(data) {
        this.props.getVideoOrChannelRelatedData(data);
        NavigationService.navigate('Play', {from: 'channel'});
    }

    _onViewAllButtonPress(categoryId) {
       // console.log(categoryId);
        NavigationService.navigate('Channels',{from: 'LiveChannels',categoryId: categoryId});
    }

    isChannelFavorite = (channelId) => {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    channelFavorite(data) {
        let favoriteChannels = this.props.favorite.channels;
        //console.log('here://' + this.props.favorite.channels);
        let indexOf = favoriteChannels.findIndex((f) => {
            return f.channelId == data.channel.channelId;
        });
        let channelToBeUpdated = {
            channelId: data.channel.channelId,
            channelName: data.channel.channelName,
            channelLang: data.channel.channelLang,
            channelImage: data.channel.channelImage,
            channelLogo: data.channel.channelLogo,
            channelGrade: data.channel.channelGrade,
            channelStatus: data.channel.channelStatus,
            channelBaseline: data.channel.channelBaseline,
            channelCategories: data.channel.channelCategories,
            isFavorite: true
        };

        if (indexOf == -1) {
            favoriteChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/channels", channelToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                    //console_log('addfav' + response);
                })
                .catch((error) => {
                    //console_log(error);
                });

            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})

        } else {
            favoriteChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log('removefav' + response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }
        this.props.addFavoriteChannel(favoriteChannels);
    }

    componentDidMount(){
        Orientation.lockToPortrait();
        axios.defaults.headers.common['authorization'] = this.props.accessToken;
            this.props.show();
            setTimeout(() => {
                this.props.hide();
            }, 1500);
            this.setState({dataLoad: true});


    }

    renderLoadChannel = () => {
        let favoriteChannelsIds = this.props.favorite.channels.map((c) => c.channelId);
        return (
            <View>
                {!this.state.favoriteSwitch && this.props.category.categories ?
                    this.props.category.categories.map((category, i) => {
                        return (
                            <View key={i}>
                                <View style={{
                                    height: 35,
                                    flexDirection: 'row',
                                    paddingTop: 5,
                                    paddingBottom: 5,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    justifyContent: 'space-between',
                                    backgroundColor: 'black',
                                    alignItems: 'center'
                                }}>
                                    <View>
                                        <Text numberOfLines={1}
                                              style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                            {category.name}
                                        </Text>
                                    </View>
                                    <TouchableOpacity
                                        onPress={this._onViewAllButtonPress.bind(this, category.categoryId)}>
                                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                            <Icon name='th' size={15}
                                                  style={{backgroundColor: 'transparent', paddingRight: 5}}
                                                  color='#d51a92'/>
                                            <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                                View All
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <ScrollView horizontal={true}>
                                    <View style={{flexDirection: 'row'}}>
                                        {this.props.category.channels.map((channel, index) => {
                                            let categoryIds = channel.channelCategories.map((c) => c.categoryId);
                                            if (~categoryIds.indexOf(category.categoryId)) {
                                                return (
                                                    <View style={liveChannelStyle.tvThmbnail} key={index}>
                                                        <TouchableOpacity
                                                            onPress={this._onPressButton.bind(this, {channel: channel})}>
                                                            <ImageBackground style={liveChannelStyle.tvImageBackground}
                                                                             source={{uri: channel.channelImage ? vars.BASE_URL + "uploads/" + channel.channelImage : thumbnail}}>
                                                                <View style={{
                                                                    flex: 1,
                                                                    backgroundColor: "rgba(0,0,0,.5)"
                                                                }}>
                                                                    <TouchableHighlight style={liveChannelStyle.tvFavoriteBg}
                                                                                        onPress={this._handleFavoriteClicked.bind(this, {channel: channel})}>
                                                                        <View style={liveChannelStyle.tvFavoriteView}>
                                                                           <Icon name='heart' size={15}
                                                                                  style={{backgroundColor: 'transparent'}}
                                                                                  color={this.isChannelFavorite(channel.channelId) ? "#f00b64" : "#fff"}/>
                                                                        </View>
                                                                    </TouchableHighlight>
                                                                    <View style={{
                                                                        alignItems: 'center',
                                                                        justifyContent: 'center'
                                                                    }}>
                                                                        <Image style={{
                                                                            width: '55%',
                                                                            height: '55%',
                                                                            resizeMode: 'contain'
                                                                        }} source={{uri: channel.channelLogo}}/>
                                                                    </View>
                                                                    <View style={liveChannelStyle.videoTitleView}>
                                                                        <Text numberOfLines={1}
                                                                              style={[styles.avRegular, liveChannelStyle.videoTitle]}>{channel.channelName}</Text>
                                                                        <View style={liveChannelStyle.liveNow}>
                                                                            <Icon name='rss' size={13} style={{
                                                                                backgroundColor: 'transparent',
                                                                                paddingRight: 5
                                                                            }} color='#fff'/>
                                                                            <Text
                                                                                style={[styles.avRegular, liveChannelStyle.videoDuration]}>Live
                                                                                Now</Text>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </ImageBackground>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            }
                                        }, this)
                                        }
                                    </View>
                                </ScrollView>
                            </View>
                        )
                    })

                    :
                    this.props.category.categories.map((category, i) => {
                        let categoryChannels = this.props.category.channels.filter((channel) => {
                            let categoryIds = channel.channelCategories.map((c) => c.categoryId);
                            if (~categoryIds.indexOf(category.categoryId) && ~favoriteChannelsIds.indexOf(channel.channelId)) {
                                return channel;
                            }
                        });

                        if (categoryChannels.length > 0) {
                            return (
                                <View key={i}>
                                    <View style={{
                                        height: 35,
                                        flexDirection: 'row',
                                        paddingTop: 5,
                                        paddingBottom: 5,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        justifyContent: 'space-between',
                                        backgroundColor: 'black',
                                        alignItems: 'center'
                                    }}>
                                        <View>
                                            <Text numberOfLines={1}
                                                  style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                                {category.name}
                                            </Text>
                                        </View>
                                        <TouchableOpacity
                                            onPress={this._onViewAllButtonPress.bind(this, category.categoryId)}>
                                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                                <Icon name='th' size={15}
                                                      style={{backgroundColor: 'transparent', paddingRight: 5}}
                                                      color='#d51a92'/>
                                                <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                                    View All
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {categoryChannels ? <ScrollView horizontal={true}>
                                        <View style={{flexDirection: 'row'}}>
                                            {categoryChannels.map((channel, index) => {
                                                let categoryIds = channel.channelCategories.map((c) => c.categoryId);
                                                if (~categoryIds.indexOf(category.categoryId)) {
                                                    return (
                                                        <View style={liveChannelStyle.tvThmbnail} key={index}>
                                                            <TouchableOpacity
                                                                onPress={this._onPressButton.bind(this, {channel: channel})}>
                                                                <ImageBackground
                                                                    style={liveChannelStyle.tvImageBackground}
                                                                    source={{uri: channel.channelImage ? vars.BASE_URL + "uploads/" + channel.channelImage : thumbnail}}>
                                                                    <View style={{
                                                                        flex: 1,
                                                                        backgroundColor: "rgba(0,0,0,.5)"
                                                                    }}>
                                                                            <TouchableHighlight style={liveChannelStyle.tvFavoriteBg}
                                                                                onPress={this._handleFavoriteClicked.bind(this, {channel: channel})}>
                                                                                <View style={liveChannelStyle.tvFavoriteView}>
                                                                                <Icon name='heart' size={15}
                                                                                      style={{backgroundColor: 'transparent'}}
                                                                                      color={this.isChannelFavorite(channel.channelId) ? "#f00b64" : "#fff"}/>
                                                                                </View>
                                                                            </TouchableHighlight>
                                                                        <View style={{
                                                                            alignItems: 'center',
                                                                            justifyContent: 'center'
                                                                        }}>
                                                                            <Image style={{
                                                                                width: '60%',
                                                                                height: '40%',
                                                                                resizeMode: 'contain'
                                                                            }} source={{uri: channel.channelLogo}}/>
                                                                        </View>
                                                                        <View style={liveChannelStyle.videoTitleView}>
                                                                            <Text numberOfLines={1}
                                                                                  style={[styles.avRegular, liveChannelStyle.videoTitle]}>{channel.channelName}</Text>
                                                                            <View style={liveChannelStyle.liveNow}>
                                                                                <Icon name='rss' size={13} style={{
                                                                                    backgroundColor: 'transparent',
                                                                                    paddingRight: 5
                                                                                }} color='#fff'/>
                                                                                <Text
                                                                                    style={[styles.avRegular, liveChannelStyle.videoDuration]}>Live
                                                                                    Now</Text>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                </ImageBackground>
                                                            </TouchableOpacity>
                                                        </View>
                                                    )
                                                }
                                            }, this)
                                            }
                                        </View>
                                    </ScrollView> : ''}

                                </View>
                            )
                        }
                    })
                }

            </View>
        )
    }

    render() {
        return (
            <Container>
                <Header
                    isDrawer={true}
                    isTitle={true}
                    title='Live Channels'
                    isSearch={true}
                    rightLabel=''
                />
                <Loader visible={this.props.loader.isLoading}/>
                <Search from={"channels"}/>
                <View style={liveChannelStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView>
                        <View style={liveChannelStyle.sliderView}>
                            <Slider text="Select your favorite channel today!" isTvVideo={true}/>
                        </View>
                        <View>
                            <View style={{ height: 40, flexDirection: 'row', paddingTop: 10, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                                <View>
                                    <Text style={[styles.avRegular, liveChannelStyle.allCategory]}>
                                        All Categories
                                    </Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={[styles.avRegular, liveChannelStyle.favoriteSwitchText]}>
                                        Watch later
                                    </Text>
                                    <Switch style={{ transform: [{ scaleX: .85 }, { scaleY: .75 }] }} value={this.state.favoriteSwitch} onValueChange={this.switchFavorite} />
                                </View>
                            </View>
                        </View>
                        {this.state.dataLoad ? this.renderLoadChannel() : null}
                        <Footer />
                    </ScrollView>
                </View>
            </Container>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        loader: state.ActivityIndicatorReducer,
        historyVideos: state.HistoryReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        user: state.AuthenticationReducer,
        header: state.HeaderReducer,
        search: state.SearchReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
       // checkAccess,
        show,
        hide,
       getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveChannels);

