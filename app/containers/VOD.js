import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import { Platform, Image, View, StatusBar, Linking, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, FlatList, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
//import MaterialIcon from 'react-native-vector-icons/dist/MaterialIcon';

// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import VODCategory from '../components/VODCategory/VODCategory';
import Slider from '../components/Slider/Slider';

// Styles
import { styles } from "../style/appStyles";
import VODStyle from "../style/vodStyle";
import liveChannelStyle from "../style/liveChannelStyle";
// Other data/helper functions
import data from '../data/moviesData.json';
import { movie_1, movie_2, tv_1, tv_2, thumbnail } from "../assets/Images";
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { showMessage } from '../actions/FlashMessageActions';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getDetails, getInterests } from '../actions/AccountActions';
import { getVideosPackages, getVideos, getTVCategories, getChannels } from '../actions/CategoryActions';
import { addFavoriteChannel, addFavoriteVideo, addLikesChannels, addLikesVideos } from '../actions/FavoriteActions';
import { addVideoHistory, addChannelHistory, addShowHistory } from '../actions/HistoryActions';
import * as vars from '../constants/api';
import { messages } from '../constants/messages';
import { console_log } from '../utils/helper';
import NavigationService from "../utils/NavigationService";
import { movies } from '../constants/movies';
import { bidiotvMoviesData } from '../constants/bidiotvmovies';
import { esmobiotv } from '../constants/esmobiotv';
import Loader from '../components/Loader/Loader';
import Search from '../components/Search/Search';
let movies1 = movies;
import SplashScreen from 'react-native-splash-screen'
import MessageBar from '../components/Message/Message';
import Globals from  '../constants/Globals';
import DeviceType from '../../App';
let bidiotvMovies = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;

class VOD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data.moviesData,
            favoriteSwitch: false,
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
        };

        //console.log('bidiotvmovies1:', bidiotvmovies1)

        //this.videoFavorite = this.videoFavorite.bind(this);
        //this.isVideoFavorite = this.isVideoFavorite.bind(this);
        this.switchFavorite = this.switchFavorite.bind(this);
        axios.defaults.headers.common['authorization'] = this.props.accessToken;
    }

    clickedButton() {

    }

    componentWillMount(){
        //console.log('%%%%%')
        axios.defaults.headers.common['authorization'] = this.props.accessToken;
        if(Globals.url ===  'http://uk.mobiotv.com' && this.props.category.categories.length === 0) {
            this.props.show();
            axios.all([axios.get(vars.BASE_API_URL + '/getUserProfile'), axios.get(vars.BASE_API_URL + '/interests'), axios.get(vars.BASE_API_URL + '/categories'), axios.get(vars.BASE_API_URL + '/packages'), axios.get(vars.BASE_API_URL + '/favorites'), axios.get(vars.BASE_API_URL + '/history'), axios.get(vars.BASE_API_URL + '/likes')])
            //axios.all([axios.get(vars.BASE_API_URL + '/getUserProfile'), axios.get(vars.BASE_API_URL + '/interests'), axios.get(vars.BASE_API_URL + '/categories'), axios.get(vars.BASE_API_URL + '/packages'), axios.get(vars.BASE_API_URL + '/favorites'), axios.get(vars.BASE_API_URL + '/likes')])
                .then(axios.spread((userProfile, interests, categories, packages, favorites, history, likes) => {
                //.then(axios.spread((userProfile, interests, categories, packages, favorites, likes) => {
                   // console.log('history.data.data:',history.data.data)
                    // for user details
                    if (userProfile.data.data) {
                        this.props.getDetails(userProfile.data.data);
                    }

                    // for interests
                    if (interests.data.data) {
                        this.props.getInterests(interests.data.data);
                    }

                    // for channel categories
                    if (categories.data.data) {
                        this.props.getTVCategories(categories.data.data);
                    }

                    // for packages of Video On Demand
                    if (packages.data.data.packages) {
                        this.props.getVideosPackages(packages.data.data.packages);

                        // for Videos On Demand
                        axios.all(packages.data.data.packages.map(l => axios.get(vars.BASE_API_URL + '/packages/' + l.id)))
                            .then(axios.spread((...res) => {
                                // all requests are now complete
                                res.map((packageDetails) => {
                                    if (packageDetails) {
                                        this.props.getVideos(packageDetails.data.data.package);
                                    }
                                });
                            }));
                    }

                    // for favorites
                    if (favorites.data.data) {
                        this.props.addFavoriteChannel(favorites.data.data.channels);
                        this.props.addFavoriteVideo(favorites.data.data.videos);
                    }

                    //for history
                    if (history.data.data) {
                        this.props.addChannelHistory(history.data.data.channels);
                        this.props.addVideoHistory(history.data.data.videos);
                        this.props.addShowHistory(history.data.data.shows);
                    }

                    // for likes
                    if (likes.data.data) {
                        this.props.addLikesChannels(likes.data.data.channels);
                        this.props.addLikesVideos(likes.data.data.videos);
                    }

                    //this.props.hide();
                    this.setState({dataLoad: true})
                }));
        }
    }

    componentDidMount(){
        this.setState({dataLoad: true})
        this.props.show();
        setTimeout(() => {
            this.props.hide();
            Platform.OS !== 'ios' ?  SplashScreen.hide() : null;
        }, 1500);
    }

    componentWillReceiveProps(newProps){

        if(newProps.category.categories.length !== 0){
           // this.setState({dataLoad: true})
        }    }


    _onViewAllButtonPress(categoryId, from) {
        if(from === 'channel'){
            NavigationService.navigate('Channels',{from: 'VOD',categoryId: categoryId});
        }
        else{
            NavigationService.navigate('VideoList',{from: 'VOD',categoryId: categoryId});
        }
    }

    _handleFavoriteClicked(data) {
        //console.log(JSON.stringify(data));
        this.videoFavorite(data.video);
    }

    _onPressButton(data) {
        //console_log('****', data)
        NavigationService.navigate('PlayVOD');
        this.props.getVideoOrChannelRelatedData(data);
    }

    _onPressButtonVODChannel(data) {
         NavigationService.navigate('PlayOthers');
        this.props.getVideoOrChannelRelatedData(data);
    }

    videoFavorite(data) {
       let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == data.id;
        });
        let videoToBeUpdated = {
            videoId: data.id,
            duration: data.duration,
            name: data.name,
            preview: data.preview
        };
        if (indexOf == -1) {
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                    //console_log('addVide' + response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);


    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });
        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    LoadVOD = () =>{
        //console_log('***', this.props.category.videos.length)
        let favoriteVideosIds = this.props.favorite.videos.map((v) => v.videoId);
        return(
            <View>
        {!this.state.favoriteSwitch && movies1 ?
            movies1.map((category, i) => {
                return (
                    <View key={i}>
                        <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                            <View>
                                <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                    {category.name}
                                </Text>
                            </View>
                            <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, category.id, 'video')}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='th' size={15} style={{ backgroundColor: 'transparent', paddingRight: 5 }} color='#d51a92' />
                                    <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                        Ver todos
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {category.videos.length > 0 ?
                            <ScrollView horizontal={true} >
                                <View style={{ flexDirection: 'row' }}>
                                    {
                                        category.videos.map((m, index) => {
                                            return (
                                                <View style={liveChannelStyle.imageThmbnail} key={index}>
                                                    <TouchableOpacity onPress={this._onPressButton.bind(this, {video: m})}>
                                                        <ImageBackground style={liveChannelStyle.imageBackground} source={m.preview}>
                                                            <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: m})}>
                                                                <View style={liveChannelStyle.tvFavoriteView}>
                                                                <Icon name='star' size={15} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(m.id) ? "#FFC107" : "#fff"} />
                                                                </View>
                                                            </TouchableOpacity>
                                                        </ImageBackground>
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        })
                                    }
                                </View>

                            </ScrollView>
                            : null}

                    </View>
                )
            })
            : movies1.map((category, i) => {
                let newVideos = category.videos.filter((video) => {
                    if (~favoriteVideosIds.indexOf(video.id)) {
                        return video;
                    }
                });
                //console_log('newVideos://',newVideos)
                if (newVideos.length > 0) {
                    return (
                        <View key={i}>
                            <View style={{
                                height: 35,
                                flexDirection: 'row',
                                paddingTop: 5,
                                paddingBottom: 5,
                                paddingLeft: 10,
                                paddingRight: 10,
                                justifyContent: 'space-between',
                                backgroundColor: 'black',
                                alignItems: 'center'
                            }}>
                                <View>
                                    <Text numberOfLines={1}
                                          style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                        {category.name}
                                    </Text>
                                </View>
                                <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, category.id, 'video')}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Icon name='th' size={15}
                                              style={{backgroundColor: 'transparent', paddingRight: 5}}
                                              color='#d51a92'/>
                                        <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                            Ver todos
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {newVideos.length > 0 ?
                                <ScrollView horizontal={true}>
                                    <View style={{flexDirection: 'row'}}>
                                        {
                                            newVideos.reverse().map((m, index) => {
                                                return (
                                                    <View style={liveChannelStyle.imageThmbnail}
                                                          key={index}>
                                                        <TouchableOpacity onPress={this._onPressButton.bind(this, {video: m})}>
                                                            <ImageBackground style={liveChannelStyle.imageBackground} source={m.preview}>
                                                                <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: m})}>
                                                                    <View style={liveChannelStyle.tvFavoriteView}>
                                                                    <Icon name='star' size={15} style={{backgroundColor: 'transparent'}} color={this.isVideoFavorite(m.id) ? "#FFC107" : "#fff"}/>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </ImageBackground>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>

                                </ScrollView>
                                : null}

                        </View>
                    )
                }
            })
        }

        {!this.state.favoriteSwitch && this.props.category.videos ?
            this.props.category.videos.sort((a, b) => a.id > b.id).map((videos, key) => {
                return (<View key = {key}>
                        <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                            <View>
                                <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                    {videos.name}
                                </Text>
                            </View>
                            <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, videos.id, 'channel')}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='th' size={15} style={{ backgroundColor: 'transparent', paddingRight: 5 }} color='#d51a92' />
                                    <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                        Ver todos
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {videos.videos?
                            <ScrollView horizontal={true} >
                                <View style={{ flexDirection: 'row' }}>
                                    {videos.videos.map((v, k) => {
                                        return (
                                            <View style={liveChannelStyle.tvThmbnail} key={k}>
                                                <TouchableOpacity onPress={this._onPressButtonVODChannel.bind(this, {video: v})}>
                                                    <ImageBackground style={liveChannelStyle.tvImageBackground} source={{uri: v.preview}}>
                                                        <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}>
                                                                <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                    <View style={liveChannelStyle.tvFavoriteView}>
                                                                    <Icon name='star' size={15} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id)? "#FFC107" : "#fff"} />
                                                                    </View>
                                                                </TouchableOpacity>
                                                            <View style={liveChannelStyle.videoTitleView}>
                                                                <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{v.name}</Text>
                                                                <View style={[liveChannelStyle.videoDurationView]}>
                                                                    <Text style={[styles.avRegular, liveChannelStyle.videoDuration,{paddingBottom: 1}]}>1h 40m</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                        )

                                    },this)
                                    }
                                </View>
                            </ScrollView>

                            : ''
                        }

                    </View>
                )
            })
            :

            this.props.category.videos.sort((a, b) => a.id > b.id).map((videos, key) => {
                let packageVideos = videos.videos.filter((video) => {
                    if (~favoriteVideosIds.indexOf(video.id)) {
                        return video;
                    }
                });
                if (packageVideos.length > 0) {
                    return (
                        <View key = {key}>
                            <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                                <View>
                                    <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                        {videos.name}
                                    </Text>
                                </View>
                                <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, videos.id, 'channel')}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='th' size={15} style={{ backgroundColor: 'transparent', paddingRight: 5 }} color='#d51a92' />
                                        <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                            Ver todos
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {packageVideos?
                                <ScrollView horizontal={true} >
                                    <View style={{ flexDirection: 'row' }}>
                                        {packageVideos.map((v, k) => {
                                            //console_log(v.preview)
                                            return (
                                                <View style={liveChannelStyle.tvThmbnail} key={k}>
                                                    <TouchableOpacity onPress={this._onPressButtonVODChannel.bind(this, {video: v})}>
                                                        <ImageBackground style={liveChannelStyle.tvImageBackground} source={{uri: v.preview}}>
                                                            <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}>
                                                                    <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                        <View style={liveChannelStyle.tvFavoriteView}>
                                                                        <Icon name='star' size={15} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"} />
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                <View style={liveChannelStyle.videoTitleView}>
                                                                    <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{v.name}</Text>
                                                                    <View style={liveChannelStyle.videoDurationView}>
                                                                        <Text style={[styles.avRegular, liveChannelStyle.videoDuration]}>1h 40m</Text>
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        </ImageBackground>
                                                    </TouchableOpacity>
                                                </View>
                                            )

                                        },this)
                                        }
                                    </View>
                                </ScrollView>

                                : ''
                            }
                        </View>
                    )
                }
            })
        }
            </View>
    )

    }


    LoadBiodtv = () =>{
        let favoriteVideosIds = this.props.favorite.videos.map((v) => v.videoId);
        let bidiotvfavourite = [].concat.apply([], bidiotvMovies.map((c) => c.videos)).filter((v) => {
            if (~favoriteVideosIds.indexOf(v.id)) {
                return v;
            }
        });
        //console.log('bidiotvfavourite:', bidiotvfavourite.length)
        return(
            <View >
                {!this.state.favoriteSwitch  ?
                    bidiotvMovies.sort((a, b) => a.id > b.id).map((videos, key) => {
                        return (<View key = {key}>
                                <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                                    <View>
                                        <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                            {videos.name}
                                        </Text>
                                    </View>
                                    <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, videos.id, Globals.type === 'es' ?  'video' : 'channel')}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                                {Globals.type === 'es' ? "Más" : 'More'}
                                            </Text>
                                            <FeatherIcon name='chevron-right' size={17} style={{ backgroundColor: 'transparent', paddingRight: 5, marginTop: 2 }} color='#d51a92' />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {videos.videos?
                                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} >
                                        <View style={{ flexDirection: 'row' }}>
                                            {videos.videos.map((v, k) => {
                                                return (
                                                    Globals.type === 'es' ?
                                                    <View style={liveChannelStyle.imageThmbnail}
                                                          key={k}>
                                                        <TouchableOpacity onPress={this._onPressButton.bind(this, {video: v})}>
                                                            <ImageBackground style={liveChannelStyle.imageBackground} resizeMode= 'stretch' source={v.preview}>
                                                                <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                    <View style={liveChannelStyle.tvFavoriteView}>
                                                                        <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{backgroundColor: 'transparent'}} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"}/>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </ImageBackground>
                                                        </TouchableOpacity>
                                                    </View>
                                                        :
                                                        <View style={liveChannelStyle.tvThmbnail} key={k}>
                                                            <TouchableOpacity onPress={this._onPressButtonVODChannel.bind(this, {video: v})}>
                                                                <ImageBackground style={liveChannelStyle.tvImageBackground} resizeMode="stretch" source={v.preview}>
                                                                    <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}>
                                                                        <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                            <View style={liveChannelStyle.tvFavoriteView}>
                                                                                <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id)? "#FFC107" : "#fff"} />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                        <View style={liveChannelStyle.videoTitleView}>
                                                                            <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{v.name}</Text>
                                                                            <View style={[liveChannelStyle.videoDurationView]}>
                                                                                <Text style={[styles.avRegular, liveChannelStyle.videoDuration,{paddingBottom: 1}]}>1h 40m</Text>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                </ImageBackground>
                                                            </TouchableOpacity>
                                                        </View>
                                                )

                                            },this)
                                            }
                                        </View>
                                    </ScrollView>

                                    : ''
                                }

                            </View>
                        )
                    })
                    :
                    bidiotvfavourite.length !==0 ?
                    bidiotvMovies.sort((a, b) => a.id > b.id).map((videos, key) => {
                        let packageVideos = videos.videos.filter((video) => {
                            if (~favoriteVideosIds.indexOf(video.id)) {
                                return video;
                            }
                        });
                        if (packageVideos.length > 0) {
                            return (
                                <View key = {key}>
                                    <View style={{ height: 35, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                                        <View>
                                            <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName]}>
                                                {videos.name}
                                            </Text>
                                        </View>
                                        <TouchableOpacity onPress={this._onViewAllButtonPress.bind(this, videos.id, Globals.type === 'es' ?  'video' : 'channel')}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Text style={[styles.avRegular, liveChannelStyle.browseAll]}>
                                                    {Globals.type === 'es' ? "Más" : 'More'}
                                                </Text>
                                                <FeatherIcon name='chevron-right' size={17} style={{ backgroundColor: 'transparent', paddingRight: 5, marginTop: 2 }} color='#d51a92' />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {packageVideos?
                                        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} >
                                            <View style={{ flexDirection: 'row' }}>
                                                {packageVideos.map((v, k) => {
                                                    //console_log(v.preview)
                                                    return (
                                                        Globals.type === 'es' ?
                                                        <View style={liveChannelStyle.imageThmbnail}
                                                              key={k}>
                                                            <TouchableOpacity onPress={this._onPressButton.bind(this, {video: v})}>
                                                                <ImageBackground style={liveChannelStyle.imageBackground} resizeMode= 'stretch' source={v.preview}>
                                                                    <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                        <View style={liveChannelStyle.tvFavoriteView}>
                                                                            <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{backgroundColor: 'transparent'}} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"}/>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                </ImageBackground>
                                                            </TouchableOpacity>
                                                        </View>
                                                            :
                                                            <View style={liveChannelStyle.tvThmbnail} key={k}>
                                                                <TouchableOpacity onPress={this._onPressButtonVODChannel.bind(this, {video: v})}>
                                                                    <ImageBackground style={liveChannelStyle.tvImageBackground} resizeMode="stretch" source={v.preview}>
                                                                        <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}>
                                                                            <TouchableOpacity style={liveChannelStyle.tvFavoriteBg} onPress={this._handleFavoriteClicked.bind(this, {video: v})}>
                                                                                <View style={liveChannelStyle.tvFavoriteView}>
                                                                                    <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"} />
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                            <View style={liveChannelStyle.videoTitleView}>
                                                                                <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{v.name}</Text>
                                                                                <View style={liveChannelStyle.videoDurationView}>
                                                                                    <Text style={[styles.avRegular, liveChannelStyle.videoDuration]}>1h 40m</Text>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    </ImageBackground>
                                                                </TouchableOpacity>
                                                            </View>
                                                    )

                                                },this)
                                                }
                                            </View>
                                        </ScrollView>

                                        : ''
                                    }

                                </View>
                            )
                        }
                    }) :  <View>
                            <View style={favoriteStyles.noChannelsViewTitle}>
                                <Text style={[styles.avRegular, favoriteStyles.noData]}>{Globals.type=== 'es' ?  "Ningún video añadido para ver más tarde" : "No Videos Added To Favorites Yet"}</Text>
                            </View>
                            <View style={favoriteStyles.noChannelsViewDesc}>
                                <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>{Globals.type=== 'es' ?  "Agrega tus videos favoritos para poder verlos fácilmente y sin problemas." : "Add your favorite videos to access and watch easily without any hassles."}</Text>
                            </View>
                            <TouchableHighlight  onPress={()=> this.setState({ favoriteSwitch: false })} style={[favoriteStyles.exploreButtonView]}>
                                <View style={[favoriteStyles.exploreButton, {backgroundColor: "#e83e8c"}]}>
                                    <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                        {Globals.type=== 'es' ? "Explora más videos" : "Explore Videos"}
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>

                }

            </View>
        )

    }

    render() {
        let favoriteVideosIds = this.props.favorite.videos.map((v) => v.videoId);
        return (
            <Container >
                <ImageBackground  style={{ zIndex: 999 }}>
                <Header
                    isDrawer={true}
                    isTitle= {Globals.url !==  'http://uk.mobiotv.com' ? true : false}
                    title='VOD'
                    isSearch={true}
                    showSearch={true}
                    rightLabel=''
                />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading}/>
                {/*<Search from={"videos"}/>*/}
                <View style={[VODStyle.contentView]}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView  bounces={false} keyboardShouldPersistTaps={'always'} keyboardDismissMode='on-drag'>
                        <View style={{flex: 1}}>
                        <View style={VODStyle.sliderView}>
                            <Slider text="Select your favorite show today!" isTvVideo={false}/>
                        </View>
                        <View>
                            <View style={{ height: 40, flexDirection: 'row', paddingTop: 10, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                                <View>
                                    <Text style={[styles.avRegular, VODStyle.allCategory]}>
                                        {Globals.type === 'es' ? "Todas las categorías" : 'All Categories'}
                                    </Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={[styles.avRegular, VODStyle.favoriteSwitchText]}>
                                        {Globals.type === 'es' ? "Favoritos" : 'Favorites'}
                                    </Text>
                                    <Switch style={{ transform: [{ scaleX: .85 }, { scaleY: .75 }] }} value={this.state.favoriteSwitch} onValueChange={this.switchFavorite} />
                                </View>
                            </View>
                        </View>
                        { this.state.dataLoad ? this.LoadBiodtv() : null}
                        </View>
                        {/*<Footer />*/}
                    </ScrollView>
                </View>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
         getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
        getDetails,
        getInterests,
        getVideosPackages,
        getVideos,
        getTVCategories,
        getChannels,
        addChannelHistory,
        addVideoHistory,
        addShowHistory,
        addLikesChannels,
        addLikesVideos
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(VOD);

