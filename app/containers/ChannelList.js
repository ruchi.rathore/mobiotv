import React, { Component } from "react";
import { Image, View, StatusBar, Linking, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, FlatList, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';

// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

// Styles
import { styles } from "../style/appStyles";
import channelListStyle from "../style/channelListStyle";
import liveChannelStyle from '../style/liveChannelStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
// Other data/helper functions
import data from '../data/moviesData.json';
import NavigationService from "../utils/NavigationService";
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { tv_2, channelLogo, thumbnail } from "../assets/Images";
import * as vars from '../constants/api';
import { console_log } from '../utils/helper';
import { messages } from '../constants/messages';
import Search from "../components/Search/Search";
import MessageBar from '../components/Message/Message';
import { bidiotvMoviesData } from '../constants/bidiotvmovies';
let bidiotvMovies = bidiotvMoviesData;
import Globals from  '../constants/Globals';

class Channels extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data.moviesData,
            favoriteSwitch: false,
            channels: [],
            videos: [],
            type: '',
            pageId: this.props.navigation.state.params.categoryId,
            categoryName: "",
            color: '',
            message:'',
            showMessage:false,
        };

        this.channelFavorite = this.channelFavorite.bind(this);
        this.isChannelFavorite = this.isChannelFavorite.bind(this);

    }

    componentDidMount(){
        //console.log('NavigationService:' + JSON.stringify(this.props));
        if (this.props.navigation.state.params.from === "LiveChannels"){
            let channelsByCategory = this.props.category.channels.filter((channel) => {
                    let catArr = channel.channelCategories.map((cat) => {return cat.categoryId});
                    if (~catArr.indexOf(this.state.pageId)) {
                        return channel;
                    }
                }),
                category = this.props.category.categories.find((c) => {
                    return c.categoryId == this.state.pageId;
                });

            this.setState({
                channels: channelsByCategory,
                type: 'channels',
                categoryName: category.name
            });
        }
        else if(Globals.url == 'http://uk.mobiotv.com'){
                    console.log("******************bidiotv",bidiotvMovies);
                    let videoforbidiotv = bidiotvMovies.find((v) => v.id == this.state.pageId)['videos'],
                        categoryforbidiotv = bidiotvMovies.find((v) => v.id == this.state.pageId);

                    this.setState({
                        videos: videoforbidiotv,
                        type: 'videos',
                        categoryName: categoryforbidiotv.name
                    });
        }
        else{
            let videoByCategory = this.props.category.videos.find((v) => v.id == this.state.pageId)['videos'],
                category = this.props.category.videos.find((v) => v.id == this.state.pageId);

            this.setState({
                videos: videoByCategory,
                type: 'videos',
                categoryName: category.name
            });
        }
    }

    isChannelFavorite(channelId) {
        let indexOf = this.props.favorite.channels.findIndex((f) => {
            return f.channelId == channelId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _handleFavoriteVideoClicked(data) {
        this.videoFavorite(data.video);
    }

    _handleFavoriteClicked(data) {
        //console.log(JSON.stringify(data));
        this.channelFavorite(data);
    }

    _onPressButton(data) {
        if(this.props.navigation.state.params.from === 'LiveChannels'){
            NavigationService.navigate('Play');
        }else{
            NavigationService.navigate('PlayOthers');
        }
        this.props.getVideoOrChannelRelatedData(data);
    }

    onPressButtonOthers(data){
        NavigationService.navigate('PlayOthers');
        this.props.getVideoOrChannelRelatedData(data);
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    videoFavorite(data) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == data.id;
        });
        let videoToBeUpdated = {
            videoId: data.id,
            duration: data.duration,
            name: data.name,
            preview: data.preview
        };

        if (indexOf == -1) {
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                   // console_log('inVideofav' + response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log('inremoveVudeofav' + response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    channelFavorite(data) {
        let favoriteChannels = this.props.favorite.channels;
        let indexOf = favoriteChannels.findIndex((f) => {
            return f.channelId == data.channel.channelId;
        });
        let channelToBeUpdated = {
            channelId: data.channel.channelId,
            channelName: data.channel.channelName,
            channelLang: data.channel.channelLang,
            channelImage: data.channel.channelImage,
            channelLogo: data.channel.channelLogo,
            channelGrade: data.channel.channelGrade,
            channelStatus: data.channel.channelStatus,
            channelBaseline: data.channel.channelBaseline,
            channelCategories: data.channel.channelCategories,
            isFavorite: true
        };

        if (indexOf == -1) {
            favoriteChannels.push(channelToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/channels", channelToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteChannels.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/channels/"+channelToBeUpdated.channelId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteChannel(favoriteChannels);
    }

    render() {
        let favoriteChannelIds = this.props.favorite.channels.map(m => m.channelId);
        let thisCategoryChannels = (!this.state.favoriteSwitch)? this.state.channels: this.state.channels.filter((channel) => {
            if (~favoriteChannelIds.indexOf(channel.channelId)) {
                return channel;
            }
        });
        let favoriteVideoIds = this.props.favorite.videos.map(v => v.videoId);
        let thisCategoryVideos = (!this.state.favoriteSwitch)? this.state.videos : this.state.videos.filter((video) => {
            if (~favoriteVideoIds.indexOf(video.id)) {
                return video;
            }
        });
        return (
            <Container>
                <ImageBackground  style={{ zIndex: 999 }}>
                <Header
                    isDrawer={false}
                    isTitle={false}
                    title={this.state.type === 'videos' ? 'Video On Demand' : 'Live Channels'}
                    isSearch={true}
                    showSearch={true}
                    rightLabel=''
                />
                </ImageBackground>
                {/*<Search from={this.props.navigation.state.params.from === 'LiveChannels' ? 'channels' :  "videos"}/>*/}
                <View style={channelListStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView bounces={false} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 80}}>
                        <View style ={{flex: 3}}>
                        <View style={{ marginTop: 10,height: 40, flexDirection: 'row', padding: 5, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                            <View>
                                <Text style={[styles.avBold, channelListStyle.categoryName]}>
                                    {this.state.categoryName}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[styles.avBold, channelListStyle.favoriteSwitchText]}>
                                    Favorites
                                </Text>
                                <Switch style={{ transform: [{ scaleX: .85 }, { scaleY: .75 }] }} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                            </View>
                        </View>


                        {Globals.url !== 'http://uk.mobiotv.com'?

                        this.state.type == 'channels'?
                            <View style={{ backgroundColor: 'black', flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                                {thisCategoryChannels.length > 0 ?
                                    thisCategoryChannels.map((channel, index) => {
                                        return (
                                            <TouchableOpacity style={channelListStyle.imageThmbnail} key={index} onPress={this._onPressButton.bind(this, {channel: channel})}>
                                                    <ImageBackground style={channelListStyle.imageBackground} resizeMode="stretch" source={{uri:channel.channelImage ? vars.BASE_URL+"uploads/"+channel.channelImage : thumbnail}}>
                                                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,.5)' }}>
                                                                <TouchableOpacity style={[channelListStyle.tvFavoriteBg]} onPress={this._handleFavoriteClicked.bind(this, {channel: channel})}>
                                                                    <View style={channelListStyle.tvFavoriteView}>
                                                                    <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isChannelFavorite(channel.channelId) ? "#FFC107" : "#fff"} />
                                                                    </View>
                                                                </TouchableOpacity>
                                                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: -10 }}>
                                                                <Image style={{ width: '50%', height: '50%' }} resizeMode='contain' source={{uri:channel.channelLogo}} />
                                                            </View>
                                                            <View style={channelListStyle.videoTitleView}>
                                                                <Text numberOfLines={1} style={[styles.avBold, channelListStyle.videoTitle]}>{channel.channelName}</Text>
                                                                <View style={[channelListStyle.liveNow]}>
                                                                    <Icon name='rss' size={11} style={{ backgroundColor: 'transparent', marginLeft: 15, marginTop: 2}} color='#fff' />
                                                                    <Text style={[styles.avRegular, channelListStyle.videoDuration]}>Live Now</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </ImageBackground>
                                            </TouchableOpacity>
                                        )
                                    })

                                    : <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                                        <Text style={[styles.avRegular,{color: '#ffffff', fontSize: 18}]}>No channels found.</Text>
                                    </View>
                                }
                            </View>
                            :
                            <View style={{ backgroundColor: 'black', flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                            {thisCategoryVideos.length > 0 ?
                                    thisCategoryVideos.map((v, index) => {
                                        return (
                                            <TouchableOpacity style={channelListStyle.imageThmbnail} key={index} onPress={this._onPressButton.bind(this, {video: v})}>
                                                    <ImageBackground style={channelListStyle.imageBackground} resizeMode="stretch" source={{uri: v.preview}}>
                                                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,.5)' }}>
                                                                <TouchableOpacity style={channelListStyle.tvFavoriteBg} onPress={this._handleFavoriteVideoClicked.bind(this, {video: v})}>
                                                                    <View style={channelListStyle.tvFavoriteView}>
                                                                    <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"} />
                                                                    </View>
                                                                </TouchableOpacity>
                                                            <View style={channelListStyle.videoTitleView}>
                                                                <Text numberOfLines={1} style={[styles.avBold, channelListStyle.videoTitle]}>{v.name}</Text>
                                                                <View style={channelListStyle.videoDurationView}>
                                                                    <Text style={[styles.avRegular, channelListStyle.videoDuration]}>1h 40m</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </ImageBackground>
                                            </TouchableOpacity>
                                        )
                                    })
                                    :
                                    <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                                        <Text style={[styles.avRegular,{color: '#ffffff', fontSize: 18}]}>No Videos found.</Text>
                                    </View>
                                }

                            </View>
                        :
                        <View style={{ backgroundColor: 'black', flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                            {thisCategoryVideos.length > 0 ?
                                thisCategoryVideos.map((v, index) => {
                                    return (
                                        <TouchableOpacity style={channelListStyle.imageThmbnail} key={index} onPress={this.onPressButtonOthers.bind(this, {video: v})}>
                                            <ImageBackground style={channelListStyle.imageBackground} resizeMode="stretch" source={v.preview}>
                                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,.5)' }}>
                                                    <TouchableOpacity style={channelListStyle.tvFavoriteBg} onPress={this._handleFavoriteVideoClicked.bind(this, {video: v})}>
                                                        <View style={channelListStyle.tvFavoriteView}>
                                                            <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"} />
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={channelListStyle.videoTitleView}>
                                                        <Text numberOfLines={1} style={[styles.avBold, channelListStyle.videoTitle]}>{v.name}</Text>
                                                        <View style={channelListStyle.videoDurationView}>
                                                            <Text style={[styles.avRegular, channelListStyle.videoDuration]}>1h 40m</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </TouchableOpacity>
                                    )
                                })
                                :
                                <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                                        <View style={favoriteStyles.noChannelsViewTitle}>
                                            <Text style={[styles.avRegular, favoriteStyles.noData]}>{Globals.type=== 'es' ?  "Ningún video añadido para ver más tarde" : "No Videos Added To Favorites Yet"}</Text>
                                        </View>
                                        <View style={favoriteStyles.noChannelsViewDesc}>
                                            <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>{Globals.type=== 'es' ?  "Agrega tus videos favoritos para poder verlos fácilmente y sin problemas." : "Add your favorite videos to access and watch easily without any hassles."}</Text>
                                        </View>
                                        <TouchableHighlight  onPress={()=> NavigationService.goBack("null")} style={[favoriteStyles.exploreButtonView]}>
                                            <View style={[favoriteStyles.exploreButton, {backgroundColor: "#e83e8c"}]}>
                                                <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                                    {Globals.type=== 'es' ? "Explora más videos" : "Explore Videos"}
                                                </Text>
                                            </View>
                                        </TouchableHighlight>
                                </View>
                            }

                        </View>
                        }
                        </View>
                        {/*<Footer />*/}

                    </ScrollView>
                </View>
            </Container >
        );
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Channels);

