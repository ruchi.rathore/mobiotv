import React, { Component } from "react";
import { Image, View, StatusBar, Linking, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, FlatList, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';

// Components
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

// Styles
import { styles } from "../style/appStyles";
import channelListStyle from "../style/channelListStyle";
import liveChannelStyle from "../style/liveChannelStyle";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
// Other data/helper functions
import data from '../data/moviesData.json';
import NavigationService from "../utils/NavigationService";
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { tv_2, channelLogo, thumbnail } from "../assets/Images";
import * as vars from '../constants/api';
import { console_log } from '../utils/helper';
import { messages } from '../constants/messages';
import { esmobiotv } from '../constants/esmobiotv';
import { bidiotvMoviesData } from '../constants/bidiotvmovies';
import Search from "../components/Search/Search";
import MessageBar from '../components/Message/Message';
import Globals from "../constants/Globals";
let movies1 = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;
let bidiotvMovies = Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;


class VideoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            channels: [],
            videos: [],
            type: '',
            pageId: this.props.navigation.state.params.categoryId,
            categoryName: "",
            favoriteSwitch: false,
            color: '',
            message:'',
            showMessage:false,
        };
    }

    componentDidMount() {
        if (this.state.pageId == 0) {
            this.setState({
                videos: [].concat.apply([], movies1.map((c) => c.videos)),
                type: 'videos',
                categoryName: "Recently added"
            });
        } else {
            //console_log('this.state.pageId:' + this.state.pageId)
            let category = movies1.find((v) => v.id == this.state.pageId),
                videoByCategory = category['videos'];

            this.setState({
                videos: videoByCategory,
                type: 'videos',
                categoryName: category.name
            });
        }
    }

    goToPlay(data, event) {

    }

    _handleFavoriteClicked(data) {
        //console.log(JSON.stringify(data));
        this.videoFavorite(data.video);
    }

    _onPressButton(data) {
        NavigationService.navigate('PlayVOD');
        this.props.getVideoOrChannelRelatedData(data);
    }

    videoFavorite(data) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == data.id;
        });
        let videoToBeUpdated = {
            videoId: data.id,
            duration: data.duration,
            name: data.name,
            preview: data.preview
        };

        if (indexOf == -1) {
            this.props.showMessage({
                message: messages.addToFavorites,
                type: true
            });
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            this.props.showMessage({
                message: messages.removeFromFavorites,
                type: false
            });
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    render() {
        let favoriteVideoIds = this.props.favorite.videos.map(v => v.videoId);
        let thisCategoryVideos = (!this.state.favoriteSwitch)? this.state.videos : this.state.videos.filter((video) => {
            if (~favoriteVideoIds.indexOf(video.id)) {
                return video;
            }
        });
        return (
            <Container>
                <ImageBackground  style={{ zIndex: 999 }}>
                <Header
                    isDrawer={false}
                    isTitle={false}
                    title={'Video On Demand'}
                    isSearch={true}
                    showSearch={true}
                    rightLabel=''
                />
                </ImageBackground>
                {/*<Search from={"videos"}/>*/}
                <View style={channelListStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message} />
                    <ScrollView bounces={false} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 90}}>
                        <View style ={{flex: 3}}>
                        <View style={{ marginTop: 10, height: 40, flexDirection: 'row', padding: 5, justifyContent: 'space-between', backgroundColor: 'black', alignItems: 'center' }}>
                            <View>
                                <Text style={[styles.avRegular, channelListStyle.categoryName]}>
                                    {this.state.categoryName}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[styles.avRegular, channelListStyle.favoriteSwitchText]}>
                                    Favoritos
                                </Text>
                                <Switch style={{ transform: [{ scaleX: .85 }, { scaleY: .75 }] }} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                            </View>
                        </View>

                        {this.state.videos.length > 0 ?
                            <View style={{ backgroundColor: 'black', flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                                {this.state.videos.map((m, index) => {
                                    if (!this.state.favoriteSwitch || this.isVideoFavorite(m.id)) {

                                        return (
                                            <View style={liveChannelStyle.imageVideoThmbnail}
                                                  key={index}>
                                                <TouchableOpacity onPress={this._onPressButton.bind(this, {video: m})}>
                                                    <ImageBackground style={[liveChannelStyle.imageVideoListBackground]} resizeMode= 'stretch' source={m.preview}>
                                                           <TouchableOpacity style={liveChannelStyle.tvFavoriteVideoBg} onPress={this._handleFavoriteClicked.bind(this, {video: m})}>
                                                               <View style={liveChannelStyle.tvFavoriteView}>
                                                               <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{backgroundColor: 'transparent'}} color={this.isVideoFavorite(m.id) ? "#FFC107" : "#fff"}/>
                                                               </View>
                                                           </TouchableOpacity>
                                                    </ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }else{

                                    }
                                })
                                }
                            </View>

                            :
                            <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                                <Text style={[styles.avRegular, {color: '#ffffff', fontSize: 18}]}>No Videos found.</Text>
                            </View>
                        }
                        {thisCategoryVideos.length === 0 ?
                                <View>
                                    <View style={favoriteStyles.noChannelsViewTitle}>
                                        <Text style={[styles.avRegular, favoriteStyles.noData]}>Ningún video añadido para ver más tarde</Text>
                                    </View>
                                    <View style={favoriteStyles.noChannelsViewDesc}>
                                     <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>Agrega tus videos favoritos para poder verlos fácilmente y sin problemas.</Text>
                                    </View>
                                    <TouchableOpacity  onPress={()=> NavigationService.goBack("null")} style={[favoriteStyles.exploreButtonView]}>
                                        <View style={[favoriteStyles.exploreButton, {backgroundColor: "#e83e8c"}]}>
                                            <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                                Explora más videos
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            : null
                        }
                        </View>
                         {/*<Footer />*/}
                    </ScrollView>
                </View>
            </Container >
        );
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(VideoList);

