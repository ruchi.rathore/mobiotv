import React, { Component } from "react";
import { StatusBar, View, Text, TextInput, AsyncStorage, NetInfo, Alert, Dimensions, ImageBackground, Platform, Image } from "react-native";
import { Provider } from "react-redux";
import { StackNavigator } from "react-navigation";

// Components
import Accounts from "./app/containers/Accounts";
import Channels from "./app/containers/ChannelList";
import VideoList from "./app/containers/VideoList";
import History from "./app/containers/History";
import DrawerNavigation from "./app/components/Navigator/DrawerNavigator";
import DrawerVOD from "./app/components/Navigator/DrawerVOD";
import LiveChannels from "./app/containers/LiveChannels";
import Login from "./app/containers/Login";
import Play from "./app/containers/Play";
import PlayVOD from "./app/containers/PlayVOD";
import PlayOthers from "./app/containers/PlayOthers";
import Player from "./app/components/Player/Player";
import SmartTV from "./app/containers/SmartTV";
import SmartTVGuide from "./app/containers/SmartTVGuide";
import ShowSplashScreen from "./app/containers/ShowSplashScreen";
import StaticScreens from "./app/containers/StaticScreens";
import VOD from "./app/containers/VOD";
import WatchLater from "./app/containers/WatchLater";
import { splashBg, background } from "./app/assets/Images";
//import PaymentAndroid from "./app/components/Payment/PaymentAndroid";
import Search from "./app/components/Search/Search";
import TabNav from './app/components/Navigator/TabNav';
// Styles
import { styles } from "./app/style/appStyles";
console.disableYellowBox = true;
// Other data/functions
import store from "./app/store/configureStore";

import NavigationService from "./app/utils/NavigationService";
import Globals from './app/constants/Globals';
import Loader from './app/components/Loader/Loader';

Text.defaultProps.allowFontScaling=false;
TextInput.defaultProps.allowFontScaling=false;

const RootNavigator = StackNavigator({
    Welcome: {
        screen: ShowSplashScreen,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Access Id',
            header: null,
            gesturesEnabled: true
        }
    },
    Channels: {
        screen: Channels,
        navigationOptions: {
            title: 'Channels',
            header: null,
            gesturesEnabled: true
        }
    },
    VideoList: {
        screen: VideoList,
        navigationOptions: {
            title: 'VideoList',
            header: null,
            gesturesEnabled: true
        }
    },
    Play: {
        screen: Play,
        navigationOptions: {
            title: 'Play',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    PlayVOD: {
        screen: PlayVOD,
        navigationOptions: {
            title: 'Play VOD',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    PlayOthers: {
        screen: PlayOthers,
        navigationOptions: {
            title: 'Play Others',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    Player: {
        screen: Player,
        navigationOptions: {
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    SmartTV: {
        screen: SmartTV,
        navigationOptions: {
            title: 'Smart TV',
            header: null,
            gesturesEnabled: true
        }
    },
    SmartTVGuide: {
        screen: SmartTVGuide,
        navigationOptions: {
            title: 'Smart TV Guide',
            header: null,
            gesturesEnabled: true
        }
    },
    TabNav: {
        screen: TabNav,
        navigationOptions: {
            title: 'TabNav',
            header: null,
            gesturesEnabled: true
        }
    },

});

export default class MobioTV extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        }
    }

    componentWillMount() {
        Globals.url =  'http://uk.mobiotv.com';
        Globals.lang = "spanish";
        NetInfo.getConnectionInfo().then((connectionInfo) => {
                if (connectionInfo.type === "none") {
                    Alert.alert('MobioTV', "Please check your Internet Connection");
                }
            });
    }

    componentDidMount(){
        setTimeout(() => {
            this.setState({isLoading: false});
        },1000)
    }

    render() {
        return(
                 <Provider store={store}>
                    <RootNavigator ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }}/>
                </Provider>
        )

    }

};
